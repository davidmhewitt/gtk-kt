package org.gtk.dsl.gtk

import org.gtk.dsl.GtkDsl
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.widgets.box.Box
import org.gtk.gtk.widgets.button.toggleable.ToggleButton

/**
 * kotlinx-gtk
 * 16 / 03 / 2021
 */
@GtkDsl
fun Box.toggleButton(
	label: String? = null,
	mnemonic: Boolean = false,
	buttonBuilder: ToggleButton.() -> Unit = {},
): ToggleButton =
	(if (label != null) ToggleButton(label, mnemonic) else ToggleButton())
		.apply(buttonBuilder)
		.also { append(it) }


@GtkDsl
inline fun ToggleButton.onToggle(noinline onToggle: TypedNoArgFunc<ToggleButton>) {
	addOnToggledCallback(onToggle)
}
