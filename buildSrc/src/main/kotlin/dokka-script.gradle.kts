import org.jetbrains.dokka.gradle.DokkaTaskPartial

plugins {
    id("org.jetbrains.dokka")
}

tasks {
    val processDokkaMarkdown = register<Copy>("processDokkaMarkdown") {
        group = "documentation"

        from(layout.projectDirectory.dir("docs"))
        into(layout.buildDirectory.dir("docs-markdown"))

        val properties = linkedMapOf(
            "dependencyNotice" to """
                ### Dependency
                ```kotlin
                implementation("${project.group}:${project.name}:${project.version}")
                ```
            """.trimIndent()
        )

        inputs.properties(properties)
        expand(properties)
    }

    withType<DokkaTaskPartial> {
        dependsOn(processDokkaMarkdown)

        dokkaSourceSets.configureEach {
            includes.from(
                (buildDir.resolve("docs-markdown").listFiles() ?: emptyArray())
                    .sortedBy { if (it.name == "Module.md") "0" else it.name }
                    .map { "build/docs-markdown/${it.name}" }
                    .toTypedArray()
            )
        }
    }
}
