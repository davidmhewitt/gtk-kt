package org.gtk.gobject

import gobject.GType
import gobject.g_type_name
import kotlinx.cinterop.toKString

/**
 * 30 / 11 / 2021
 *
 * GType is an alias for a UInt, but because it has functions which take it,
 *  this class wraps a GType and so the functions can easily be accessed.
 *
 * @see <a href="https://docs.gtk.org/gobject/alias.Type.html">GType</a>
 */
class WrappedType(
	val gType: GType
) {
	val name: String?
		get() = g_type_name(gType)?.toKString()
}