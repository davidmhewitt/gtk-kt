package org.gtk.gobject

import gobject.GTypeClass_autoptr
import gobject.g_type_class_unref
import kotlinx.cinterop.pointed
import org.gtk.glib.UnrefMe

/**
 * 30 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gobject/struct.TypeClass.html">
 *     GTypeClass</a>
 */
class TypeClass(
	val typeClassPointer: GTypeClass_autoptr
) : UnrefMe {

	val gType: WrappedType
		get() = WrappedType(typeClassPointer.pointed.g_type)

	override fun unref() {
		g_type_class_unref(typeClassPointer)
	}

	companion object {
		inline fun GTypeClass_autoptr?.wrap() =
			this?.wrap()

		inline fun GTypeClass_autoptr.wrap() =
			TypeClass(this)
	}
}