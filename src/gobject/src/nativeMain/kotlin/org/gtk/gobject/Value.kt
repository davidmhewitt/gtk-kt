package org.gtk.gobject

import gobject.*
import kotlinx.cinterop.*
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.KGObject.Companion.wrap
import org.gtk.gobject.ParamSpec.Companion.wrap

/**
 * @see <a href="https://docs.gtk.org/gobject/struct.Value.html">GtkValue</a>
 */
class Value(val pointer: CPointer<GValue>) {
	constructor(type: KGType) :
			this(memScoped { g_value_init(alloc<GValue>().ptr, type.glib)!! })

	constructor(value: Int) : this(
		memScoped {
			val gValue = alloc<GValue>()
			g_value_init(gValue.ptr, KGType.INT.glib)
			g_value_set_int(gValue.ptr, value)
			gValue.ptr
		}
	)

	constructor(value: String) : this(
		memScoped {
			val gValue = alloc<GValue>()
			g_value_init(gValue.ptr, KGType.STRING.glib)
			g_value_set_string(gValue.ptr, value)
			gValue.ptr
		}
	)

	constructor(value: Boolean) : this(
		memScoped {
			val gValue = alloc<GValue>()
			g_value_init(gValue.ptr, KGType.BOOLEAN.glib)
			g_value_set_boolean(gValue.ptr, value.gtk)
			gValue.ptr
		}
	)

	fun copy(destValue: Value) {
		g_value_copy(pointer, destValue.pointer)
	}

	// TODO g_value_dup_boxed

	fun dupObject(): KGObject? =
		g_value_dup_object(pointer)?.reinterpret<GObject>().wrap()

	fun dupParam(): ParamSpec =
		g_value_dup_param(pointer)!!.wrap()

	fun dupString(): String =
		g_value_dup_string(pointer)!!.toKString()

	fun dupVariant(): Variant? =
		g_value_dup_variant(pointer).wrap()

	var boolean: Boolean
		get() = g_value_get_boolean(pointer).bool
		set(value) = g_value_set_boolean(pointer, value.gtk)

	// TODO g_value_get_boxed
	// TODO g_value_set_boxed

	// g_value_get_char && g_value_set_char is ignored due to being buggy

	var double: Double
		get() = g_value_get_double(pointer)
		set(value) = g_value_set_double(pointer, value)

	var enum: Int
		get() = g_value_get_enum(pointer)
		set(value) = g_value_set_enum(pointer, value)

	var flags: UInt
		get() = g_value_get_flags(pointer)
		set(value) = g_value_set_flags(pointer, value)

	var float: Float
		get() = g_value_get_float(pointer)
		set(value) = g_value_set_float(pointer, value)

	var gType: KGType
		get() = KGType.valueOf(g_value_get_gtype(pointer))
		set(value) = g_value_set_gtype(pointer, value.glib)

	var int: Int
		get() = g_value_get_int(pointer)
		set(value) = g_value_set_int(pointer, value)

	var long: Long
		get() = g_value_get_int64(pointer)
		set(value) = g_value_set_int64(pointer, value)

	var kgObject: KGObject
		get() = g_value_get_object(pointer)!!.reinterpret<GObject>().wrap()
		set(value) = g_value_set_object(pointer, value.pointer)

	var param: ParamSpec
		get() = g_value_get_param(pointer)!!.wrap()
		set(value) = g_value_set_param(pointer, value.paramSpecPointer)

	var char: Byte
		get() = g_value_get_schar(pointer)
		set(value) = g_value_set_schar(pointer, value)

	var string: String
		get() = g_value_get_string(pointer)!!.toKString()
		set(value) = g_value_set_string(pointer, value)

	var uChar: UByte
		get() = g_value_get_uchar(pointer)
		set(value) = g_value_set_uchar(pointer, value)

	var uInt: UInt
		get() = g_value_get_uint(pointer)
		set(value) = g_value_set_uint(pointer, value)

	var uLong: ULong
		get() = g_value_get_uint64(pointer)
		set(value) = g_value_set_uint64(pointer, value)

	var variant: Variant?
		get() = g_value_get_variant(pointer).wrap()
		set(value) = g_value_set_variant(pointer, value?.variantPointer)

	fun reset(): Value =
		g_value_reset(pointer)!!.wrap()

	// TODO g_value_set_boxed

	// ignore g_value_set_boxed_take_ownership
	// ignore g_value_set_interned_string as that is a C specific
	// ignore g_value_set_object_take_ownership as that is C specific
	// ignore g_value_set_param_take_ownership as that is C specific
	// ignore g_value_set_static_boxed as that is C specific
	// ignore g_value_set_static_string as that is C specific
	// ignore g_value_set_string_take_ownership as that is C specific

	// TODO g_value_take_boxed

	fun take(obj: KGObject) {
		g_value_take_object(pointer, obj.pointer)
	}

	fun take(param: ParamSpec) {
		g_value_take_param(pointer, param.paramSpecPointer)
	}

	fun take(variant: Variant) {
		g_value_take_variant(pointer, variant.variantPointer)
	}

	fun transform(dest: Value): Boolean =
		g_value_transform(pointer, dest.pointer).bool

	fun unset() {
		g_value_unset(pointer)
	}

	companion object {
		fun compatible(src: KGType, dest: KGType): Boolean =
			g_value_type_compatible(src.glib, dest.glib).bool

		fun transformable(src: KGType, dest: KGType): Boolean =
			g_value_type_transformable(src.glib, dest.glib).bool

		inline fun CPointer<GValue>?.wrap() =
			this?.wrap()

		inline fun CPointer<GValue>.wrap() =
			Value(this)
	}
}