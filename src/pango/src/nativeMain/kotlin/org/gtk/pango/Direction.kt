package org.gtk.pango

import kotlinx.cinterop.internal.ConstantValue.UInt
import pango.PangoDirection
import pango.PangoDirection.*

/**
 * kotlinx-gtk
 *
 * 15 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/Pango/enum.Direction.html">
 *     PangoDirection</a>
 */
enum class Direction(val pango: PangoDirection) {
	LTR(PANGO_DIRECTION_LTR),
	RTL(PANGO_DIRECTION_RTL),
	TTB_LTR(PANGO_DIRECTION_TTB_LTR),
	TTB_RTL(PANGO_DIRECTION_TTB_RTL),
	WEAK_LTR(PANGO_DIRECTION_WEAK_LTR),
	WEAK_RTL(PANGO_DIRECTION_WEAK_RTL),
	NEUTRAL(PANGO_DIRECTION_NEUTRAL);

	companion object {
		fun valueOf(pango: PangoDirection): Direction =
			when (pango) {
				PANGO_DIRECTION_LTR -> LTR
				PANGO_DIRECTION_RTL -> RTL
				PANGO_DIRECTION_TTB_LTR -> TTB_LTR
				PANGO_DIRECTION_TTB_RTL -> TTB_RTL
				PANGO_DIRECTION_WEAK_LTR -> WEAK_LTR
				PANGO_DIRECTION_WEAK_RTL -> WEAK_RTL
				PANGO_DIRECTION_NEUTRAL -> NEUTRAL
			}
	}
}