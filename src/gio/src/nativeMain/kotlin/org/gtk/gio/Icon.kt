package org.gtk.gio

import gio.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.toKString
import org.gtk.glib.*
import org.gtk.glib.Variant.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 28 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/iface.Icon.html">GIcon</a>
 */
interface Icon {
	val iconPointer: GIcon_autoptr

	/**
	 * @see <a href="https://docs.gtk.org/gio/type_func.Icon.hash.html">
	 *     g_icon_hash</a>
	 */
	val hash: UInt
		get() = g_icon_hash(iconPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Icon.equal.html">
	 *     g_icon_equal</a>
	 */
	fun equal(other: Icon): Boolean =
		g_icon_equal(iconPointer, other.iconPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Icon.to_string.html">
	 *     g_icon_to_string</a>
	 */
	fun iconToString(): String? =
		g_icon_to_string(iconPointer)?.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Icon.serialize.html">
	 *     g_icon_serialize</a>
	 */
	fun serialize(): Variant? =
		g_icon_serialize(iconPointer)?.wrap()

	companion object {
		/**
		 * @see <a href="https://docs.gtk.org/gio/type_func.Icon.new_for_string.html">
		 *     g_icon_new_for_string</a>
		 */
		@Throws(KGError::class)
		fun newForString(string: String): Icon = memScoped {
			val err = allocateGErrorPtr()
			val result = g_icon_new_for_string(string, err)
			err.unwrap()
			result!!.wrap()
		}

		/**
		 * @see <a href="https://docs.gtk.org/gio/type_func.Icon.deserialize.html">
		 *     g_icon_deserialize</a>
		 */
		fun deserialize(variant: Variant): Icon? =
			g_icon_deserialize(variant.variantPointer)?.wrap()


		inline fun GIcon_autoptr?.wrap() =
			this?.wrap()

		fun GIcon_autoptr.wrap(): Icon =
			ImplIcon(this)

		internal class ImplIcon(override val iconPointer: CPointer<GIcon>) : Icon
	}
}

