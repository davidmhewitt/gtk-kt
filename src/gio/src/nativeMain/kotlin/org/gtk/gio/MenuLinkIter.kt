package org.gtk.gio

import gio.*
import glib.gcharVar
import kotlinx.cinterop.*
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 09 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.MenuLinkIter.html">
 *     GMenuLinkIter</a>
 */
class MenuLinkIter(val menuLinkIterPointer: GMenuLinkIter_autoptr) :
	KGObject(menuLinkIterPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuLinkIter.get_name.html">
	 *     g_menu_link_iter_get_name</a>
	 */
	val name: String
		get() = g_menu_link_iter_get_name(menuLinkIterPointer)!!.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuLinkIter.get_next.html">
	 *     g_menu_link_iter_get_next</a>
	 */
	fun getNext(nextLink: (name: String, value: MenuModel) -> Unit): Boolean {
		memScoped {
			val outLink = cValue<CPointerVar<gcharVar>>()
			val value = cValue<GMenuModel_autoptrVar>()

			val result = g_menu_link_iter_get_next(
				menuLinkIterPointer,
				outLink,
				value
			).bool

			if (result) {
				nextLink(
					outLink.ptr.pointed.value!!.toKString(),
					value.ptr.pointed.value!!.wrap()
				)
			}

			return result
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuLinkIter.get_value.html">
	 *     g_menu_link_iter_get_value</a>
	 */
	val value: MenuModel
		get() = g_menu_link_iter_get_value(menuLinkIterPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuLinkIter.next.html">
	 *     g_menu_link_iter_next</a>
	 */
	fun next(): Boolean =
		g_menu_link_iter_next(menuLinkIterPointer).bool

	companion object {
		inline fun GMenuLinkIter_autoptr?.wrap() =
			this?.wrap()

		inline fun GMenuLinkIter_autoptr.wrap() =
			MenuLinkIter(this)
	}
}