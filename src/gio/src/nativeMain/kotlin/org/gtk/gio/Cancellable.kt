package org.gtk.gio

import gio.*
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.PollFD
import org.gtk.glib.Source
import org.gtk.glib.Source.Companion.wrap
import org.gtk.glib.asStablePointer
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.staticDestroyStableRefFunction

/**
 * gtk-kt
 *
 * 26 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.Cancellable.html">
 *     GCancellable</a>
 */
class Cancellable(val cancellablePointer: GCancellable_autoptr) :
	KGObject(cancellablePointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.Cancellable.new.html">
	 *     g_cancellable_new</a>
	 */
	constructor() : this(g_cancellable_new()!!)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.cancel.html">
	 *     g_cancellable_cancel</a>
	 */
	fun cancel() {
		g_cancellable_cancel(cancellablePointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.connect.html">
	 *     g_cancellable_connect</a>
	 */
	fun connect(callback: () -> Unit): ULong =
		g_cancellable_connect(
			cancellablePointer,
			staticCancelledCallback,
			callback.asStablePointer(),
			staticDestroyStableRefFunction
		)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.disconnect.html">
	 *     g_cancellable_disconnect</a>
	 */
	fun disconnect(handleId: ULong) {
		g_cancellable_disconnect(cancellablePointer, handleId)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.get_fd.html">
	 *     g_cancellable_get_fd</a>
	 */
	val fd: Int
		get() = g_cancellable_get_fd(cancellablePointer)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.is_cancelled.html">
	 *     g_cancellable_is_cancelled</a>
	 */
	val isCancelled: Boolean
		get() = g_cancellable_is_cancelled(cancellablePointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.make_pollfd.html">
	 *     g_cancellable_make_pollfd</a>
	 */
	fun makePollFD(pollFD: PollFD) =
		g_cancellable_make_pollfd(cancellablePointer, pollFD.pollFDPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.pop_current.html">
	 *     g_cancellable_pop_current</a>
	 */
	fun popCurrent() {
		g_cancellable_pop_current(cancellablePointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.push_current.html">
	 *     g_cancellable_push_current</a>
	 */
	fun pushCurrent() {
		g_cancellable_push_current(cancellablePointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.release_fd.html">
	 *     g_cancellable_release_fd</a>
	 */
	fun releaseFD() {
		g_cancellable_release_fd(cancellablePointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.reset.html">
	 *     g_cancellable_reset</a>
	 */
	fun reset() {
		g_cancellable_reset(cancellablePointer)
	}

	/*
	Ignored due to not quite understanding how this can be used
	fun setErrorIfCancelled()
	https://docs.gtk.org/gio/method.Cancellable.set_error_if_cancelled.html
	*/

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.Cancellable.source_new.html">
	 *     g_cancellable_source_new</a>
	 */
	fun sourceNew(): Source =
		g_cancellable_source_new(cancellablePointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/signal.Cancellable.cancelled.html">
	 *     cancelled</a>
	 */
	fun addOnCancelledCallback(action: () -> Unit) =
		addSignalCallback(Signals.CANCELLED, action, staticCancelledCallback)

	companion object {
		private val staticCancelledCallback: GCallback =
			staticCFunction { _: GCancellable_autoptr, data: gpointer ->
				data.asStableRef<() -> Unit>().get().invoke()
				Unit
			}.reinterpret()

		val current: Cancellable?
			get() = g_cancellable_get_current().wrap()

		inline fun GCancellable_autoptr?.wrap() =
			this?.wrap()

		inline fun GCancellable_autoptr.wrap() =
			Cancellable(this)
	}
}