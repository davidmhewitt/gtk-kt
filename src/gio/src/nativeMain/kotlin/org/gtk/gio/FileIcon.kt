package org.gtk.gio

import gio.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.File.Companion.wrap
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 26 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.FileIcon.html">GFileIcon</a>
 */
class FileIcon(val fileIconPointer: GFileIcon_autoptr) :
	KGObject(fileIconPointer.reinterpret()), Icon, LoadableIcon {
	override val loadableIconPointer: GLoadableIcon_autoptr by lazy {
		fileIconPointer.reinterpret()
	}

	override val iconPointer: CPointer<GIcon> by lazy {
		fileIconPointer.reinterpret()
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/ctor.FileIcon.new.html">
	 *     g_file_icon_new</a>
	 */
	@Throws(FileIconCreateConstructor::class)
	constructor(file: File) : this(
		g_file_icon_new(file.filePointer)
			?.reinterpret() ?: throw FileIconCreateConstructor()
	)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.FileIcon.get_file.html">
	 *     g_file_icon_get_file</a>
	 */
	val file: File
		get() = g_file_icon_get_file(fileIconPointer)!!.wrap()


	/**
	 * Thrown when [g_file_icon_new] returns null in the constructor of [FileIcon]
	 */
	class FileIconCreateConstructor(
	) : Exception("g_file_icon_new returned null")
}