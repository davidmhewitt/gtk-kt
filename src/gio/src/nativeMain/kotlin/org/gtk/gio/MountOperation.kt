package org.gtk.gio

import gio.GMountOperation_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.MountOperation.html">
 *     GMountOperation</a>
 */
open class MountOperation(
	val gMountOperationPointer: GMountOperation_autoptr,
) : KGObject(gMountOperationPointer.reinterpret()) {
}