package org.gtk.gio

import gio.*
import glib.GError
import glib.GVariantDict
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.*
import org.gtk.gio.ApplicationCommandLine.Companion.wrap
import org.gtk.gio.DBusConnection.Companion.wrap
import org.gtk.gio.File.Companion.toCArray
import org.gtk.glib.*
import org.gtk.glib.VariantDictionary.Companion.wrap
import org.gtk.gobject.KGObject
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * kotlinx-gtk
 * 22 / 02 / 2021
 */
open class Application(
	private val gApplicationPointer: CPointer<GApplication>
) : KGObject(gApplicationPointer.reinterpret()), ActionMap {
	// TODO g_application_bind_busy_property
	// TODO g_application_unbind_busy_property

	var applicationID: String?
		get() = g_application_get_application_id(gApplicationPointer)?.toKString()
		set(value) = g_application_set_application_id(
			gApplicationPointer,
			value
		)

	var inactivityTimeout: UInt
		get() = g_application_get_inactivity_timeout(gApplicationPointer)
		set(value) = g_application_set_inactivity_timeout(
			gApplicationPointer,
			value
		)

	var flags: GApplicationFlags
		get() = g_application_get_flags(gApplicationPointer)
		set(value) = g_application_set_flags(gApplicationPointer, value)

	var resourceBasePath: String?
		get() = g_application_get_resource_base_path(gApplicationPointer)?.toKString()
		set(value) = g_application_set_resource_base_path(
			gApplicationPointer,
			value
		)
	val dbusConnection: DBusConnection?
		get() = g_application_get_dbus_connection(gApplicationPointer).wrap()
	val dbusObjectPath: String?
		get() = g_application_get_dbus_object_path(gApplicationPointer)?.toKString()
	val isRegistered: Boolean
		get() = g_application_get_is_registered(gApplicationPointer).bool
	val isRemote: Boolean
		get() = g_application_get_is_remote(gApplicationPointer).bool
	val isBusy: Boolean
		get() = g_application_get_is_busy(gApplicationPointer).bool


	fun addOnActivateCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
				Signals.ACTIVATE,
				action
		)

	fun addOnCommandLineCallback(action: ApplicationCommandLineFunction): SignalManager =
		addSignalCallback(
				Signals.COMMAND_LINE,
				action,
				staticCommandLineFunction
		)


	fun addOnHandleLocalOptionsCallback(action: ApplicationHandleLocalOptionsFunction): SignalManager =
		addSignalCallback(
				Signals.HANDLE_LOCAL_OPTIONS,
				action,
				staticHandleLocalOptionsFunction
		)


	fun addOnNameLostCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
				Signals.NAME_LOST,
				action
		)

	//TODO open signal

	fun addOnShutdownCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
				Signals.SHUTDOWN,
				action
		)

	fun addOnStartupCallback(action: () -> Unit): SignalManager =
		addSignalCallback(
				Signals.STARTUP,
				action
		)

	override val actionMapPointer: CPointer<GActionMap>
		get() = gApplicationPointer.reinterpret()

	constructor(applicationID: String?, flags: GApplicationFlags) : this(
		g_application_new(
			applicationID,
			flags
		)!!.reinterpret()
	)

	fun register(cancellable: KGCancellable): Boolean = memScoped {
		val err = allocPointerTo<GError>().ptr
		val r = g_application_register(
			gApplicationPointer,
			cancellable.cancellablePointer,
			err
		)
		err.unwrap()
		r.bool
	}

	fun hold() {
		g_application_hold(gApplicationPointer)
	}

	fun release() {
		g_application_release(gApplicationPointer)
	}

	fun quit() {
		g_application_quit(gApplicationPointer)
	}

	fun activate() {
		g_application_activate(gApplicationPointer)
	}

	fun open(files: Array<File>, hint: String = "") {
		g_application_open(
			gApplicationPointer,
			files.toCArray(),
			files.size,
			hint
		)
	}

	fun sendNotification(notification: Notification, id: String? = null) {
		g_application_send_notification(
			gApplicationPointer,
			id,
			notification.notificationPointer
		)
	}

	fun withdrawNotification(id: String) {
		g_application_withdraw_notification(gApplicationPointer, id)
	}

	fun run(argc: Int = 0, argv: Array<String> = arrayOf()): Int = memScoped {
		g_application_run(
			gApplicationPointer,
			argc,
			memScoped { argv.toCStringArray(this) })
	}

	fun addMainOptionEntries(entries: Array<OptionEntry>) {
		TODO("g_application_add_main_option_entries")
	}

	fun addMainOption(
		longName: String,
		shortName: Char,
		description: String,
		flags: OptionFlags = OptionFlags.NONE,
		arg: OptionArg = OptionArg.NONE,
		argDescription: String? = null
	) {
		g_application_add_main_option(
			gApplicationPointer,
			longName,
			shortName.code.toByte(),
			flags.gtk,
			arg.gio,
			description,
			argDescription
		)
	}

	fun addOptionGroup(optionGroup: OptionGroup) {
		g_application_add_option_group(
			gApplicationPointer,
			optionGroup.optionPointer
		)
	}

	fun setOptionContextParameterString(parameterString: String?) {
		g_application_set_option_context_parameter_string(
			gApplicationPointer,
			parameterString
		)
	}

	fun setOptionContextSummary(summary: String?) {
		g_application_set_option_context_summary(
			gApplicationPointer,
			summary
		)
	}

	fun setOptionContextDescription(description: String?) {
		g_application_set_option_context_description(
			gApplicationPointer,
			description
		)
	}

	fun setDefault() {
		g_application_set_default(gApplicationPointer)
	}

	fun markBusy() =
		g_application_mark_busy(gApplicationPointer)

	fun markNotBusy() =
		g_application_unmark_busy(gApplicationPointer)

	fun unmarkBusy() {
		g_application_unmark_busy(gApplicationPointer)
	}

	data class OpenEvent(
		val files: Sequence<File>,
		val hint: String,
	)

	companion object {
		private val staticCommandLineFunction: GCallback =
			staticCFunction { _: gpointer, commandLine: CPointer<GApplicationCommandLine>, data: gpointer ->
				data.asStableRef<ApplicationCommandLineFunction>().get().invoke(commandLine.wrap())
			}.reinterpret()

		private val staticHandleLocalOptionsFunction: GCallback =
			staticCFunction { _: gpointer, dict: CPointer<GVariantDict>, data: gpointer ->
				data.asStableRef<ApplicationHandleLocalOptionsFunction>().get().invoke(dict.wrap())
			}.reinterpret()

		val default: Application?
			get() = g_application_get_default().wrap()

		fun isIdValid(applicationID: String): Boolean =
			g_application_id_is_valid(applicationID).bool

		inline fun CPointer<GApplication>?.wrap() =
			this?.let { Application(it) }

		inline fun CPointer<GApplication>.wrap() =
			Application(this)
	}
}

typealias ApplicationCommandLineFunction = (ApplicationCommandLine) -> Int

typealias ApplicationHandleLocalOptionsFunction = (VariantDictionary) -> Int