package org.gtk.gio

import gio.*
import glib.gpointer
import gobject.GCallback
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gio.MenuAttributeIter.Companion.wrap
import org.gtk.gio.MenuLinkIter.Companion.wrap
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.VariantType
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * kotlinx-gtk
 *
 * 08 / 02 / 2021
 *
 * @see <a href="https://docs.gtk.org/gio/class.MenuModel.html">GMenuModel</a>
 */
abstract class MenuModel(
	val menuModelPointer: GMenuModel_autoptr
) : KGObject(menuModelPointer.reinterpret()) {

	/*
	https://docs.gtk.org/gio/method.MenuModel.get_item_attribute.html
	TODO Clarification on g_menu_model_get_item_attribute
	fun getItemAttribute(index: Int, attribute: String, formatString: String) :Boolean
	 */

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.get_item_attribute_value.html">
	 *     g_menu_model_get_item_attribute_value</a>
	 */
	fun getItemAttributeValue(
		itemIndex: Int,
		attribute: String,
		expectedType: VariantType? = null
	): Variant? =
		g_menu_model_get_item_attribute_value(
			menuModelPointer,
			itemIndex,
			attribute,
			expectedType?.variantTypePointer
		).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.get_item_link.html">
	 *     g_menu_model_get_n_items</a>
	 */
	fun getItemLink(itemIndex: Int, link: String): MenuModel? =
		g_menu_model_get_item_link(menuModelPointer, itemIndex, link).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.get_n_items.html"></a>
	 */
	val nItems: Int
		get() = g_menu_model_get_n_items(menuModelPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.is_mutable.html">
	 *     g_menu_model_is_mutable</a>
	 */
	val isMutable: Boolean
		get() = g_menu_model_is_mutable(menuModelPointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.items_changed.html">
	 *     g_menu_model_iterate_item_attributes</a>
	 */
	fun itemsChanged(position: Int, removed: Int, added: Int) {
		g_menu_model_items_changed(menuModelPointer, position, removed, added)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.iterate_item_attributes.html">
	 *     g_menu_model_iterate_item_attributes</a>
	 */
	fun iterateITemAttributes(itemIndex: Int): MenuAttributeIter =
		g_menu_model_iterate_item_attributes(menuModelPointer, itemIndex)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gio/method.MenuModel.iterate_item_links.html">
	 *     g_menu_model_iterate_item_links/a>
	 */
	fun iterateItemsLinks(itemIndex: Int): MenuLinkIter =
		g_menu_model_iterate_item_links(menuModelPointer, itemIndex)!!.wrap()


	data class ItemsChangedEvent(
		val position: Int,
		val removed: Int,
		val added: Int
	)

	/**
	 * @see <a href="https://docs.gtk.org/gio/signal.MenuModel.items-changed.html">
	 *     items-changed</a>
	 */
	fun addOnItemsChangedCallback(action: (ItemsChangedEvent) -> Unit): SignalManager =
		addSignalCallback(Signals.ITEMS_CHANGED, action, staticItemsChangedFunction)

	companion object {
		inline fun GMenuModel_autoptr?.wrap() =
			this?.wrap()

		fun GMenuModel_autoptr.wrap(): MenuModel =
			Impl(this)

		/**
		 * Impl classes are used to purely wrap pointers returned for [MenuModel]
		 */
		internal class Impl(menuModelPointer: GMenuModel_autoptr) :
			MenuModel(menuModelPointer)

		private val staticItemsChangedFunction: GCallback =
			staticCFunction { _: GMenuModel_autoptr, position: Int, removed: Int, added: Int, data: gpointer ->
				data.asStableRef<(ItemsChangedEvent) -> Unit>().get()
					.invoke(ItemsChangedEvent(position, removed, added))
				Unit
			}.reinterpret()
	}
}