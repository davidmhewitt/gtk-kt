package org.gtk.gio

import gio.GActionEntry
import gio.GSimpleAction
import glib.GVariant
import glib.gpointer
import kotlinx.cinterop.*

/**
 * While coding this class, Had issues with creating proper callbacks.
 *
 * Instead of actually wrapping the C function,
 * the C function calls the K function
 */
class ActionEntry(
	val actionEntryPointer: CPointer<GActionEntry>
) {
	var name: String
		get() = actionEntryPointer.pointed.name!!.toKString()
		set(value) {
			actionEntryPointer.pointed.name = memScoped {
				value.cstr.ptr
			}
		}
	var state: String?
		get() = actionEntryPointer.pointed.state?.toKString()
		set(value) {
			actionEntryPointer.pointed.state = memScoped {
				value?.cstr?.ptr
			}
		}
	var parameterType: String?
		get() = actionEntryPointer.pointed.parameter_type?.toKString()
		set(value) {
			actionEntryPointer.pointed.parameter_type = memScoped {
				value?.cstr?.ptr
			}
		}

	var activate: ActionMapEntryFunction = null

	var changeState: ActionMapEntryFunction = null

	init {
		actionEntryPointer.pointed.activate = activateCallback
		actionEntryPointer.pointed.change_state = changeStateCallback
	}

	constructor(
		name: String,
		onActivate: ActionMapEntryFunction = null,
		parameterType: String? = null,
		state: String? = null,
		changeState: ActionMapEntryFunction = null
	) : this(memScoped { cValue<GActionEntry>().ptr }) {
		this.name = name
		this.state = state
		this.parameterType = parameterType
		this.activate = onActivate
		this.changeState = changeState
	}

	companion object {
		val activateCallback =
			staticCFunction { action: CPointer<GSimpleAction>?, variant: CPointer<GVariant>?, userData: gpointer? ->
				userData?.asStableRef<NativeActionMapEntryFunction>()
					?.get()
					?.invoke(action, variant, 0)
				Unit
			}


		val changeStateCallback =
			staticCFunction { action: CPointer<GSimpleAction>?, variant: CPointer<GVariant>?, userData: gpointer? ->
				userData?.asStableRef<NativeActionMapEntryFunction>()
					?.get()
					?.invoke(action, variant, 1)
				Unit
			}

	}
}