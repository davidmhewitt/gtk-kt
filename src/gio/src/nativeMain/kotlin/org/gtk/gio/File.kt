package org.gtk.gio

import gio.*
import glib.GError
import glib.g_free
import kotlinx.cinterop.*
import org.gtk.gio.FileEnumerator.Companion.wrap
import org.gtk.glib.*
import org.gtk.gobject.KGObject

/**
 * kotlinx-gtk
 * 08 / 03 / 2021
 */
class File(
	val filePointer: GFile_autoptr
) : KGObject(filePointer.reinterpret()) {

	fun peekPath(): String? =
		g_file_peek_path(filePointer)?.toKString()

	@Throws(KGError::class)
	fun loadContents(cancellable: KGCancellable? = null): String? {
		memScoped {
			val contents: CValue<CPointerVarOf<CStringPointer>> = cValue()
			val cLength = cValue<ULongVar>()
			val err = allocPointerTo<GError>().ptr

			val r = g_file_load_contents(
				filePointer,
				cancellable?.cancellablePointer,
				contents,
				cLength,
				null,
				err
			).bool
			err.unwrap()
			return if (r) {
				val builder = StringBuilder()
				for (index in 0 until cLength.ptr.pointed.value.toInt())
					builder.append(contents.ptr[index]?.toKString()).append("\n")
				g_free(contents.ptr)
				builder.toString()
			} else null
		}
	}

	val baseName: String?
		get() = g_file_get_basename(filePointer)?.use { it.toKString() }

	fun enumerateChildren(): FileEnumerator? {
		return g_file_enumerate_children(filePointer, null, 0u, null, null).wrap()
	}

	companion object {
		inline fun GFile_autoptr?.wrap() =
			this?.wrap()

		inline fun GFile_autoptr.wrap() =
			File(this)

		fun Array<File>.toCArray(): CPointer<CPointerVar<GFile>> =
			memScoped {
				allocArrayOf(this@toCArray.map { it.filePointer })
			}

		fun newForPath(path: String): File? {
			return g_file_new_for_path(path).wrap()
		}
	}
}