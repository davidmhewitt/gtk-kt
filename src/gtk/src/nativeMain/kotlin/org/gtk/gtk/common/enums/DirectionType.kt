package org.gtk.gtk.common.enums

import glib.gpointer
import gobject.GCallback
import gtk.GtkDirectionType
import gtk.GtkDirectionType.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction

/**
 * kotlinx-gtk
 * 06 / 03 / 2021
 *
 * @see <a href="https://developer.gnome.org/gtk3/stable/gtk3-Standard-Enumerations.html#GtkDirectionType">
 *     GtkDirectionType</a>
 */
enum class DirectionType(val gtk: GtkDirectionType) {
	FORWARD(GTK_DIR_TAB_FORWARD),
	BACKWARD(GTK_DIR_TAB_BACKWARD),
	UP(GTK_DIR_UP),
	DOWN(GTK_DIR_DOWN),
	LEFT(GTK_DIR_LEFT),
	RIGHT(GTK_DIR_RIGHT);

	companion object {

		fun valueOf(gtk: GtkDirectionType) =
			when (gtk) {
				GTK_DIR_TAB_FORWARD -> FORWARD
				GTK_DIR_TAB_BACKWARD -> BACKWARD
				GTK_DIR_UP -> UP
				GTK_DIR_DOWN -> DOWN
				GTK_DIR_LEFT -> LEFT
				GTK_DIR_RIGHT -> RIGHT
			}

		/** Generic callback for signals */
		@Deprecated("Use self refrence")
		val staticDirectionTypeCallback: GCallback =
			staticCFunction { _: gpointer, direction: GtkDirectionType, data: gpointer ->
				data.asStableRef<(DirectionType) -> Unit>().get().invoke(valueOf(direction))
				Unit
			}.reinterpret()
	}
}