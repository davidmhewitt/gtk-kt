package org.gtk.gtk

import gio.GListModel
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 19 / 09 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SliceListModel.html">GtkSliceListModel</a>
 */
class SliceListModel(val sliceListModelPointer: CPointer<GtkSliceListModel>) :
	KGObject(sliceListModelPointer.reinterpret()), ListModel {

	override val listModelPointer: CPointer<GListModel> by lazy { sliceListModelPointer.reinterpret() }

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.SliceListModel.new.html">gtk_slice_list_model_new</a>
	 */
	constructor(model: ListModel?, offset: UInt, size: UInt) : this(
		gtk_slice_list_model_new(
			model?.listModelPointer,
			offset,
			size
		)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SliceListModel.get_model.html">gtk_slice_list_model_get_model</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SliceListModel.set_model.html">gtk_slice_list_model_set_model</a>
	 */
	var model: ListModel?
		get() = gtk_slice_list_model_get_model(sliceListModelPointer).wrap()
		set(value) = gtk_slice_list_model_set_model(sliceListModelPointer, value?.listModelPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SliceListModel.get_offset.html">gtk_slice_list_model_get_offset</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SliceListModel.set_offset.html">gtk_slice_list_model_set_offset</a>
	 */
	var offset: UInt
		get() = gtk_slice_list_model_get_offset(sliceListModelPointer)
		set(value) = gtk_slice_list_model_set_offset(sliceListModelPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SliceListModel.get_size.html">gtk_slice_list_model_get_size</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SliceListModel.set_size.html">gtk_slice_list_model_set_size</a>
	 */
	var size: UInt
		get() = gtk_slice_list_model_get_size(sliceListModelPointer)
		set(value) = gtk_slice_list_model_set_size(sliceListModelPointer, value)
}