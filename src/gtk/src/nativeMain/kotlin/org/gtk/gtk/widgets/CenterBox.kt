package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.common.enums.BaselinePosition

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CenterBox.html">
 *     GtkCenterBox</a>
 */
class CenterBox(
	val centerBoxPointer: CPointer<GtkCenterBox>,
) : Widget(centerBoxPointer.reinterpret()) {

	constructor() : this(gtk_center_box_new()!!.reinterpret())

	var baselinePosition: BaselinePosition
		get() = BaselinePosition.valueOf(gtk_center_box_get_baseline_position(centerBoxPointer))
		set(value) = gtk_center_box_set_baseline_position(centerBoxPointer, value.gtk)

	var centerWidget: Widget
		get() = gtk_center_box_get_center_widget(centerBoxPointer)!!.wrap()
		set(value) = gtk_center_box_set_center_widget(centerBoxPointer, value.widgetPointer)

	var endWidget: Widget
		get() = gtk_center_box_get_end_widget(centerBoxPointer)!!.wrap()
		set(value) = gtk_center_box_set_end_widget(centerBoxPointer, value.widgetPointer)

	var startWidget: Widget
		get() = gtk_center_box_get_start_widget(centerBoxPointer)!!.wrap()
		set(value) = gtk_center_box_set_start_widget(centerBoxPointer, value.widgetPointer)

}