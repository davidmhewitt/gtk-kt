package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.SearchBar.html">
 *     GtkSearchBar</a>
 */
class SearchBar(
	val searchBarPointer: CPointer<GtkSearchBar>
) : Widget(searchBarPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SEARCH_BAR))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.SearchBar.new.html">
	 *     gtk_search_bar_new</a>
	 */
	constructor() : this(gtk_search_bar_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SearchBar.connect_entry.html">
	 *     gtk_search_bar_connect_entry</a>
	 */
	fun connectEntry(entry: Editable) {
		gtk_search_bar_connect_entry(searchBarPointer, entry.editablePointer)
	}

	var child: Widget?
		get() = gtk_search_bar_get_child(searchBarPointer).wrap()
		set(value) = gtk_search_bar_set_child(
			searchBarPointer,
			value?.widgetPointer
		)

	var keyCaptureWidget: Widget
		get() = gtk_search_bar_get_key_capture_widget(searchBarPointer)!!.wrap()
		set(value) = gtk_search_bar_set_key_capture_widget(
			searchBarPointer,
			value.widgetPointer
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SearchBar.get_search_mode.html">
	 *     gtk_search_bar_get_search_mode</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SearchBar.set_search_mode.html">
	 *     gtk_search_bar_set_search_mode</a>
	 */
	var searchMode: Boolean
		get() = gtk_search_bar_get_search_mode(searchBarPointer).bool
		set(value) = gtk_search_bar_set_search_mode(searchBarPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.SearchBar.get_show_close_button.html">
	 *     gtk_search_bar_get_show_close_button</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.SearchBar.set_show_close_button.html">
	 *     gtk_search_bar_set_show_close_button</a>
	 */
	var showCloseButton: Boolean
		get() = gtk_search_bar_get_show_close_button(searchBarPointer).bool
		set(value) = gtk_search_bar_set_show_close_button(searchBarPointer, value.gtk)

}