package org.gtk.gtk.widgets

import gtk.GtkTreeViewColumn_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 31 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.TreeViewColumn.html">
 *     GtkTreeViewColumn</a>
 */
class TreeViewColumn(
	val columnPointer: GtkTreeViewColumn_autoptr,
) : KGObject(columnPointer.reinterpret()) {

	companion object{
		inline fun GtkTreeViewColumn_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkTreeViewColumn_autoptr.wrap() =
			TreeViewColumn(this)
	}
}