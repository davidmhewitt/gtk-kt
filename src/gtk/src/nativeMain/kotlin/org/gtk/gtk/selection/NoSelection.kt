package org.gtk.gtk.selection

import gio.GListModel_autoptr
import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NoSelection.html">
 *     GtkNoSelection</a>
 */
class NoSelection(val noSelectionPointer: GtkNoSelection_autoptr) :
	KGObject(noSelectionPointer.reinterpret()), SelectionModel {

	override val selectionModelPointer: GtkSelectionModel_autoptr by lazy {
		noSelectionPointer.reinterpret()
	}

	override val listModelPointer: GListModel_autoptr by lazy {
		noSelectionPointer.reinterpret()
	}

	constructor(model: ListModel?) :
			this(gtk_no_selection_new(model?.listModelPointer)!!)

	var model: ListModel
		get() = gtk_no_selection_get_model(noSelectionPointer)!!.wrap()
		set(value) = gtk_no_selection_set_model(
			noSelectionPointer,
			value.listModelPointer
		)
}