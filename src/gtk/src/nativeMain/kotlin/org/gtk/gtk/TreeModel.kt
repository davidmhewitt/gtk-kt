package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.asSequence
import org.gtk.glib.bool
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * kotlinx-gtk
 * 13 / 03 / 2021
 */
interface TreeModel {
	val treeModelPointer: CPointer<GtkTreeModel>

	fun getPath(iter: TreeIter): TreePath =
		TreePath(gtk_tree_model_get_path(treeModelPointer, iter.treeIterPointer)!!)

	fun iterHasChild(iter: TreeIter): Boolean =
		gtk_tree_model_iter_has_child(treeModelPointer, iter.treeIterPointer).bool

	fun addOnRowChangedCallback(action: (RowChanged) -> Unit) =
		KGObject(treeModelPointer.reinterpret())
			.addSignalCallback(Signals.ROW_CHANGED, action, RowChanged.staticCallback)

	fun addOnRowDeletedCallback(action: (RowDeleted) -> Unit) =
		KGObject(treeModelPointer.reinterpret())
			.addSignalCallback(Signals.ROW_DELETED, action, RowDeleted.staticCallback)

	fun addOnRowHasChildToggledCallback(action: (RowHasChildToggled) -> Unit) =
		KGObject(treeModelPointer.reinterpret())
			.addSignalCallback(Signals.ROW_HAS_CHILD_TOGGLED, action, RowHasChildToggled.staticCallback)

	fun addOnRowInsertedCallback(action: (RowInserted) -> Unit) =
		KGObject(treeModelPointer.reinterpret())
			.addSignalCallback(Signals.ROW_INSERTED, action, RowInserted.staticCallback)

	fun addOnReorderedCallback(action: (RowsReordered) -> Unit) =
		KGObject(treeModelPointer.reinterpret())
			.addSignalCallback(Signals.ROWS_REORDERED, action, RowsReordered.staticCallback)

	fun getIter(path: TreePath): TreeIter = TreeIter(memScoped {
		val iter = cValue<GtkTreeIter>()
		gtk_tree_model_get_iter(treeModelPointer, iter.ptr, path.treePathPointer)
		iter.ptr
	})

	data class RowChanged(
		val path: TreePath,
		val iter: TreeIter,
	) {
		companion object {
			val staticCallback: GCallback =
				staticCFunction { _: gpointer?, path: CPointer<GtkTreePath>, iter: CPointer<GtkTreeIter>, data: gpointer? ->
					data?.asStableRef<(RowChanged) -> Unit>()?.get()
						?.invoke(
							RowChanged(
								TreePath(path),
								TreeIter(iter)
							)
						)
					Unit
				}.reinterpret()
		}
	}

	data class RowDeleted(
		val path: TreePath
	) {
		companion object {
			val staticCallback: GCallback =
				staticCFunction { _: gpointer?, path: CPointer<GtkTreePath>, data: gpointer? ->
					data?.asStableRef<(RowDeleted) -> Unit>()?.get()
						?.invoke(
							RowDeleted(TreePath(path))
						)
					Unit
				}.reinterpret()

		}
	}

	data class RowHasChildToggled(
		val path: TreePath,
		val iter: TreeIter
	) {
		companion object {
			val staticCallback: GCallback =
				staticCFunction { _: gpointer?, path: CPointer<GtkTreePath>, iter: CPointer<GtkTreeIter>, data: gpointer? ->
					data?.asStableRef<(RowHasChildToggled) -> Unit>()
						?.get()
						?.invoke(
							RowHasChildToggled(
								TreePath(path),
								TreeIter(iter)
							)
						)
					Unit
				}.reinterpret()
		}
	}

	data class RowInserted(
		val path: TreePath,
		val iter: TreeIter
	) {
		companion object {
			val staticCallback: GCallback =
				staticCFunction { _: gpointer?, path: CPointer<GtkTreePath>, iter: CPointer<GtkTreeIter>, data: gpointer? ->
					data?.asStableRef<(RowInserted) -> Unit>()?.get()
						?.invoke(
							RowInserted(
								TreePath(path),
								TreeIter(iter)
							)
						)
					Unit
				}.reinterpret()
		}
	}

	data class RowsReordered(
		val path: TreePath,
		val iter: TreeIter?,
		val newOrder: Sequence<Pair<Int, Int>>
	) {
		companion object {
			val staticCallback: GCallback =
				staticCFunction { _: gpointer?, path: CPointer<GtkTreePath>, iter: CPointer<GtkTreeIter>, newOrder: gpointer, data: gpointer? ->
					data?.asStableRef<(RowsReordered) -> Unit>()
						?.get()
						?.invoke(
							RowsReordered(
								TreePath(path),
								TreeIter(iter),
								newOrder.reinterpret<CPointerVar<IntVar>>()
									.asSequence()
									.mapIndexed { index, cPointer ->
										index to cPointer.pointed.value
									}
							)
						)
					Unit
				}.reinterpret()

		}
	}



	companion object {
		inline fun CPointer<GtkTreeModel>?.wrap() =
			this?.wrap()

		fun CPointer<GtkTreeModel>.wrap(): TreeModel =
			ImplTreeModel(this)
	}
}

internal class ImplTreeModel(override val treeModelPointer: CPointer<GtkTreeModel>) : TreeModel