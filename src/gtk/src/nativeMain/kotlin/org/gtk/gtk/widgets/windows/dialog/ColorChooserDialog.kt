package org.gtk.gtk.widgets.windows.dialog

import gtk.GTK_TYPE_COLOR_CHOOSER_DIALOG
import gtk.GtkColorChooser
import gtk.GtkColorChooserDialog_autoptr
import gtk.gtk_color_chooser_dialog_new
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.ColorChooser
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.Window

/**
 * kotlinx-gtk
 *
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ColorChooserDialog.html">
 *     GtkColorChooser</a>
 */
class ColorChooserDialog(
	val aboutDialogPointer: GtkColorChooserDialog_autoptr,
) : Dialog(aboutDialogPointer.reinterpret()), ColorChooser {

	override val colorChooserPointer: CPointer<GtkColorChooser> by lazy {
		aboutDialogPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_COLOR_CHOOSER_DIALOG))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ColorChooserDialog.new.html">
	 *     gtk_color_chooser_dialog_new</a>
	 */
	constructor(window: Window?, title: String?) : this(
		gtk_color_chooser_dialog_new(
			title,
			window?.windowPointer
		)!!.reinterpret()
	)
}