package org.gtk.gtk

import glib.gpointer
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.WrappedKList
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.glib.asStablePointer
import org.gtk.glib.gtk
import org.gtk.gobject.staticDestroyStableRefFunction
import org.gtk.gtk.CellArea.Companion.wrap
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gtk.cellrenderer.CellRenderer
import org.gtk.gtk.cellrenderer.CellRenderer.Companion.wrap

/**
 * @see <a href="https://docs.gtk.org/gtk4/iface.CellLayout.html">
 *     GtkCellLayout</a>
 */
interface CellLayout {

	val cellLayoutHolder: GtkCellLayout_autoptr

	fun addAttribute(cell: CellRenderer, attribute: String, column: Int) {
		gtk_cell_layout_add_attribute(
			cellLayoutHolder,
			cell.cellRendererPointer,
			attribute,
			column
		)
	}

	fun clear() {
		gtk_cell_layout_clear(cellLayoutHolder)
	}

	fun clearAttributes(cell: CellRenderer) {
		gtk_cell_layout_clear_attributes(
			cellLayoutHolder,
			cell.cellRendererPointer
		)
	}

	val area: CellArea?
		get() = gtk_cell_layout_get_area(cellLayoutHolder).wrap()

	val cells: WrappedKList<CellRenderer>
		get() = gtk_cell_layout_get_cells(cellLayoutHolder)!!
			.asWrappedKList(
				{ reinterpret<GtkCellRenderer>().wrap() },
				{ cellRendererPointer }
			)

	fun packEnd(cellRenderer: CellRenderer, expand: Boolean) {
		gtk_cell_layout_pack_end(
			cellLayoutHolder,
			cellRenderer.cellRendererPointer,
			expand.gtk
		)
	}

	fun packStart(cellRenderer: CellRenderer, expand: Boolean) {
		gtk_cell_layout_pack_start(
			cellLayoutHolder,
			cellRenderer.cellRendererPointer,
			expand.gtk
		)
	}

	fun reorder(cell: CellRenderer, position: Int) {
		gtk_cell_layout_reorder(
			cellLayoutHolder,
			cell.cellRendererPointer,
			position
		)
	}

	// ignore gtk_cell_layout_set_attributes due to vararg

	fun setCellDataFunc(
		renderer: CellRenderer,
		function: CellLayoutDataFunc?,
	) {
		if (function == null) {
			gtk_cell_layout_set_cell_data_func(
				cell_layout = cellLayoutHolder,
				cell = renderer.cellRendererPointer,
				func = null,
				func_data = null,
				destroy = null
			)
			return
		}

		gtk_cell_layout_set_cell_data_func(
			cell_layout = cellLayoutHolder,
			cell = renderer.cellRendererPointer,
			func = staticCellDataFunc,
			func_data = function.asStablePointer(),
			destroy = staticDestroyStableRefFunction
		)
	}

	companion object {
		private class Impl(
			override val cellLayoutHolder: GtkCellLayout_autoptr,
		) : CellLayout

		private val staticCellDataFunc: GtkCellLayoutDataFunc =
			staticCFunction {
					cellLayout: GtkCellLayout_autoptr?,
					cell: GtkCellRenderer_autoptr?,
					treeModel: GtkTreeModel_autoptr?,
					iter: GtkTreeIter_autoptr?,
					data: gpointer?,
				->
				data!!

				data.asStableRef<CellLayoutDataFunc>()
					.get()
					.invoke(
						Impl(cellLayout!!),
						CellRenderer(cell!!),
						treeModel!!.wrap(),
						TreeIter(iter!!)
					)
			}
	}
}

typealias CellLayoutDataFunc =
		CellLayout.(
			cell: CellRenderer,
			treeModel: TreeModel,
			iter: TreeIter,
		) -> Unit