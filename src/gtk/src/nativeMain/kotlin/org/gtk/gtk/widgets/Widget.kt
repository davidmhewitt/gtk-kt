package org.gtk.gtk.widgets

import glib.GList
import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.cairo.FontOptions
import org.gtk.cairo.FontOptions.Companion.wrap
import org.gtk.gdk.Clipboard
import org.gtk.gdk.Clipboard.Companion.wrap
import org.gtk.gdk.Cursor
import org.gtk.gdk.Cursor.Companion.wrap
import org.gtk.gdk.Display
import org.gtk.gdk.Display.Companion.wrap
import org.gtk.gdk.FrameClock
import org.gtk.gdk.FrameClock.Companion.wrap
import org.gtk.gio.ActionGroup
import org.gtk.glib.*
import org.gtk.glib.Variant.Companion.toCArray
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.gobject.*
import org.gtk.graphene.Matrix
import org.gtk.graphene.Matrix.Companion.wrap
import org.gtk.graphene.Point
import org.gtk.graphene.Point.Companion.wrap
import org.gtk.graphene.Rect
import org.gtk.graphene.Rect.Companion.wrap
import org.gtk.gsk.Transform
import org.gtk.gtk.*
import org.gtk.gtk.Accessible.Role
import org.gtk.gtk.ImplNative.Companion.wrap
import org.gtk.gtk.ImplRoot.Companion.wrap
import org.gtk.gtk.Settings.Companion.wrap
import org.gtk.gtk.StyleContext.Companion.wrap
import org.gtk.gtk.Tooltip.Companion.wrap
import org.gtk.gtk.common.data.Requisition
import org.gtk.gtk.common.data.Requisition.Companion.wrap
import org.gtk.gtk.common.enums.*
import org.gtk.gtk.controller.EventController
import org.gtk.gtk.manager.LayoutManager
import org.gtk.gtk.manager.LayoutManager.Companion.wrap
import org.gtk.pango.Context
import org.gtk.pango.Context.Companion.wrap
import org.gtk.pango.FontMap
import org.gtk.pango.FontMap.Companion.wrap
import org.gtk.pango.Layout
import org.gtk.pango.Layout.Companion.wrap

/**
 * kotlinx-gtk
 * 08 / 02 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Widget.html">GtkWidget</a>
 */
open class Widget(val widgetPointer: WidgetPointer) :
	KGObject(widgetPointer.reinterpret()),
	Accessible,
	Buildable,
	ConstraintTarget {

	override val accessiblePointer: CPointer<GtkAccessible> by lazy {
		widgetPointer.reinterpret()
	}

	override val constraintTargetPointer: CPointer<GtkConstraintTarget> by lazy {
		widgetPointer.reinterpret()
	}

	override val buildablePointer: GtkBuildable_autoptr by lazy {
		widgetPointer.reinterpret()
	}

	/*
	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Widget.activate_action.html"></a>
	 */
	fun activateAction(name: String, formatString: String) {} // Ignored due to vararg
	*/


	val isInDestruction: Boolean
		get() = gtk_widget_in_destruction(widgetPointer).bool

	var canFocus: Boolean
		get() = gtk_widget_get_can_focus(widgetPointer).bool
		set(value) = gtk_widget_set_can_focus(widgetPointer, value.gtk)

	var canTarget: Boolean
		get() = gtk_widget_get_can_target(widgetPointer).bool
		set(value) = gtk_widget_set_can_target(widgetPointer, value.gtk)

	var focusOnClick: Boolean
		get() = gtk_widget_get_focus_on_click(widgetPointer).bool
		set(value) = gtk_widget_set_focus_on_click(
			widgetPointer,
			value.gtk
		)

	val preferredSize: PreferredSize
		get() = memScoped {
			val min = cValue<GtkRequisition>()
			val nat = cValue<GtkRequisition>()

			gtk_widget_get_preferred_size(widgetPointer, min, nat)

			PreferredSize(
				min.ptr.wrap(),
				nat.ptr.wrap()
			)
		}

	var horizontalAlign: Align
		get() = Align.valueOf(gtk_widget_get_halign(widgetPointer))
		set(value) = gtk_widget_set_halign(widgetPointer, value.gtk)

	val hasDefault: Boolean
		get() = gtk_widget_has_default(widgetPointer).bool

	val hasFocus: Boolean
		get() = gtk_widget_has_focus(widgetPointer).bool

	var focusable: Boolean
		get() = gtk_widget_get_focusable(widgetPointer).bool
		set(value) = gtk_widget_set_focusable(widgetPointer, value.gtk)

	var hasTooltip: Boolean
		get() = gtk_widget_get_has_tooltip(widgetPointer).bool
		set(value) = gtk_widget_set_has_tooltip(widgetPointer, value.gtk)

	var horizontalExpand: Boolean
		get() = gtk_widget_get_hexpand(widgetPointer).bool
		set(value) =
			gtk_widget_set_hexpand(widgetPointer, value.gtk)

	var horizontalExpandSet: Boolean
		get() = gtk_widget_get_hexpand_set(widgetPointer).bool
		set(value) = gtk_widget_set_hexpand_set(widgetPointer, value.gtk)

	val isFocus: Boolean
		get() = gtk_widget_is_focus(widgetPointer).bool

	var marginStart: Int
		get() = gtk_widget_get_margin_start(widgetPointer)
		set(value) = gtk_widget_set_margin_start(widgetPointer, value)

	var marginEnd: Int
		get() = gtk_widget_get_margin_end(widgetPointer)
		set(value) = gtk_widget_set_margin_end(widgetPointer, value)

	var marginTop: Int
		get() = gtk_widget_get_margin_top(widgetPointer)
		set(value) = gtk_widget_set_margin_top(widgetPointer, value)

	var marginBottom: Int
		get() = gtk_widget_get_margin_bottom(widgetPointer)
		set(value) = gtk_widget_set_margin_bottom(widgetPointer, value)

	var name: String?
		get() = gtk_widget_get_name(widgetPointer)?.toKString()
		set(value) = gtk_widget_set_name(widgetPointer, value)

	val scaleFactor: Int
		get() = gtk_widget_get_scale_factor(widgetPointer)

	var sensitive: Boolean
		get() = gtk_widget_get_sensitive(widgetPointer).bool
		set(value) = gtk_widget_set_sensitive(widgetPointer, value.gtk)

	var parent: Widget?
		get() = gtk_widget_get_parent(widgetPointer)?.let { Widget(it) }
		set(value) = gtk_widget_set_parent(widgetPointer, value?.widgetPointer)

	var verticalAlign: Align
		get() = Align.valueOf(gtk_widget_get_valign(widgetPointer))
		set(value) = gtk_widget_set_valign(widgetPointer, value.gtk)

	var verticalExpand: Boolean
		get() = gtk_widget_get_vexpand(widgetPointer).bool
		set(value) = gtk_widget_set_vexpand(widgetPointer, value.gtk)

	var verticalExpandSet: Boolean
		get() = gtk_widget_get_vexpand_set(widgetPointer).bool
		set(value) = gtk_widget_set_vexpand_set(widgetPointer, value.gtk)

	val root: Root?
		get() = gtk_widget_get_root(widgetPointer).wrap()

	val native: Native?
		get() = gtk_widget_get_native(widgetPointer).wrap()

	val frameClock: FrameClock?
		get() = gtk_widget_get_frame_clock(widgetPointer).wrap()

	var direction: TextDirection
		get() = TextDirection.valueOf(gtk_widget_get_direction(widgetPointer))
		set(value) = gtk_widget_set_direction(widgetPointer, value.gtk)

	val pangoContext: Context
		get() = gtk_widget_get_pango_context(widgetPointer)!!.wrap()

	var fontOptions: FontOptions?
		get() = gtk_widget_get_font_options(widgetPointer).wrap()
		set(value) = gtk_widget_set_font_options(widgetPointer, value?.pointer)

	var fontMap: FontMap?
		get() = gtk_widget_get_font_map(widgetPointer).wrap()
		set(value) = gtk_widget_set_font_map(widgetPointer, value?.fontMapPointer)

	var cursor: Cursor?
		get() = gtk_widget_get_cursor(widgetPointer).wrap()
		set(value) = gtk_widget_set_cursor(widgetPointer, value?.cursorPointer)

	var isChildVisible: Boolean
		get() = gtk_widget_get_child_visible(widgetPointer).bool
		set(value) = gtk_widget_set_child_visible(widgetPointer, value.gtk)

	val settings: Settings
		get() = gtk_widget_get_settings(widgetPointer)!!.wrap()

	val clipboard: Clipboard
		get() = gtk_widget_get_clipboard(widgetPointer)!!.wrap()

	var cssClasses: List<String>
		get() = stringListFromNullTerminatedCStringListAndFree(
			gtk_widget_get_css_classes(
				widgetPointer
			)
		)
		set(value) = gtk_widget_set_css_classes(widgetPointer, value.toNullTermCStringArray())

	val cssName: String
		get() = gtk_widget_get_css_name(widgetPointer)!!.toKString()

	val primaryClipboard: Clipboard
		get() = gtk_widget_get_primary_clipboard(widgetPointer)!!.wrap()

	val display: Display
		get() = gtk_widget_get_display(widgetPointer)!!.wrap()

	var sizeRequest: Pair<Int, Int>
		get() = memScoped {
			val w = cValue<IntVar>()
			val h = cValue<IntVar>()
			gtk_widget_get_size_request(widgetPointer, w, h)
			w.ptr.pointed.value to h.ptr.pointed.value
		}
		set(value) = gtk_widget_set_size_request(widgetPointer, value.first, value.second)

	val mnemonicLabels: WrappedKList<Widget>
		get() = gtk_widget_list_mnemonic_labels(widgetPointer)!!.asMutableWidgetList()

	var tooltipMarkup: String?
		get() = gtk_widget_get_tooltip_markup(widgetPointer)?.toKString()
		set(value) = gtk_widget_set_tooltip_markup(widgetPointer, value)

	var tooltipText: String?
		get() = gtk_widget_get_tooltip_text(widgetPointer)?.toKString()
		set(value) = gtk_widget_set_tooltip_text(widgetPointer, value)

	val allocatedWidth: Int
		get() = gtk_widget_get_allocated_width(widgetPointer)

	val allocatedHeight: Int
		get() = gtk_widget_get_allocated_height(widgetPointer)

	val allocation: Allocation
		get() = memScoped {
			val p = cValue<GtkAllocation>()
			gtk_widget_get_allocation(widgetPointer, p)
			Allocation(p.ptr)
		}

	val width: Int
		get() = gtk_widget_get_width(widgetPointer)

	val height: Int
		get() = gtk_widget_get_height(widgetPointer)

	val allocatedBaseline: Int
		get() = gtk_widget_get_allocated_baseline(widgetPointer)

	val isSensitive: Boolean
		get() = gtk_widget_is_sensitive(widgetPointer).bool

	val isVisible: Boolean
		get() = gtk_widget_is_visible(widgetPointer).bool

	var visible: Boolean
		get() = gtk_widget_get_visible(widgetPointer).bool
		set(value) = gtk_widget_set_visible(widgetPointer, value.gtk)

	val hasVisibleFocus: Boolean
		get() = gtk_widget_has_visible_focus(widgetPointer).bool

	val isDrawable: Boolean
		get() = gtk_widget_is_drawable(widgetPointer).bool

	var receivesDefault: Boolean
		get() = gtk_widget_get_receives_default(widgetPointer).bool
		set(value) = gtk_widget_set_receives_default(widgetPointer, value.gtk)

	val realized: Boolean
		get() = gtk_widget_get_realized(widgetPointer).bool

	val mapped: Boolean
		get() = gtk_widget_get_mapped(widgetPointer).bool

	var opacity: Double
		get() = gtk_widget_get_opacity(widgetPointer)
		set(value) = gtk_widget_set_opacity(widgetPointer, value)

	val styleContext: StyleContext
		get() = gtk_widget_get_style_context(widgetPointer)!!.wrap()

	val firstChild: Widget?
		get() = gtk_widget_get_first_child(widgetPointer).wrap()

	var focusChild: Widget?
		get() = gtk_widget_get_focus_child(widgetPointer).wrap()
		set(value) = gtk_widget_set_focus_child(
			widgetPointer,
			value?.widgetPointer
		)

	val lastChild: Widget?
		get() = gtk_widget_get_last_child(widgetPointer).wrap()

	var layoutManager: LayoutManager?
		get() = gtk_widget_get_layout_manager(widgetPointer).wrap()
		set(value) = gtk_widget_set_layout_manager(
			widgetPointer,
			value?.layoutManagerPointer
		)

	val nextSibling: Widget?
		get() = gtk_widget_get_next_sibling(widgetPointer).wrap()

	val prevSibling: Widget?
		get() = gtk_widget_get_prev_sibling(widgetPointer).wrap()

	var overflow: Overflow
		get() = Overflow.valueOf(gtk_widget_get_overflow(widgetPointer))
		set(value) = gtk_widget_set_overflow(widgetPointer, value.gtk)

	val requestMode: SizeRequestMode
		get() = SizeRequestMode.valueOf(
			gtk_widget_get_request_mode(widgetPointer))

	val stateFlags: StateFlags
		get() = StateFlags.valueOf(
			gtk_widget_get_state_flags(widgetPointer))

	val shouldLayout: Boolean
		get() = gtk_widget_should_layout(widgetPointer).bool

	fun unparent() {
		gtk_widget_unparent(widgetPointer)
	}

	fun getAncestor(type: KGType): Widget? =
		gtk_widget_get_ancestor(widgetPointer, type.glib).wrap()

	fun isInAncestor(ancestor: Widget): Boolean =
		gtk_widget_is_ancestor(widgetPointer, ancestor.widgetPointer).bool

	fun unParent() {
		gtk_widget_unparent(widgetPointer)
	}

	fun contains(x: Double, y: Double): Boolean =
		gtk_widget_contains(widgetPointer, x, y).bool

	fun show() {
		gtk_widget_show(widgetPointer)
	}

	fun map() {
		gtk_widget_map(widgetPointer)
	}

	fun unmap() {
		gtk_widget_unmap(widgetPointer)
	}

	fun realize() {
		gtk_widget_realize(widgetPointer)
	}

	fun unrealize() {
		gtk_widget_unrealize(widgetPointer)
	}

	fun queueDraw() {
		gtk_widget_queue_draw(widgetPointer)
	}

	fun queueResize() {
		gtk_widget_queue_resize(widgetPointer)
	}

	fun queueAllocate() {
		gtk_widget_queue_allocate(widgetPointer)
	}

	fun addTickCallback(callback: TickCallback): UInt =
		gtk_widget_add_tick_callback(
			widgetPointer,
			staticTickCallback,
			callback.asStablePointer(),
			staticDestroyStableRefFunction
		)

	fun removeTickCallback(id: UInt) {
		gtk_widget_remove_tick_callback(widgetPointer, id)
	}

	fun sizeAllocate(allocation: Allocation, baseline: Int) {
		gtk_widget_size_allocate(
			widgetPointer,
			allocation.allocationPointer,
			baseline
		)
	}

	fun allocate(
		width: Int,
		height: Int,
		baseline: Int = -1,
		transform: Transform? = null,
	) {
		gtk_widget_allocate(
			widgetPointer,
			width,
			height,
			baseline,
			transform?.transformPointer
		)
	}

	fun activate(): Boolean =
		gtk_widget_activate(widgetPointer).bool

	fun grabFocus(): Boolean =
		gtk_widget_grab_focus(widgetPointer).bool

	fun getAncestor(widgetType: ULong) =
		gtk_widget_get_ancestor(widgetPointer, widgetType)?.let { Widget(it) }

	fun computeExpand(orientation: Orientation): Boolean =
		gtk_widget_compute_expand(widgetPointer, orientation.gtk).bool

	fun computeBounds(target: Widget): Rect? = memScoped {
		val rect = cValue<graphene_rect_t>()
		val r = gtk_widget_compute_bounds(
			widgetPointer,
			target.widgetPointer,
			rect
		).bool

		return if (r)
			rect.ptr.wrap()
		else null
	}

	fun computePoint(target: Widget, point: Point): Point? = memScoped {
		val outPoint = cValue<graphene_point_t>()

		val r = gtk_widget_compute_point(
			widgetPointer,
			target.widgetPointer,
			point.pointPointer,
			outPoint
		).bool

		return if (r) {
			outPoint.ptr.wrap()
		} else {
			null
		}
	}

	fun computeTransform(target: Widget): Matrix? = memScoped {
		val matrix = cValue<graphene_matrix_t>()
		val r = gtk_widget_compute_transform(widgetPointer,
			target = target.widgetPointer,
			out_transform = matrix).bool

		return if (r)
			matrix.ptr.wrap()
		else null
	}

	fun initTemplate() {
		gtk_widget_init_template(widgetPointer)
	}

	fun isAncestor(widget: Widget): Boolean =
		gtk_widget_is_ancestor(widgetPointer, widget.widgetPointer).bool

	fun translateCoordinates(
		destination: Widget,
		srcX: Double,
		srcY: Double,
	): Pair<Double, Double>? =
		memScoped {
			val destY = cValue<DoubleVar>()
			val destX = cValue<DoubleVar>()
			if (gtk_widget_translate_coordinates(
					widgetPointer,
					destination.widgetPointer,
					srcX,
					srcY,
					destX,
					destY
				).bool
			)
				destX.ptr.pointed.value to destY.ptr.pointed.value
			else
				null
		}

	fun addController(controller: EventController) {
		gtk_widget_add_controller(
			widgetPointer,
			controller.eventControllerPointer
		)
	}

	fun removeController(controller: EventController) {
		gtk_widget_remove_controller(
			widgetPointer,
			controller.eventControllerPointer
		)
	}

	fun removeCssClass(cssClass: String) {
		gtk_widget_remove_css_class(widgetPointer, cssClass)
	}

	fun createPangoContext(): Context =
		gtk_widget_create_pango_context(widgetPointer)!!.wrap()

	fun createPangoLayout(text: String? = null): Layout =
		gtk_widget_create_pango_layout(widgetPointer, text)!!.wrap()

	fun setCursorFromName(name: String) {
		gtk_widget_set_cursor_from_name(widgetPointer, name)
	}

	fun mnemonicActivate(groupCycling: Boolean): Boolean =
		gtk_widget_mnemonic_activate(widgetPointer, groupCycling.gtk).bool

	fun activateAction(name: String, formatString: String?): Boolean =
		gtk_widget_activate_action(widgetPointer, name, formatString).bool

	fun activateActionVariant(name: String, vararg args: Variant): Boolean =
		gtk_widget_activate_action_variant(
			widgetPointer,
			name,
			args.toCArray().pointed.value
		).bool

	fun activateDefault() {
		gtk_widget_activate_default(widgetPointer)
	}

	fun addCSSClass(cssClass: String) {
		gtk_widget_add_css_class(widgetPointer, cssClass)
	}

	fun childFocus(direction: DirectionType) {
		gtk_widget_child_focus(widgetPointer, direction.gtk)
	}

	fun addMnemonicLabel(label: Widget) {
		gtk_widget_add_mnemonic_label(widgetPointer, label.widgetPointer)
	}

	fun removeMnemonicLabel(label: Widget) {
		gtk_widget_remove_mnemonic_label(widgetPointer, label.widgetPointer)
	}

	fun errorBell() {
		gtk_widget_error_bell(widgetPointer)
	}

	fun keynavFailed(direction: DirectionType): Boolean =
		gtk_widget_keynav_failed(widgetPointer, direction.gtk).bool

	fun triggerTooltipQuery() {
		gtk_widget_trigger_tooltip_query(widgetPointer)
	}

	fun getSize(orientation: Orientation) =
		gtk_widget_get_size(widgetPointer, orientation.gtk)

	fun setStateFlags(flags: StateFlags, clear: Boolean) {
		gtk_widget_set_state_flags(widgetPointer, flags.gtk, clear.gtk)
	}

	fun unsetStateFlags(flags: StateFlags) {
		gtk_widget_unset_state_flags(widgetPointer, flags.gtk)
	}

	fun dragCheckThreshold(
		startX: Int,
		startY: Int,
		currentX: Int,
		currentY: Int,
	): Boolean =
		gtk_drag_check_threshold(
			widgetPointer,
			startX,
			startY,
			currentX,
			currentY
		).bool

	fun insertActionGroup(name: String, group: ActionGroup) {
		gtk_widget_insert_action_group(widgetPointer, name, group.actionGroupPointer)
	}

	fun insertAfter(widget: Widget, previousSibling: Widget) {
		gtk_widget_insert_after(
			widget.widgetPointer,
			widgetPointer,
			previousSibling.widgetPointer
		)
	}

	fun insertBefore(widget: Widget, nextSibling: Widget) {
		gtk_widget_insert_before(widget.widgetPointer,
			parent = widgetPointer,
			next_sibling = nextSibling.widgetPointer)
	}

	fun actionSetEnabled(action: String, enabled: Boolean) {
		gtk_widget_action_set_enabled(widgetPointer, action, enabled.gtk)
	}

	fun addOnDestroyCallback(action: Widget.() -> Unit) =
		addSignalCallback(Signals.DESTROY, action, staticNoArgGCallback)

	fun addOnDirectionChangedCallback(action: (TextDirection) -> Unit) =
		addSignalCallback(
			Signals.DIRECTION_CHANGED,
			action,
			staticDirectionChangedCallback
		)

	fun addOnHideCallback(action: Widget.() -> Unit) =
		addSignalCallback(Signals.HIDE, action, staticNoArgGCallback)

	fun setKeynavFailedCallback(action: KeynavFailedFunction) =
		addSignalCallback(Signals.KEYNAV_FAILED, action, staticKeynavFailedFunction)

	fun setMapCallback(action: MapFunction) =
		addSignalCallback(Signals.MAP, action, staticMapFunction)

	fun setMnemonicActivateCallback(action: MnemonicActivateFunction) =
		addSignalCallback(
			Signals.MNEMONIC_ACTIVATE,
			action,
			staticMnemonicActivateFunction
		)

	fun addOnMoveFocusCallback(action: WidgetMoveFocusFunction) =
		addSignalCallback(Signals.MOVE_FOCUS, action, staticMoveFocusFunction)

	fun setQueryTooltipCallback(action: QueryTooltipFunction) =
		addSignalCallback(Signals.QUERY_TOOLTIP, action, staticQueryTooltipFunction)

	fun addOnRealizeCallback(action: Widget.() -> Unit) =
		addSignalCallback(Signals.REALIZE, action, staticNoArgGCallback)

	fun addOnShowCallback(action: Widget.() -> Unit) =
		addSignalCallback(Signals.SHOW, action, staticNoArgGCallback)

	fun addOnStateFlagsChangedCallback(action: (StateFlags) -> Unit) =
		addSignalCallback(
			Signals.STATE_FLAGS_CHANGED,
			action,
			staticStateFlagsChangedCallback
		)

	fun addOnUnmapCallback(action: Widget.() -> Unit) =
		addSignalCallback(Signals.UNMAP, action, staticNoArgGCallback)

	fun addOnUnrealizeCallback(action: Widget.() -> Unit) =
		addSignalCallback(Signals.UNREALIZE, action, staticNoArgGCallback)

	fun getTemplateChild(widget: KGType, name: String): KGObject =
		gtk_widget_get_template_child(widgetPointer, widget.glib, name)!!.wrap()

	fun hasCssClass(cssClass: String): Boolean =
		gtk_widget_has_css_class(widgetPointer, cssClass).bool

	fun hide() {
		gtk_widget_hide(widgetPointer)
	}

	fun measure(orientation: Orientation, forSize: Int): LayoutManager.MeasureResult =
		memScoped {
			val minimum = cValue<IntVar>()
			val natural = cValue<IntVar>()
			val minimumBaseline = cValue<IntVar>()
			val naturalBaseline = cValue<IntVar>()

			gtk_widget_measure(
				widgetPointer,
				orientation.gtk,
				forSize,
				minimum,
				natural,
				minimumBaseline,
				naturalBaseline
			)

			LayoutManager.MeasureResult(
				minimum.ptr.pointed.value,
				natural.ptr.pointed.value,
				minimumBaseline.ptr.pointed.value,
				naturalBaseline.ptr.pointed.value,
			)
		}


	/*
	Ignored gtk_widget_observe_children & gtk_widget_observe_controllers
	as gtk documentation states these functions should be ignored
	 */

	fun pick(x: Double, y: Double, pickFlags: @PickFlags UInt): Widget? =
		gtk_widget_pick(widgetPointer, x, y, pickFlags).wrap()

	fun snapshotChild(child: Widget, snapshot: Snapshot) {
		gtk_widget_snapshot_child(
			widgetPointer,
			child.widgetPointer,
			snapshot.snapshotPointer
		)
	}

	data class PreferredSize(
		val minimmum: Requisition,
		val maximum: Requisition,
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/class.Widget.html#class-methods">
	 *     Class methods</a>
	 * @see <a href="https://docs.gtk.org/gtk4/class.Widget.html#virtual-methods">
	 *     Virtual methods</a>
	 */
	class Class(
		val classPointer: CPointer<GtkWidgetClass>,
	) {
		var accessibleRole: Role
			get() = Role.valueOf(gtk_widget_class_get_accessible_role(classPointer))
			set(value) = gtk_widget_class_set_accessible_role(classPointer, value.gtk)

		fun setTemplate(templateBytes: KGBytes) {
			gtk_widget_class_set_template(classPointer, templateBytes.pointer)
		}
	}

	companion object {
		var defaultDirection: TextDirection
			get() = TextDirection.valueOf(gtk_widget_get_default_direction())
			set(value) = gtk_widget_set_default_direction(value.gtk)

		private val staticTickCallback: GtkTickCallback =
			staticCFunction {
					self: GtkWidget_autoptr,
					frameClock: CPointer<GdkFrameClock>,
					data: gpointer,
				->
				data.asStableRef<TickCallback>().get()
					.invoke(self.wrap(), FrameClock(frameClock)).gtk
			}.reinterpret()

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkWidget_autoptr, data: gpointer? ->
				data?.asStableRef<Widget.() -> Unit>()?.get()?.invoke(self.wrap())
				Unit
			}.reinterpret()

		private val staticDirectionChangedCallback: GCallback =
			staticCFunction {
					_: WidgetPointer,
					previous: GtkTextDirection,
					data: gpointer,
				->
				data.asStableRef<(TextDirection) -> Unit>().get()
					.invoke(TextDirection.valueOf(previous))
				@Suppress("RedundantUnitExpression")
				Unit
			}.reinterpret()

		private val staticStateFlagsChangedCallback: GCallback =
			staticCFunction {
					_: WidgetPointer,
					flags: GtkStateFlags,
					data: gpointer,
				->
				data.asStableRef<(StateFlags) -> Unit>()
					.get()
					.invoke(StateFlags.valueOf(flags))
				@Suppress("RedundantUnitExpression")
				Unit
			}.reinterpret()

		private val staticMapFunction: GCallback =
			staticCFunction { self: WidgetPointer, data: gpointer ->
				data.asStableRef<MapFunction>().get().invoke(self.wrap())
				@Suppress("RedundantUnitExpression")
				Unit
			}.reinterpret()

		private val staticKeynavFailedFunction: GCallback = staticCFunction {
				self: WidgetPointer,
				direction: GtkDirectionType,
				data: gpointer,
			->
			data.asStableRef<KeynavFailedFunction>().get()
				.invoke(self.wrap(), DirectionType.valueOf(direction)).gtk
		}.reinterpret()

		private val staticMoveFocusFunction: GCallback = staticCFunction {
				self: WidgetPointer,
				direction: GtkDirectionType,
				data: gpointer,
			->
			data.asStableRef<WidgetMoveFocusFunction>().get()
				.invoke(self.wrap(), DirectionType.valueOf(direction))
			@Suppress("RedundantUnitExpression")
			Unit
		}.reinterpret()

		private val staticMnemonicActivateFunction: GCallback =
			staticCFunction {
					self: WidgetPointer,
					groupCycling: gboolean,
					data: gpointer,
				->
				data.asStableRef<MnemonicActivateFunction>().get()
					.invoke(self.wrap(), groupCycling.bool).gtk
			}.reinterpret()

		private val staticQueryTooltipFunction: GCallback = staticCFunction {
				self: WidgetPointer,
				x: Int,
				y: Int,
				key: gboolean,
				tooltip: CPointer<GtkTooltip>,
				data: gpointer,
			->
			data.asStableRef<QueryTooltipFunction>().get()
				.invoke(self.wrap(), x, y, key.bool, tooltip.wrap()).gtk
		}.reinterpret()

		inline fun GtkWidget_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkWidget_autoptr.wrap() =
			Widget(this)

		inline fun CPointer<GList>.asMutableWidgetList(): WrappedKList<Widget> =
			asWrappedKList({ reinterpret<GtkWidget>().wrap() }, { widgetPointer })
	}
}

typealias TickCallback = Widget.(FrameClock) -> Boolean

typealias QueryTooltipFunction =
		Widget.(
			@ParameterName("x") Int,
			@ParameterName("y") Int,
			Boolean,
			Tooltip,
		) -> Boolean

typealias MnemonicActivateFunction =
		Widget.(
			@ParameterName("groupCycling") Boolean,
		) -> Boolean

typealias KeynavFailedFunction = Widget.(DirectionType) -> Boolean

typealias MapFunction = Widget.() -> Unit

typealias WidgetMoveFocusFunction = Widget.(DirectionType) -> Unit
