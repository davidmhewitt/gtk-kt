package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gdk.Drag
import org.gtk.gdk.Paintable
import org.gtk.gobject.Value
import org.gtk.gtk.Native
import org.gtk.gtk.Root

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DragIcon.html">
 *     GtkDragIcon</a>
 */
class DragIcon(
	val dragIconPointer: CPointer<GtkDragIcon>,
) : Widget(dragIconPointer.reinterpret()), Native, Root {

	override val nativePointer: CPointer<GtkNative> by lazy {
		dragIconPointer.reinterpret()
	}

	override val rootPointer: CPointer<GtkRoot> by lazy {
		dragIconPointer.reinterpret()
	}

	var child: Widget?
		get() = gtk_drag_icon_get_child(dragIconPointer).wrap()
		set(value) = gtk_drag_icon_set_child(dragIconPointer, value?.widgetPointer)

	companion object {
		fun createWidgetForValue(value: Value): Widget? =
			gtk_drag_icon_create_widget_for_value(value.pointer).wrap()

		fun getForDrag(drag: Drag): Widget =
			gtk_drag_icon_get_for_drag(drag.dragPointer)!!.wrap()

		fun setFromPaintable(drag: Drag, paintable: Paintable, hotX: Int, hotY: Int) {
			gtk_drag_icon_set_from_paintable(
				drag.dragPointer,
				paintable.paintablePointer,
				hotX,
				hotY
			)
		}

	}
}