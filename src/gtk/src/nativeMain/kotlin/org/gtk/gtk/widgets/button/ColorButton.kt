package org.gtk.gtk.widgets.button

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.RGBA
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.ColorChooser
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.button.toggleable.checkable.CheckButton

/**
 * kotlinx-gtk
 * 16 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ColorButton.html">
 *     GtkColorButton</a>
 */
class ColorButton(
	val colorButtonPointer: GtkColorButton_autoptr,
) : Widget(colorButtonPointer.reinterpret()), ColorChooser {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_COLOR_BUTTON))

	override val colorChooserPointer: GtkColorChooser_autoptr by lazy {
		colorButtonPointer.reinterpret()
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ColorButton.new.html">
	 *     gtk_color_button_new</a>
	 */
	constructor() : this(gtk_color_button_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.ColorButton.new_with_rgba.html">
	 *     gtk_color_button_new_with_rgba</a>
	 */
	constructor(rgba: RGBA) : this(gtk_color_button_new_with_rgba(rgba.rgbaPointer)!!.reinterpret())

	var modal: Boolean
		get() = gtk_color_button_get_modal(colorButtonPointer).bool
		set(value) = gtk_color_button_set_modal(colorButtonPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.ColorButton.get_title.html">
	 *     gtk_color_button_get_title</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.ColorButton.set_title.html">
	 *     gtk_color_button_set_title</a>
	 */
	var title: String?
		get() = gtk_color_button_get_title(colorButtonPointer)?.toKString()
		set(value) = gtk_color_button_set_title(colorButtonPointer, value)

	fun addOnActivateCallback(action: TypedNoArgFunc<ColorButton>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgGCallback)

	fun addOnColorSetCallback(action: TypedNoArgFunc<ColorButton>) =
		addSignalCallback(Signals.COLOR_SET, action, staticNoArgGCallback)

	companion object {

		inline fun GtkColorButton_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkColorButton_autoptr.wrap() =
			ColorButton(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkColorButton_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<ColorButton>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}