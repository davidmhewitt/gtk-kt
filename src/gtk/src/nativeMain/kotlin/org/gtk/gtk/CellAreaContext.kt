package org.gtk.gtk

import gtk.GtkCellAreaContext_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 26 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CellAreaContext.html">
 *     GtkCellAreaContext</a>
 */
class CellAreaContext(
	 val cellAreaContextPointer: GtkCellAreaContext_autoptr
) : KGObject(cellAreaContextPointer.reinterpret()){

	companion object{
		inline fun GtkCellAreaContext_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCellAreaContext_autoptr.wrap() =
			CellAreaContext(this)
	}
}