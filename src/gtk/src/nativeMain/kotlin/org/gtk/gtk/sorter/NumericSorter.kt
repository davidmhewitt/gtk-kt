package org.gtk.gtk.sorter

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gtk.Expression
import org.gtk.gtk.Expression.Companion.wrap
import org.gtk.gtk.common.enums.SortType

/**
 * 17 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.NumericSorter.html">
 *     GtkNumericSorter</a>
 */
class NumericSorter(val numericSorterPointer: GtkNumericSorter_autoptr) :
	Sorter(numericSorterPointer.reinterpret()) {

	constructor(expression: Expression) : this(gtk_numeric_sorter_new(expression.expressionPointer)!!)

	var expression: Expression?
		get() = gtk_numeric_sorter_get_expression(numericSorterPointer).wrap()
		set(value) = gtk_numeric_sorter_set_expression(
			numericSorterPointer,
			value?.expressionPointer
		)

	var sortOrder: SortType
		get() = SortType.valueOf(gtk_numeric_sorter_get_sort_order(numericSorterPointer))
		set(value) = gtk_numeric_sorter_set_sort_order(numericSorterPointer, value.gtk)
}