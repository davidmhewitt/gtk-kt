package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gtk.controller.EventController
import org.gtk.gtk.trigger.Shortcut

/**
 * gtk-kt
 *
 * 25 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutController.html">
 *     GtkShortcutController</a>
 */
class ShortcutController(
	val shortcutControllerPointer: CPointer<GtkShortcutController>,
) : EventController(shortcutControllerPointer.reinterpret()) {

	constructor() : this(gtk_shortcut_controller_new()!!.reinterpret())

	constructor(model: ListModel) :
			this(
				gtk_shortcut_controller_new_for_model(
					model.listModelPointer
				)!!.reinterpret()
			)

	fun addShortcut(shortcut: Shortcut) {
		gtk_shortcut_controller_add_shortcut(
			shortcutControllerPointer,
			shortcut.shortcutPointer
		)
	}

	var mnemonicsModifiers: GdkModifierType
		get() = gtk_shortcut_controller_get_mnemonics_modifiers(
			shortcutControllerPointer
		)
		set(value) = gtk_shortcut_controller_set_mnemonics_modifiers(
			shortcutControllerPointer,
			value
		)

	var scope: ShortcutScope
		get() = ShortcutScope.valueOf(
			gtk_shortcut_controller_get_scope(shortcutControllerPointer)
		)
		set(value) = gtk_shortcut_controller_set_scope(
			shortcutControllerPointer,
			value.gtk
		)
}