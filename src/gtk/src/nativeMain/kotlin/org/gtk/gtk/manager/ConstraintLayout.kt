package org.gtk.gtk.manager

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.gtk.Buildable
import org.gtk.gtk.Constraint
import org.gtk.gtk.ConstraintGuide

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ConstraintLayout.html">
 *     GtkConstraintLayout</a>
 */
class ConstraintLayout(val constraintLayout: GtkConstraintLayout_autoptr) :
	LayoutManager(constraintLayout.reinterpret()), Buildable {

	override val buildablePointer: GtkBuildable_autoptr by lazy {
		constraintLayout.reinterpret()
	}

	constructor() : this(gtk_constraint_layout_new()!!.reinterpret())

	fun addConstraint(constraint: Constraint) {
		gtk_constraint_layout_add_constraint(
			constraintLayout,
			constraint.constraintPointer
		)
	}

	// TODO gtk_constraint_layout_add_constraints_from_descriptionv

	fun addGuide(guide: ConstraintGuide) {
		gtk_constraint_layout_add_guide(constraintLayout, guide.guidePointer)
	}

	fun observeConstraints(): ListModel =
		gtk_constraint_layout_observe_constraints(constraintLayout)!!.wrap()

	fun observeGuides(): ListModel =
		gtk_constraint_layout_observe_guides(constraintLayout)!!.wrap()

	fun removeAllConstraints() {
		gtk_constraint_layout_remove_all_constraints(constraintLayout)
	}

	fun removeConstraint(constraint: Constraint) {
		gtk_constraint_layout_remove_constraint(
			constraintLayout,
			constraint.constraintPointer
		)
	}

	fun removeGuide(guide: ConstraintGuide) {
		gtk_constraint_layout_remove_guide(
			constraintLayout,
			guide.guidePointer
		)
	}
}