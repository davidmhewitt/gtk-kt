package org.gtk.gtk

import gtk.GtkTreeIter
import kotlinx.cinterop.*

data class TreeIter(
	val treeIterPointer: CPointer<GtkTreeIter>,
) {
	constructor() : this(memScoped { alloc<GtkTreeIter>().ptr })

	val stamp: Int
		get() = treeIterPointer.pointed.stamp

	var userData: Any? = null
	var userData1: Any? = null
	var userData3: Any? = null

	companion object {
		inline fun CPointer<GtkTreeIter>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkTreeIter>.wrap() =
			TreeIter(this)
	}
}