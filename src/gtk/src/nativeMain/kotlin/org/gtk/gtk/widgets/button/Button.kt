package org.gtk.gtk.widgets.button

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.*
import org.gtk.gtk.Actionable
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 08 / 02 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Button.html">
 *     GtkButton</a>
 */
open class Button(val buttonPointer: GtkButton_autoptr) :
	Widget(buttonPointer.reinterpret()), Actionable {

	override val actionablePointer: GtkActionable_autoptr by lazy {
		buttonPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_BUTTON))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Button.new.html">
	 *     gtk_button_new</a>
	 */
	constructor() :
			this(gtk_button_new()!!.reinterpret())

	enum class From {
		ICON_NAME,
		LABEL,
		MNEMONIC
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Button.new_from_icon_name.html">
	 *     gtk_button_new_from_icon_name</a>
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Button.new_with_label.html">
	 *     gtk_button_new_with_label</a>
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Button.new_with_mnemonic.html">
	 *     gtk_button_new_with_mnemonic</a>
	 */
	constructor(label: String, from: From = From.LABEL) : this(
		when (from) {
			From.ICON_NAME -> gtk_button_new_from_icon_name(label)
			From.LABEL -> gtk_button_new_with_label(label)
			From.MNEMONIC -> gtk_button_new_with_mnemonic(label)
		}!!.reinterpret()
	)

	var child: Widget?
		get() = gtk_button_get_child(buttonPointer).wrap()
		set(value) = gtk_button_set_child(buttonPointer, value?.widgetPointer)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.get_has_frame.html">
	 *     gtk_button_get_has_frame</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.set_has_frame.html">
	 *     gtk_button_set_has_frame</a>
	 */
	var hasFrame: Boolean
		get() = gtk_button_get_has_frame(buttonPointer).bool
		set(value) = gtk_button_set_has_frame(buttonPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.get_icon_name.html">
	 *     gtk_button_get_icon_name</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.set_icon_name.html">
	 *     gtk_button_set_icon_name</a>
	 */
	open var iconName: String?
		get() = gtk_button_get_icon_name(buttonPointer)?.toKString()
		set(value) = gtk_button_set_icon_name(buttonPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.get_label.html">
	 *     gtk_button_get_label</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.set_label.html">
	 *     gtk_button_set_label</a>
	 */
	var label: String?
		get() = gtk_button_get_label(buttonPointer)?.toKString()
		set(value) = gtk_button_set_label(buttonPointer, value)


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.get_use_underline.html">
	 *     gtk_button_get_use_underline</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Button.set_use_underline.html">
	 *     gtk_button_set_use_underline</a>
	 */
	var userUnderline: Boolean
		get() = gtk_button_get_use_underline(buttonPointer).bool
		set(value) = gtk_button_set_use_underline(buttonPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Button.clicked.html">
	 *     clicked</a>
	 */
	fun addOnClickedCallback(flags: UInt = 0u, action: TypedNoArgFunc<Button>) =
		addSignalCallback(Signals.CLICKED, action, staticNoArgGCallback, flags)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Button.activate.html">
	 *     activate</a>
	 */
	fun addOnActivateCallback(flags: UInt = 0u, action: TypedNoArgFunc<Button>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgGCallback, flags)

	companion object {
		inline fun GtkButton_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkButton_autoptr.wrap() =
			Button(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkButton_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<Button>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}