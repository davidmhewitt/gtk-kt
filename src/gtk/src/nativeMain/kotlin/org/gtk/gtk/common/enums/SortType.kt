package org.gtk.gtk.common.enums

import gtk.GtkSortType
import gtk.GtkSortType.GTK_SORT_ASCENDING
import gtk.GtkSortType.GTK_SORT_DESCENDING

/**
 * kotlinx-gtk
 * 06 / 03 / 2021
 */
enum class SortType(  val gtk: GtkSortType) {
	ASCENDING( GTK_SORT_ASCENDING),
	DESCENDING( GTK_SORT_DESCENDING);

	companion object {
		 fun valueOf(gtk: GtkSortType) =
			when(gtk){
			GTK_SORT_ASCENDING -> ASCENDING
			GTK_SORT_DESCENDING -> DESCENDING
			}
	}
}