package org.gtk.gtk.common.enums

import gtk.GtkOverflow
import gtk.GtkOverflow.GTK_OVERFLOW_HIDDEN
import gtk.GtkOverflow.GTK_OVERFLOW_VISIBLE

/**
 * kotlinx-gtk
 *
 * 01 / 08 / 2021
 *
 * @see <a href=""></a>
 */
enum class Overflow(val gtk: GtkOverflow) {
	VISIBLE(GTK_OVERFLOW_VISIBLE),
	HIDDEN(GTK_OVERFLOW_HIDDEN);

	companion object {
		inline fun valueOf(gtk: GtkOverflow) =
			when (gtk) {
				GTK_OVERFLOW_VISIBLE -> VISIBLE
				GTK_OVERFLOW_HIDDEN -> HIDDEN
			}
	}
}