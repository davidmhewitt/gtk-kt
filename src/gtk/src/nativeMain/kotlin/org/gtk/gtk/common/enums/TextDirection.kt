package org.gtk.gtk.common.enums;

import gtk.GtkTextDirection
import gtk.GtkTextDirection.*

/**
 * @see <a href="https://docs.gtk.org/gtk4/enum.TextDirection.html">
 *     GtkTextDirection</a>
 */
enum class TextDirection(val gtk: GtkTextDirection) {
	NONE(GTK_TEXT_DIR_NONE),
	LTR(GTK_TEXT_DIR_LTR),
	RTL(GTK_TEXT_DIR_RTL);

	companion object {
		fun valueOf(gtk: GtkTextDirection) =
			when (gtk) {
				GTK_TEXT_DIR_NONE -> NONE
				GTK_TEXT_DIR_LTR -> LTR
				GTK_TEXT_DIR_RTL -> RTL
			}
	}
}