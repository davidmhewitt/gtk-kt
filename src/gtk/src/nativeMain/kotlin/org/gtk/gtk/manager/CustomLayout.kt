package org.gtk.gtk.manager

import gtk.GtkCustomLayout_autoptr
import kotlinx.cinterop.reinterpret

/**
 * kotlinx-gtk
 *
 * 22 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CustomLayout.html">
 *     GtkCustomLayout</a>
 */
class CustomLayout(
	val customLayoutPointer: GtkCustomLayout_autoptr
) : LayoutManager(customLayoutPointer.reinterpret()) {

	// TODO Cannot create a kotlin constructor, no userdata param
}