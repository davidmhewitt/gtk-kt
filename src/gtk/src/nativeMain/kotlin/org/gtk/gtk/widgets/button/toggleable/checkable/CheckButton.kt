package org.gtk.gtk.widgets.button.toggleable.checkable

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.Actionable
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.widgets.Calendar
import org.gtk.gtk.widgets.Calendar.Companion.wrap
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.button.toggleable.ToggleButton

/**
 * kotlinx-gtk
 *
 * 05 / 07 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CheckButton.html">
 *     GtkCheckButton</a>
 */
open class CheckButton(val checkButtonPointer: GtkCheckButton_autoptr) :
	Widget(checkButtonPointer.reinterpret()), Actionable {

	override val actionablePointer: GtkActionable_autoptr by lazy {
		checkButtonPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_CHECK_BUTTON))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.CheckButton.new.html">
	 *     gtk_check_button_new</a>
	 */
	constructor() : this(gtk_check_button_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.CheckButton.new_with_label.html">
	 *     gtk_check_button_new_with_label</a>
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.CheckButton.new_with_mnemonic.html">
	 *     gtk_check_button_new_with_mnemonic</a>
	 */
	constructor(label: String, mnemonic: Boolean = false) : this((
			if (mnemonic)
				gtk_check_button_new_with_label(
					label
				)
			else gtk_check_button_new_with_mnemonic(
				label
			)
			)!!.reinterpret()
	)

	var active: Boolean
		get() = gtk_check_button_get_active(checkButtonPointer).bool
		set(value) = gtk_check_button_set_active(checkButtonPointer, value.gtk)

	var inconsistent: Boolean
		get() = gtk_check_button_get_inconsistent(checkButtonPointer).bool
		set(value) = gtk_check_button_set_inconsistent(checkButtonPointer, value.gtk)

	var label: String?
		get() = gtk_check_button_get_label(checkButtonPointer)?.toKString()
		set(value) = gtk_check_button_set_label(checkButtonPointer, value)

	var useUnderline: Boolean
		get() = gtk_check_button_get_use_underline(checkButtonPointer).bool
		set(value) = gtk_check_button_set_use_underline(checkButtonPointer, value.gtk)

	fun addOnActivateCallback(action: TypedNoArgFunc<CheckButton>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgGCallback)

	fun addOnToggledCallback(action: TypedNoArgFunc<CheckButton>) =
		addSignalCallback(Signals.TOGGLED, action, staticNoArgGCallback)

	companion object {

		inline fun GtkCheckButton_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkCheckButton_autoptr.wrap() =
			CheckButton(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkCheckButton_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<CheckButton>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}