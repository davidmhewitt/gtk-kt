package org.gtk.gtk.entrybuffer

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.CStringPointer
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * kotlinx-gtk
 *
 * 30 / 06 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.EntryBuffer.html">GtkEntryBuffer</a>
 */
open class EntryBuffer(val entryBufferPointer: CPointer<GtkEntryBuffer>) :
	KGObject(entryBufferPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.EntryBuffer.new.html">
	 *     gtk_entry_buffer_new</a>
	 */
	constructor(initialCharacters: String? = null) : this(
		gtk_entry_buffer_new(initialCharacters, initialCharacters?.length ?: -1)!!
	)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.delete_text.html">
	 *     gtk_entry_buffer_delete_text</a>
	 */
	fun deleteText(position: UInt, count: Int) {
		gtk_entry_buffer_delete_text(entryBufferPointer, position, count)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.emit_deleted_text.html">
	 *     gtk_entry_buffer_emit_deleted_text</a>
	 */
	fun emitDeletedText(position: UInt, count: UInt) {
		gtk_entry_buffer_emit_deleted_text(entryBufferPointer, position, count)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.emit_inserted_text.html">
	 *     gtk_entry_buffer_emit_inserted_text</a>
	 */
	fun emitInsertedText(position: UInt, chars: String) {
		gtk_entry_buffer_emit_inserted_text(entryBufferPointer,
			position,
			chars,
			chars.length.toUInt())
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.get_bytes.html">
	 *     gtk_entry_buffer_get_bytes</a>
	 */
	val bytes: ULong
		get() = gtk_entry_buffer_get_bytes(entryBufferPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.get_length.html">
	 *     gtk_entry_buffer_get_length</a>
	 */
	val length: UInt
		get() = gtk_entry_buffer_get_length(entryBufferPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.get_max_length.html">
	 *     gtk_entry_buffer_get_max_length</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.set_max_length.html">
	 *     gtk_entry_buffer_set_max_length</a>
	 */
	var maxLength: Int
		get() = gtk_entry_buffer_get_max_length(entryBufferPointer)
		set(value) = gtk_entry_buffer_set_max_length(entryBufferPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.get_text.html">
	 *     gtk_entry_buffer_get_text</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.set_text.html">
	 *     gtk_entry_buffer_set_text</a>
	 */
	var text: String
		get() = gtk_entry_buffer_get_text(entryBufferPointer)!!.toKString()
		set(value) = gtk_entry_buffer_set_text(entryBufferPointer, value, value.length)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.EntryBuffer.insert_text.html">
	 *     gtk_entry_buffer_insert_text</a>
	 */
	fun insertText(position: UInt, chars: String) {
		gtk_entry_buffer_insert_text(entryBufferPointer, position, chars, chars.length)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.EntryBuffer.deleted-text.html">
	 *     deleted-text</a>
	 */
	fun addDeletedTextCallback(action: DeletedTextFunction) =
		addSignalCallback(Signals.DELETED_TEXT, action, staticDeletedTextFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.EntryBuffer.inserted-text.html">
	 *     inserted-text</a>
	 */
	fun addInsertedTextCallback(action: InsertedTextFunction) =
		addSignalCallback(Signals.INSERTED_TEXT, action, staticInsertedTextFunction)

	companion object {
		/**
		 * @see <a href="https://docs.gtk.org/gtk4/signal.EntryBuffer.deleted-text.html">
		 *     deleted-text</a>
		 */
		val staticDeletedTextFunction: GCallback =
			staticCFunction {
					self: GtkEntryBuffer_autoptr,
					position: UInt,
					nChars: UInt,
					data: gpointer,
				->
				data.asStableRef<DeletedTextFunction>()
					.get()
					.invoke(self.wrap(), position, nChars)
				Unit
			}.reinterpret()

		/**
		 * @see <a href="https://docs.gtk.org/gtk4/signal.EntryBuffer.inserted-text.html">
		 *     inserted-text</a>
		 */
		val staticInsertedTextFunction: GCallback by lazy {
			staticCFunction {
					self: GtkEntryBuffer_autoptr,
					position: UInt,
					chars: CStringPointer,
					nChars: UInt,
					data: gpointer,
				->
				data.asStableRef<InsertedTextFunction>()
					.get()
					.invoke(self.wrap(), position, chars.toKString(), nChars)
				Unit
			}.reinterpret()
		}

		fun CPointer<GtkEntryBuffer>?.wrap() =
			this?.wrap()

		fun CPointer<GtkEntryBuffer>.wrap() =
			EntryBuffer(this)
	}
}

/**
 * @see <a href="https://docs.gtk.org/gtk4/signal.EntryBuffer.deleted-text.html">
 *     deleted-text</a>
 */
typealias DeletedTextFunction = EntryBuffer.(
	@ParameterName("position") UInt,
	UInt,
) -> Unit

/**
 * @see <a href="https://docs.gtk.org/gtk4/signal.EntryBuffer.inserted-text.html">
 *     inserted-text</a>
 */
typealias InsertedTextFunction = EntryBuffer. (
	@ParameterName("position") UInt,
	@ParameterName("chars") String,
	@ParameterName("nChars") UInt,
) -> Unit