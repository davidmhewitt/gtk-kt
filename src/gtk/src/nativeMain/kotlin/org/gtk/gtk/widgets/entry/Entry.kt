package org.gtk.gtk.widgets.entry

import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkEntryIconPosition.GTK_ENTRY_ICON_PRIMARY
import gtk.GtkEntryIconPosition.GTK_ENTRY_ICON_SECONDARY
import kotlinx.cinterop.*
import org.gtk.gdk.ContentProvider
import org.gtk.gdk.Event
import org.gtk.gdk.Event.Companion.wrap
import org.gtk.gdk.Paintable
import org.gtk.gdk.Paintable.Companion.wrap
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.gio.Icon
import org.gtk.gio.Icon.Companion.wrap
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.CellEditable
import org.gtk.gtk.EntryCompletion
import org.gtk.gtk.EntryCompletion.Companion.wrap
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.common.enums.InputPurpose
import org.gtk.gtk.entrybuffer.EntryBuffer
import org.gtk.gtk.entrybuffer.EntryBuffer.Companion.wrap
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.misc.Image
import org.gtk.pango.AttrList
import org.gtk.pango.AttrList.Companion.wrap
import org.gtk.pango.TabArray
import org.gtk.pango.TabArray.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 16 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Entry.html">GtkEntry</a>
 */
open class Entry(
	val entryPointer: CPointer<GtkEntry>,
) : Widget(entryPointer.reinterpret()), CellEditable {

	override val cellEditablePointer: GtkCellEditable_autoptr by lazy {
		entryPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_ENTRY))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Entry.new.html">
	 *     gtk_entry_new</a>
	 */
	constructor() : this(gtk_entry_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Entry.new_with_buffer.html">
	 *     gtk_entry_new_with_buffer</a>
	 */
	constructor(buffer: EntryBuffer) :
			this(gtk_entry_new_with_buffer(buffer.entryBufferPointer)!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_activates_default.html">
	 *     gtk_entry_get_activates_default</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_activates_default.html">
	 *     gtk_entry_set_activates_default</a>
	 */
	var activatesDefault: Boolean
		get() = gtk_entry_get_activates_default(entryPointer).bool
		set(value) = gtk_entry_set_activates_default(entryPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_alignment.html">
	 *     gtk_entry_get_alignment</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_alignment.html">
	 *     gtk_entry_set_alignment</a>
	 */
	var alignment: Float
		get() = gtk_entry_get_alignment(entryPointer)
		set(value) = gtk_entry_set_alignment(entryPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_attributes.html">
	 *     gtk_entry_get_attributes</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_attributes.html">
	 *     gtk_entry_set_attributes</a>
	 */
	var attributes: AttrList?
		get() = gtk_entry_get_attributes(entryPointer).wrap()
		set(value) = gtk_entry_set_attributes(entryPointer, value?.attrListPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_buffer.html">
	 *     gtk_entry_get_buffer</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_buffer.html">
	 *     gtk_entry_set_buffer</a>
	 */
	var buffer: EntryBuffer
		get() = gtk_entry_get_buffer(entryPointer)!!.wrap()
		set(value) = gtk_entry_set_buffer(entryPointer, value.entryBufferPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_completion.html">
	 *     gtk_entry_get_completion</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_completion.html">
	 *     gtk_entry_set_completion</a>
	 */
	var completion: EntryCompletion?
		get() = gtk_entry_get_completion(entryPointer).wrap()
		set(value) = gtk_entry_set_completion(entryPointer, value?.entryCompletionPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_current_icon_drag_source.html">
	 *     gtk_entry_get_current_icon_drag_source</a>
	 */
	val currentIconDragSource: Int
		get() = gtk_entry_get_current_icon_drag_source(entryPointer)


	val extraMenu: MenuModel?
		get() = gtk_entry_get_extra_menu(entryPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_has_frame.html">
	 *     gtk_entry_get_has_frame</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_has_frame.html">
	 *     gtk_entry_set_has_frame</a>
	 */
	var hasFrame: Boolean
		get() = gtk_entry_get_has_frame(entryPointer).bool
		set(value) = gtk_entry_set_has_frame(entryPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_activatable.html">
	 *     gtk_entry_get_icon_activatable</a>
	 */
	fun getIconCanActivate(iconPosition: IconPosition): Boolean =
		gtk_entry_get_icon_activatable(entryPointer, iconPosition.gtk).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_area.html">
	 *     gtk_entry_get_icon_area</a>
	 */
	fun getIconArea(iconPosition: IconPosition): Rectangle = memScoped {
		val rectangle = cValue<GdkRectangle>()
		gtk_entry_get_icon_area(entryPointer, iconPosition.gtk, rectangle)
		rectangle.ptr.wrap()
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_at_pos.html">
	 *     gtk_entry_get_icon_at_pos</a>
	 */
	fun getIconAtPosition(x: Int, y: Int): Int =
		gtk_entry_get_icon_at_pos(entryPointer, x, y)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_gicon.html">
	 *     gtk_entry_get_icon_gicon</a>
	 */
	fun getIconGIcon(iconPosition: IconPosition): Icon? =
		gtk_entry_get_icon_gicon(entryPointer, iconPosition.gtk).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_name.html">
	 *     gtk_entry_get_icon_name</a>
	 */
	fun getIconName(iconPosition: IconPosition): String? =
		gtk_entry_get_icon_name(entryPointer, iconPosition.gtk)?.toKString()

	fun getIconPaintable(iconPos: IconPosition): Paintable? =
		gtk_entry_get_icon_paintable(entryPointer, iconPos.gtk).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_sensitive.html">
	 *     gtk_entry_get_icon_sensitive</a>
	 */
	fun getIconSensitive(iconPosition: IconPosition): Boolean =
		gtk_entry_get_icon_sensitive(entryPointer, iconPosition.gtk).bool

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_storage_type.html">
	 *     gtk_entry_get_icon_storage_type</a>
	 */
	fun getIconStorageType(iconPosition: IconPosition): Image.Type =
		Image.Type.valueOf(gtk_entry_get_icon_storage_type(entryPointer, iconPosition.gtk))!!

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_tooltip_markup.html">
	 *     gtk_entry_get_icon_tooltip_markup</a>
	 */
	fun getIconTooltipMarkup(iconPosition: IconPosition): String? =
		gtk_entry_get_icon_tooltip_markup(entryPointer, iconPosition.gtk)?.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_icon_tooltip_text.html">
	 *     gtk_entry_get_icon_tooltip_text</a>
	 */
	fun getIconTooltipText(iconPosition: IconPosition): String? =
		gtk_entry_get_icon_tooltip_text(entryPointer, iconPosition.gtk)?.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_input_hints.html">
	 *     gtk_entry_get_input_hints</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_input_hints.html">
	 *     gtk_entry_set_input_hints</a>
	 */
	var inputHints: GtkInputHints
		get() = gtk_entry_get_input_hints(entryPointer)
		set(value) = gtk_entry_set_input_hints(entryPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_input_purpose.html">
	 *     gtk_entry_get_input_purpose</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_input_purpose.html">
	 *     gtk_entry_set_input_purpose</a>
	 */
	var inputPurpose: InputPurpose?
		get() = InputPurpose.valueOf(gtk_entry_get_input_purpose(entryPointer))
		set(value) = gtk_entry_set_input_purpose(entryPointer, value!!.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_invisible_char.html">
	 *     gtk_entry_get_invisible_char</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_invisible_char.html">
	 *     gtk_entry_set_invisible_char</a>
	 */
	var invisibleChar: Char
		get() = gtk_entry_get_invisible_char(entryPointer).toInt().toChar()
		set(value) = gtk_entry_set_invisible_char(entryPointer, value.code.toUInt())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_max_length.html">
	 *     gtk_entry_get_max_length</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_max_length.html">
	 *     gtk_entry_set_max_length</a>
	 */
	var maxLength: Int
		get() = gtk_entry_get_max_length(entryPointer)
		set(max) = gtk_entry_set_max_length(entryPointer, max)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_overwrite_mode.html">
	 *     gtk_entry_get_overwrite_mode</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_overwrite_mode.html">
	 *     gtk_entry_set_overwrite_mode</a>
	 */
	var overwriteMode: Boolean
		get() = gtk_entry_get_overwrite_mode(entryPointer).bool
		set(value) = gtk_entry_set_overwrite_mode(entryPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_placeholder_text.html">
	 *     gtk_entry_get_placeholder_text</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_placeholder_text.html">
	 *     gtk_entry_set_placeholder_text</a>
	 */
	var placeholderText: String
		get() = gtk_entry_get_placeholder_text(entryPointer)!!.toKString()
		set(value) = gtk_entry_set_placeholder_text(entryPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_progress_fraction.html">
	 *     gtk_entry_get_progress_fraction</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_progress_fraction.html">
	 *     gtk_entry_set_progress_fraction</a>
	 */
	var progressFraction: Double
		get() = gtk_entry_get_progress_fraction(entryPointer)
		set(value) = gtk_entry_set_progress_fraction(entryPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_progress_pulse_step.html">
	 *     gtk_entry_get_progress_pulse_step</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_progress_pulse_step.html">
	 *     gtk_entry_set_progress_pulse_step</a>
	 */
	var progressPulseStep: Double
		get() = gtk_entry_get_progress_pulse_step(entryPointer)
		set(value) = gtk_entry_set_progress_pulse_step(entryPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_tabs.html">
	 *     gtk_entry_get_tabs</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_tabs.html">
	 *     gtk_entry_set_tabs</a>
	 */
	var tabs: TabArray?
		get() = gtk_entry_get_tabs(entryPointer)?.wrap()
		set(value) = gtk_entry_set_tabs(entryPointer, value?.tabArrayPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_text_length.html">
	 *     gtk_entry_get_text_length</a>
	 */
	val textLength: UShort
		get() = gtk_entry_get_text_length(entryPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.get_visibility.html">
	 *     gtk_entry_get_visibility</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_visibility.html">
	 *     gtk_entry_set_visibility</a>
	 */
	var isContentVisible: Boolean
		get() = gtk_entry_get_visibility(entryPointer).bool
		set(value) = gtk_entry_set_visibility(entryPointer, value.gtk)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.grab_focus_without_selecting.html">
	 *     gtk_entry_grab_focus_without_selecting</a>
	 */
	fun grabFocusWithoutSelecting() {
		gtk_entry_grab_focus_without_selecting(entryPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.progress_pulse.html">
	 *     gtk_entry_progress_pulse</a>
	 */
	fun progressPulse() {
		gtk_entry_progress_pulse(entryPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.reset_im_context.html">
	 *     gtk_entry_reset_im_context</a>
	 */
	fun resetImContext() {
		gtk_entry_reset_im_context(entryPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_activatable.html">
	 *     gtk_entry_set_icon_activatable</a>
	 */
	fun setIconCanActivate(iconPosition: IconPosition, canActivate: Boolean) {
		gtk_entry_set_icon_activatable(entryPointer, iconPosition.gtk, canActivate.gtk)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_drag_source.html">
	 *     gtk_entry_set_icon_drag_source</a>
	 */
	fun setIconDragSource(
		iconPosition: IconPosition,
		contentProvider: ContentProvider,
		actions: GdkDragAction,
	) {
		gtk_entry_set_icon_drag_source(
			entryPointer,
			iconPosition.gtk,
			contentProvider.contentProviderPointer,
			actions
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_from_gicon.html">
	 *     gtk_entry_set_icon_from_gicon</a>
	 */
	fun setIconFromGIcon(iconPosition: IconPosition, icon: Icon?) {
		gtk_entry_set_icon_from_gicon(entryPointer, iconPosition.gtk, icon?.iconPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_from_icon_name.html">
	 *     gtk_entry_set_icon_from_icon_name</a>
	 */
	fun setIconFromIconName(iconPosition: IconPosition, iconName: String?) {
		gtk_entry_set_icon_from_icon_name(entryPointer, iconPosition.gtk, iconName)
	}

	fun setIconFromPaintable(iconPosition: IconPosition, paintable: Paintable?) {
		gtk_entry_set_icon_from_paintable(
			entryPointer,
			iconPosition.gtk,
			paintable?.paintablePointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_sensitive.html">
	 *     gtk_entry_set_icon_sensitive</a>
	 */
	fun setIconSensitive(iconPosition: IconPosition, sensitive: Boolean) {
		gtk_entry_set_icon_sensitive(entryPointer, iconPosition.gtk, sensitive.gtk)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_tooltip_markup.html">
	 *     gtk_entry_set_icon_tooltip_markup</a>
	 */
	fun setIconTooltipMarkup(iconPosition: IconPosition, tooltip: String) {
		gtk_entry_set_icon_tooltip_markup(entryPointer, iconPosition.gtk, tooltip)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.set_icon_tooltip_text.html">
	 *     gtk_entry_set_icon_tooltip_text</a>
	 */
	fun setIconTooltipText(iconPosition: IconPosition, tooltip: String) {
		gtk_entry_set_icon_tooltip_text(entryPointer, iconPosition.gtk, tooltip)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Entry.unset_invisible_char.html">
	 *     gtk_entry_unset_invisible_char</a>
	 */
	fun unsetInvisibleChar() {
		gtk_entry_unset_invisible_char(entryPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Entry.activate.html">
	 *     activate</a>
	 */
	fun addOnActivateCallback(action: TypedNoArgFunc<Entry>): SignalManager =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgGCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Entry.icon-press.html">
	 *     icon-press</a>
	 */
	fun addOnIconPressCallback(action: EntryIconInteractionEventFunction): SignalManager =
		addSignalCallback(Signals.ICON_PRESS, action, staticIconInteractionEventFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Entry.icon-release.html">
	 *     icon-release</a>
	 */
	fun addOnIconReleaseCallback(action: EntryIconInteractionEventFunction): SignalManager =
		addSignalCallback(Signals.ICON_RELEASE, action, staticIconInteractionEventFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.EntryIconPosition.html">
	 *     GtkEntryIconPosition</a>
	 */
	enum class IconPosition(val gtk: GtkEntryIconPosition) {
		/**
		 * At the beginning of the entry (depending on the text direction).
		 */
		PRIMARY(GTK_ENTRY_ICON_PRIMARY),

		/**
		 * At the end of the entry (depending on the text direction).
		 */
		SECONDARY(GTK_ENTRY_ICON_SECONDARY);

		companion object {
			fun valueOf(gtk: GtkEntryIconPosition) = values().find { it.gtk == gtk }
		}
	}

	companion object {
		inline fun GtkEntry_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkEntry_autoptr.wrap() =
			Entry(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkEntry_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<Entry>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()

		private val staticIconInteractionEventFunction: GCallback =
			staticCFunction {
					self: GtkEntry_autoptr,
					type: GtkEntryIconPosition,
					event: CPointer<GdkEvent>,
					data: gpointer,
				->
				data.asStableRef<EntryIconInteractionEventFunction>()
					.get()
					.invoke(self.wrap(), IconPosition.valueOf(type)!!, event.wrap())
				Unit
			}.reinterpret()
	}
}

typealias EntryIconInteractionEventFunction = Entry.(Entry.IconPosition, Event) -> Unit

