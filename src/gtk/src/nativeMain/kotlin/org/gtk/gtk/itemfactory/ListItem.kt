package org.gtk.gtk.itemfactory

import gobject.GObject
import gtk.GtkListItem
import gtk.gtk_list_item_get_child
import gtk.gtk_list_item_get_item
import gtk.gtk_list_item_set_child
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.Widget.Companion.wrap

class ListItem(val listItemPointer: CPointer<GtkListItem>) : KGObject(listItemPointer.reinterpret()) {

    var child: Widget?
        get() = gtk_list_item_get_child(listItemPointer).wrap()
        set(value) = gtk_list_item_set_child(listItemPointer, value?.widgetPointer)

    val item: KGObject?
        get() = gtk_list_item_get_item(listItemPointer)?.reinterpret<GObject>().wrap()

    companion object {
        fun CPointer<GtkListItem>.wrap() = ListItem(this)
        fun CPointer<GtkListItem>?.wrap() = this?.wrap()
    }
}