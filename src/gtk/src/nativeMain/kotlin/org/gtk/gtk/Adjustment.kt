package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.common.callback.TypedNoArgFunc

/**
 * kotlinx-gtk
 * 07 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Adjustment.html">
 *     GtkAdjustment</a>
 */
class Adjustment(
	val adjustmentPointer: GtkAdjustment_autoptr,
) : KGObject(adjustmentPointer.reinterpret()) {

	constructor(
		value: Double,
		lower: Double,
		upper: Double,
		stepIncrement: Double,
		pageIncrement: Double,
		pageSize: Double,
	) : this(
		gtk_adjustment_new(
			value,
			lower,
			upper,
			stepIncrement,
			pageIncrement,
			pageSize
		)!!
	)

	fun clampPage(lower: Double, upper: Double) {
		gtk_adjustment_clamp_page(adjustmentPointer, lower, upper)
	}

	fun configure(
		value: Double = this.value,
		lower: Double = this.lower,
		upper: Double = this.upper,
		stepIncrement: Double = this.stepIncrement,
		pageIncrement: Double = this.pageIncrement,
		pageSize: Double = this.pageSize,
	) {
		gtk_adjustment_configure(
			adjustmentPointer,
			value,
			lower,
			upper,
			stepIncrement,
			pageIncrement,
			pageSize
		)
	}

	var value: Double
		get() = gtk_adjustment_get_value(adjustmentPointer)
		set(value) = gtk_adjustment_set_value(adjustmentPointer, value)

	var pageIncrement: Double
		get() = gtk_adjustment_get_page_increment(adjustmentPointer)
		set(value) = gtk_adjustment_set_page_increment(adjustmentPointer, value)

	var pageSize: Double
		get() = gtk_adjustment_get_page_size(adjustmentPointer)
		set(value) = gtk_adjustment_set_page_size(adjustmentPointer, value)

	var stepIncrement: Double
		get() = gtk_adjustment_get_step_increment(adjustmentPointer)
		set(value) = gtk_adjustment_set_step_increment(adjustmentPointer, value)

	var upper: Double
		get() = gtk_adjustment_get_upper(adjustmentPointer)
		set(value) = gtk_adjustment_set_upper(adjustmentPointer, value)

	var lower: Double
		get() = gtk_adjustment_get_lower(adjustmentPointer)
		set(value) = gtk_adjustment_set_lower(adjustmentPointer, value)

	fun addOnChangedCallback(action: TypedNoArgFunc<Adjustment>) =
		addSignalCallback(Signals.CHANGED, action, staticNoArgGCallback)

	fun addOnValueChangedCallback(action: TypedNoArgFunc<Adjustment>) =
		addSignalCallback(Signals.VALUE_CHANGED, action, staticNoArgGCallback)

	companion object {
		inline fun CPointer<GtkAdjustment>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkAdjustment>.wrap() =
			Adjustment(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkAdjustment_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<Adjustment>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}