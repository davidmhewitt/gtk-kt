package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gobject.GObject
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gio.ListModel
import org.gtk.gio.ListModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.glib.toNullTermCStringArray
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.Expression
import org.gtk.gtk.Expression.Companion.wrap
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.itemfactory.ListItemFactory
import org.gtk.gtk.itemfactory.ListItemFactory.Companion.wrap

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DropDown.html">
 *     GtkDropDown</a>
 */
class DropDown(
	val dropDownPointer: GtkDropDown_autoptr,
) : Widget(dropDownPointer.reinterpret()) {

	constructor(model: ListModel, expression: Expression) :
			this(
				gtk_drop_down_new(
					model.listModelPointer,
					expression.expressionPointer
				)!!.reinterpret()
			)

	constructor(strings: Array<String>) :
			this(
				gtk_drop_down_new_from_strings(
					strings.toNullTermCStringArray()
				)!!.reinterpret()
			)

	var isSearchEnabled: Boolean
		get() = gtk_drop_down_get_enable_search(dropDownPointer).bool;
		set(value) = gtk_drop_down_set_enable_search(dropDownPointer, value.gtk)

	var expression: Expression?
		get() = gtk_drop_down_get_expression(dropDownPointer).wrap()
		set(value) = gtk_drop_down_set_expression(
			dropDownPointer,
			value?.expressionPointer
		)

	var factory: ListItemFactory?
		get() = gtk_drop_down_get_factory(dropDownPointer).wrap()
		set(value) = gtk_drop_down_set_factory(
			dropDownPointer,
			value?.listItemFactoryPointer
		)

	var listFactory: ListItemFactory?
		get() = gtk_drop_down_get_list_factory(dropDownPointer).wrap()
		set(value) = gtk_drop_down_set_list_factory(
			dropDownPointer,
			value?.listItemFactoryPointer
		)

	var model: ListModel?
		get() = gtk_drop_down_get_model(dropDownPointer).wrap()
		set(value) = gtk_drop_down_set_model(
			dropDownPointer,
			value?.listModelPointer
		)

	var selected: UInt
		get() = gtk_drop_down_get_selected(dropDownPointer)
		set(value) = gtk_drop_down_set_selected(dropDownPointer, value)

	val selectedItem: KGObject?
		get() = gtk_drop_down_get_selected_item(dropDownPointer)
			?.reinterpret<GObject>().wrap()

	/*
	TODO Uncomment when 4.6 comes
	var showArrow: Boolean
		get() = gtk_drop_down_get_show_arrow(dropDownPointer).bool;
		set(value) = gtk_drop_down_set_show_arrow(dropDownPointer, value.gtk)
	 */

	fun addOnActivateCallback(action: TypedNoArgFunc<DropDown>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgGCallback)

	companion object {
		inline fun GtkDropDown_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkDropDown_autoptr.wrap() =
			DropDown(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkDropDown_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<DropDown>>()
					.get()
					.invoke(self.wrap())
				Unit
			}.reinterpret()
	}
}