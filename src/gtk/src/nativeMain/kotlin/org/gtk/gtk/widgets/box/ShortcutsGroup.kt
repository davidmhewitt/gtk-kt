package org.gtk.gtk.widgets.box

import gtk.GTK_TYPE_SHORTCUTS_GROUP
import gtk.GtkShortcutsGroup
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.widgets.Widget

/**
 * 08 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ShortcutsGroup.html">
 *     GtkShortcutsGroup</a>
 */
class ShortcutsGroup(
	val shortcutsGroupPointer: CPointer<GtkShortcutsGroup>
) : Box(shortcutsGroupPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SHORTCUTS_GROUP))
}