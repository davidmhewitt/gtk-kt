package org.gtk.gtk.widgets

import glib.gboolean
import glib.gint
import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkTreeViewDropPosition.*
import gtk.GtkTreeViewGridLines.*
import kotlinx.cinterop.*
import org.gtk.gdk.ContentFormats
import org.gtk.gdk.Paintable
import org.gtk.gdk.Paintable.Companion.wrap
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.glib.*
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.staticDestroyStableRefFunction
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.*
import org.gtk.gtk.TreeIter.Companion.wrap
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gtk.TreePath.Companion.wrap
import org.gtk.gtk.TreeSelection.Companion.wrap
import org.gtk.gtk.cellrenderer.CellRenderer
import org.gtk.gtk.cellrenderer.CellRenderer.Companion.wrap
import org.gtk.gtk.common.callback.TreeViewRowSeparatorFunc
import org.gtk.gtk.common.callback.TypedNoArgForBooleanFunc
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.common.callback.staticTreeViewRowSeparatorFunc
import org.gtk.gtk.common.data.Coordinates
import org.gtk.gtk.common.enums.MovementStep
import org.gtk.gtk.widgets.Editable.Companion.wrap
import org.gtk.gtk.widgets.TreeViewColumn.Companion.wrap

/**
 * 14 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.TreeView.html">
 *     GtkTreeView</a>
 */
class TreeView(
	val treeViewPointer: CPointer<GtkTreeView>,
) : Widget(treeViewPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_TREE_VIEW))

	constructor() : this(gtk_tree_view_new()!!.reinterpret())

	constructor(model: TreeModel) :
			this(
				gtk_tree_view_new_with_model(
					model.treeModelPointer
				)!!.reinterpret()
			)

	fun appendColumn(column: TreeViewColumn): Int =
		gtk_tree_view_append_column(treeViewPointer, column.columnPointer)

	fun collapseAll() {
		gtk_tree_view_collapse_all(treeViewPointer)
	}

	fun collapseRow(path: TreePath): Boolean =
		gtk_tree_view_collapse_row(treeViewPointer, path.treePathPointer).bool

	fun columnsAutosize() {
		gtk_tree_view_columns_autosize(treeViewPointer)
	}

	fun convertBinWindowToTreeCoords(bx: Int, by: Int): Coordinates =
		memScoped {
			val tx = cValue<IntVar>()
			val ty = cValue<IntVar>()

			gtk_tree_view_convert_bin_window_to_tree_coords(
				treeViewPointer,
				bx,
				by,
				tx,
				ty
			)

			Coordinates(
				tx.ptr.pointed.value,
				ty.ptr.pointed.value
			)
		}

	fun convertBinWindowToWidgetCoords(bx: Int, by: Int): Coordinates =
		memScoped {
			val wx = cValue<IntVar>()
			val wy = cValue<IntVar>()

			gtk_tree_view_convert_bin_window_to_widget_coords(
				treeViewPointer,
				bx,
				by,
				wx,
				wy
			)

			Coordinates(
				wx.ptr.pointed.value,
				wy.ptr.pointed.value
			)
		}

	fun convertTreeToBinWindowCoords(tx: Int, ty: Int): Coordinates =
		memScoped {
			val bx = cValue<IntVar>()
			val by = cValue<IntVar>()

			gtk_tree_view_convert_tree_to_bin_window_coords(
				treeViewPointer,
				tx,
				ty,
				bx,
				by
			)

			Coordinates(
				bx.ptr.pointed.value,
				by.ptr.pointed.value
			)
		}

	fun convertTreeToWidgetCoords(tx: Int, ty: Int): Coordinates =
		memScoped {
			val wx = cValue<IntVar>()
			val wy = cValue<IntVar>()

			gtk_tree_view_convert_tree_to_widget_coords(
				treeViewPointer,
				tx,
				ty,
				wx,
				wy
			)

			Coordinates(
				wx.ptr.pointed.value,
				wy.ptr.pointed.value
			)
		}

	fun convertWidgetToBinWindowCoords(tx: Int, ty: Int): Coordinates =
		memScoped {
			val wx = cValue<IntVar>()
			val wy = cValue<IntVar>()

			gtk_tree_view_convert_widget_to_bin_window_coords(
				treeViewPointer,
				tx,
				ty,
				wx,
				wy
			)

			Coordinates(
				wx.ptr.pointed.value,
				wy.ptr.pointed.value
			)
		}

	fun convertWidgetToTreeCoords(wx: Int, wy: Int): Coordinates =
		memScoped {
			val tx = cValue<IntVar>()
			val ty = cValue<IntVar>()

			gtk_tree_view_convert_widget_to_tree_coords(
				treeViewPointer,
				wx,
				wy,
				tx,
				ty
			)

			Coordinates(
				tx.ptr.pointed.value,
				ty.ptr.pointed.value
			)
		}

	fun createRowDragIcon(path: TreePath): Paintable? =
		gtk_tree_view_create_row_drag_icon(
			treeViewPointer,
			path.treePathPointer
		).wrap()

	fun enableModelDragDest(formats: ContentFormats, actions: GdkDragAction) {
		gtk_tree_view_enable_model_drag_dest(
			treeViewPointer,
			formats.contentFormatsPointer,
			actions
		)
	}

	fun enableModelDragSource(
		startButtonMask: GdkModifierType,
		formats: ContentFormats,
		actions: GdkDragAction,
	) {
		gtk_tree_view_enable_model_drag_source(
			treeViewPointer,
			startButtonMask,
			formats.contentFormatsPointer,
			actions
		)
	}

	fun expandAll() {
		gtk_tree_view_expand_all(treeViewPointer)
	}

	fun expandRow(path: TreePath, openAll: Boolean = false): Boolean =
		gtk_tree_view_expand_row(
			treeViewPointer,
			path.treePathPointer,
			openAll.gtk
		).bool

	fun expandToPath(path: TreePath) {
		gtk_tree_view_expand_to_path(treeViewPointer, path.treePathPointer)
	}

	var activateOnSingleClick: Boolean
		get() = gtk_tree_view_get_activate_on_single_click(treeViewPointer).bool
		set(value) = gtk_tree_view_set_activate_on_single_click(
			treeViewPointer,
			value.gtk
		)

	fun getBackgroundArea(
		path: TreePath?,
		column: TreeViewColumn?,
	): Rectangle = memScoped {
		val rect = alloc<GdkRectangle>()

		gtk_tree_view_get_background_area(
			treeViewPointer,
			path?.treePathPointer,
			column?.columnPointer,
			rect.ptr
		)

		rect.ptr.wrap()
	}

	fun getCellArea(
		path: TreePath?,
		column: TreeViewColumn?,
	): Rectangle = memScoped {
		val rect = alloc<GdkRectangle>()

		gtk_tree_view_get_cell_area(
			treeViewPointer,
			path?.treePathPointer,
			column?.columnPointer,
			rect.ptr
		)

		rect.ptr.wrap()
	}

	fun getColumn(position: Int): TreeViewColumn? =
		gtk_tree_view_get_column(treeViewPointer, position).wrap()

	val columns: WrappedKList<TreeViewColumn> =
		gtk_tree_view_get_columns(treeViewPointer)!!
			.asWrappedKList(
				{ reinterpret<GtkTreeViewColumn>().wrap() },
				{ treeViewPointer }
			)


	data class TreeViewCursor(
		var path: TreePath?,
		var focusColumn: TreeViewColumn?,
	)

	val treeViewCursor: TreeViewCursor
		get() = memScoped {
			val path = allocPointerTo<GtkTreePath>()
			val focusColumn = allocPointerTo<GtkTreeViewColumn>()

			gtk_tree_view_get_cursor(
				treeViewPointer,
				path.ptr,
				focusColumn.ptr
			)

			TreeViewCursor(
				path.value.wrap(),
				focusColumn.value.wrap()
			)
		}

	data class DragDestRow(
		val path: TreePath?,
		val post: DropPosition,
	)

	val dragDestRow: DragDestRow
		get() = memScoped {
			val path = allocPointerTo<GtkTreePath>()
			val pos = alloc<GtkTreeViewDropPosition.Var>()

			gtk_tree_view_get_drag_dest_row(
				treeViewPointer,
				path.ptr,
				pos.ptr,
			)

			DragDestRow(
				path.value?.wrap(),
				DropPosition.valueOf(pos.value)
			)
		}

	var enableSearch: Boolean
		get() = gtk_tree_view_get_enable_search(treeViewPointer).bool
		set(value) = gtk_tree_view_set_enable_search(
			treeViewPointer,
			value.gtk
		)

	var enableTreeLines: Boolean
		get() = gtk_tree_view_get_enable_tree_lines(treeViewPointer).bool
		set(value) = gtk_tree_view_set_enable_tree_lines(
			treeViewPointer,
			value.gtk
		)

	var expanderColumn: TreeViewColumn?
		get() = gtk_tree_view_get_expander_column(treeViewPointer).wrap()
		set(value) = gtk_tree_view_set_expander_column(
			treeViewPointer,
			value?.columnPointer
		)

	var fixedHeightMode: Boolean
		get() = gtk_tree_view_get_fixed_height_mode(treeViewPointer).bool
		set(value) = gtk_tree_view_set_fixed_height_mode(
			treeViewPointer,
			value.gtk
		)

	var gridLines: GridLines
		get() = GridLines.valueOf(gtk_tree_view_get_grid_lines(treeViewPointer))
		set(value) = gtk_tree_view_set_grid_lines(treeViewPointer, value.gtk)

	var headersClickable: Boolean
		get() = gtk_tree_view_get_headers_clickable(treeViewPointer).bool
		set(value) = gtk_tree_view_set_headers_clickable(
			treeViewPointer,
			value.gtk
		)

	var headersVisible: Boolean
		get() = gtk_tree_view_get_headers_visible(treeViewPointer).bool
		set(value) = gtk_tree_view_set_headers_visible(
			treeViewPointer,
			value.gtk
		)

	var hoverExpand: Boolean
		get() = gtk_tree_view_get_hover_expand(treeViewPointer).bool
		set(value) = gtk_tree_view_set_hover_expand(
			treeViewPointer,
			value.gtk
		)

	var hoverSelection: Boolean
		get() = gtk_tree_view_get_hover_selection(treeViewPointer).bool
		set(value) = gtk_tree_view_set_hover_selection(
			treeViewPointer,
			value.gtk
		)

	var levelIndentation: Int
		get() = gtk_tree_view_get_level_indentation(treeViewPointer)
		set(value) = gtk_tree_view_set_level_indentation(
			treeViewPointer,
			value
		)

	var model: TreeModel?
		get() = gtk_tree_view_get_model(treeViewPointer).wrap()
		set(value) = gtk_tree_view_set_model(treeViewPointer, value?.treeModelPointer)

	val nColumns: UInt
		get() = gtk_tree_view_get_n_columns(treeViewPointer)

	data class PathAtPos(
		val path: TreePath?,
		val column: TreeViewColumn?,
		val cellX: Int,
		val cellY: Int,
	)

	fun getPathAtPos(x: Int, y: Int): PathAtPos = memScoped {
		val path = allocPointerTo<GtkTreePath>()
		val column = allocPointerTo<GtkTreeViewColumn>()
		val cell_x = alloc<IntVar>()
		val cell_y = alloc<IntVar>()

		gtk_tree_view_get_path_at_pos(
			treeViewPointer,
			x,
			y,
			path.ptr,
			column.ptr,
			cell_x.ptr,
			cell_y.ptr
		)

		PathAtPos(
			path.value?.wrap(),
			column.value?.wrap(),
			cell_x.value,
			cell_y.value
		)
	}

	var reorderable: Boolean
		get() = gtk_tree_view_get_reorderable(treeViewPointer).bool
		set(value) = gtk_tree_view_set_reorderable(
			treeViewPointer,
			value.gtk
		)

	// Ignore gtk_tree_view_get_row_separator_func private func

	var rubberBanding: Boolean
		get() = gtk_tree_view_get_rubber_banding(treeViewPointer).bool
		set(value) = gtk_tree_view_set_rubber_banding(
			treeViewPointer,
			value.gtk
		)

	var searchColumn: Int
		get() = gtk_tree_view_get_search_column(treeViewPointer)
		set(value) = gtk_tree_view_set_search_column(treeViewPointer, value)

	var searchEntry: Editable?
		get() = gtk_tree_view_get_search_entry(treeViewPointer).wrap()
		set(value) = gtk_tree_view_set_search_entry(
			treeViewPointer,
			value?.editablePointer
		)

	// Ignore gtk_tree_view_get_search_equal_func private func

	val selection: TreeSelection
		get() = gtk_tree_view_get_selection(treeViewPointer)!!.wrap()

	var showExpanders: Boolean
		get() = gtk_tree_view_get_show_expanders(treeViewPointer).bool
		set(value) = gtk_tree_view_set_show_expanders(
			treeViewPointer,
			value.gtk
		)

	var tooltipColumn: Int
		get() = gtk_tree_view_get_tooltip_column(treeViewPointer)
		set(value) = gtk_tree_view_set_tooltip_column(treeViewPointer, value)

	data class TooltipContext(
		val model: TreeModel?,
		val path: TreePath?,
		val iter: TreeIter,
	)

	fun getTooltipContext(x: Int, y: Int, keyboardTip: Boolean): TooltipContext =
		memScoped {
			val model = allocPointerTo<GtkTreeModel>()
			val path = allocPointerTo<GtkTreePath>()
			val iter = alloc<GtkTreeIter>()

			gtk_tree_view_get_tooltip_context(
				treeViewPointer,
				x,
				y,
				keyboardTip.gtk,
				model.ptr,
				path.ptr,
				iter.ptr
			)

			TooltipContext(
				model.value?.wrap(),
				path.value?.wrap(),
				iter.ptr.wrap()
			)
		}

	data class VisibleRange(
		val startPath: TreePath,
		val endPath: TreePath,
	)

	val visibleRange: VisibleRange?
		get() = memScoped {
			val startPath = allocPointerTo<GtkTreePath>()
			val endPath = allocPointerTo<GtkTreePath>()

			val r = gtk_tree_view_get_visible_range(
				treeViewPointer,
				startPath.ptr,
				endPath.ptr
			).bool

			if (r)
				VisibleRange(
					startPath.value!!.wrap(),
					endPath.value!!.wrap()
				)
			else null
		}

	val visibleRect: Rectangle
		get() = memScoped {
			val visibleRect = alloc<GdkRectangle>()

			gtk_tree_view_get_visible_rect(treeViewPointer, visibleRect.ptr)

			visibleRect.ptr.wrap()
		}

	fun insertColumn(column: TreeViewColumn, position: Int): Int =
		gtk_tree_view_insert_column(
			treeViewPointer,
			column.columnPointer,
			position
		)

	// ignore gtk_tree_view_insert_column_with_attributes vararg

	fun insertColumn(
		position: Int,
		title: String,
		cellRenderer: CellRenderer,
		dataFunc: TreeCellDataFunc,
	): Int =
		gtk_tree_view_insert_column_with_data_func(
			treeViewPointer,
			position,
			title,
			cellRenderer.cellRendererPointer,
			staticTreeCellDataFunc,
			dataFunc.asStablePointer(),
			staticDestroyStableRefFunction
		)

	fun isBlankAtPos(x: Int, y: Int): Boolean =
		gtk_tree_view_is_blank_at_pos(
			treeViewPointer,
			x,
			y,
			null,
			null,
			null,
			null
		).bool

	val rubberBandingActive: Boolean
		get() = gtk_tree_view_is_rubber_banding_active(treeViewPointer).bool

	fun mapExpandedRows(func: TreeViewMappingFunc) {
		StableRef.create(func).usePointer {
			gtk_tree_view_map_expanded_rows(
				treeViewPointer,
				staticTreeMappingFunc,
				it
			)
		}
	}

	fun moveColumnAfter(column: TreeViewColumn, baseColumn: TreeViewColumn?) {
		gtk_tree_view_move_column_after(
			treeViewPointer,
			column.columnPointer,
			baseColumn?.columnPointer
		)
	}

	fun removeColumn(column: TreeViewColumn): Int =
		gtk_tree_view_remove_column(treeViewPointer, column.columnPointer)

	fun rowActivated(path: TreePath, column: TreeViewColumn?) {
		gtk_tree_view_row_activated(
			treeViewPointer,
			path.treePathPointer,
			column?.columnPointer
		)
	}

	fun rowExpanded(path: TreePath): Boolean =
		gtk_tree_view_row_expanded(treeViewPointer, path.treePathPointer).bool

	fun scrollToCell(
		path: TreePath?,
		column: TreeViewColumn?,
		useAlign: Boolean,
		rowAlign: Float,
		colAlign: Float,
	) {
		gtk_tree_view_scroll_to_cell(
			treeViewPointer,
			path?.treePathPointer,
			column?.columnPointer,
			useAlign.gtk,
			rowAlign,
			colAlign
		)
	}

	fun scrollToPoint(treeX: Int, treeY: Int) {
		gtk_tree_view_scroll_to_point(
			treeViewPointer,
			treeX,
			treeY
		)
	}

	fun setColumnDragFunction(func: TreeViewColumnDropFunc) {
		gtk_tree_view_set_column_drag_function(
			treeViewPointer,
			staticColumnDropFunc,
			func.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}

	fun setCursor(
		path: TreePath,
		focusColumn: TreeViewColumn?,
		startEditing: Boolean,
	) {
		gtk_tree_view_set_cursor(
			treeViewPointer,
			path.treePathPointer,
			focusColumn?.columnPointer,
			startEditing.gtk
		)
	}

	fun setCursorOnCell(
		path: TreePath,
		focusColumn: TreeViewColumn?,
		focusCell: CellRenderer?,
		startEditing: Boolean,
	) {
		gtk_tree_view_set_cursor_on_cell(
			treeViewPointer,
			path.treePathPointer,
			focusColumn?.columnPointer,
			focusCell?.cellRendererPointer,
			startEditing.gtk
		)
	}

	fun setDragDestRow(
		path: TreePath?,
		pos: DropPosition,
	) {
		gtk_tree_view_set_drag_dest_row(
			treeViewPointer,
			path?.treePathPointer,
			pos.gtk
		)
	}

	fun setRowSeparatorFunc(
		func: TreeViewRowSeparatorFunc?,
	) {
		if (func == null) {
			gtk_tree_view_set_row_separator_func(
				treeViewPointer,
				null,
				null,
				null
			)
			return
		}

		gtk_tree_view_set_row_separator_func(
			treeViewPointer,
			staticTreeViewRowSeparatorFunc,
			func.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}

	fun setSearchEqualFunc(func: TreeViewSearchEqualFunc?) {
		if (func == null) {
			gtk_tree_view_set_search_equal_func(
				treeViewPointer,
				null,
				null,
				null
			)
			return
		}

		gtk_tree_view_set_search_equal_func(
			treeViewPointer,
			staticSearchEqualFunc,
			func.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}

	fun setTooltipCell(
		tooltip: Tooltip,
		path: TreePath?,
		column: TreeViewColumn?,
		cell: CellRenderer?,
	) {
		gtk_tree_view_set_tooltip_cell(
			treeViewPointer,
			tooltip.tooltipPointer,
			path?.treePathPointer,
			column?.columnPointer,
			cell?.cellRendererPointer
		)
	}

	fun setTooltipRow(tooltip: Tooltip, path: TreePath) {
		gtk_tree_view_set_tooltip_row(
			treeViewPointer,
			tooltip.tooltipPointer,
			path.treePathPointer
		)
	}

	fun unsetRowDragDestination() {
		gtk_tree_view_unset_rows_drag_dest(treeViewPointer)
	}

	fun unsetRowsDragSource() {
		gtk_tree_view_unset_rows_drag_source(treeViewPointer)
	}

	fun addOnColumnsChangedCallback(action: TypedNoArgFunc<TreeView>) =
		addSignalCallback(Signals.COLUMNS_CHANGED, action, staticNoArgGCallback)

	fun addOnCursorChangedCallback(action: TypedNoArgFunc<TreeView>) =
		addSignalCallback(Signals.CURSOR_CHANGED, action, staticNoArgGCallback)

	fun addOnExpandCollapseCursorRowCallback(
		action: TreeViewExpandCollapseCursorRowFunc,
	) = addSignalCallback(
		Signals.EXPAND_COLLAPSE_CURSOR_ROW,
		action,
		staticExpandCollapseCursorRowCallback
	)

	fun addOnMoveCursorCallback(
		action: TreeViewMoveCursorFunc,
	) = addSignalCallback(Signals.MOVE_CURSOR, action, staticMoveCursorFunction)

	fun addOnRowActivatedCallback(action: TreeViewRowActivatedFunc) =
		addSignalCallback(Signals.ROW_ACTIVATED, action, staticRowActivatedFunc)

	fun addOnRowCollapsedCallback(action: TreeViewRowSpreadFunc) =
		addSignalCallback(Signals.ROW_COLLAPSED, action, staticRowSpreadFunc)

	fun addOnRowExpandedCallback(action: TreeViewRowSpreadFunc) =
		addSignalCallback(Signals.ROW_EXPANDED, action, staticRowSpreadFunc)

	fun addOnSelectAllCallback(action: TypedNoArgForBooleanFunc<TreeView>) =
		addSignalCallback(
			Signals.SELECT_ALL,
			action,
			staticNoArgForBooleanGCallback
		)

	fun addOnSelectCursorParentCallback(
		action: TypedNoArgForBooleanFunc<TreeView>,
	) =
		addSignalCallback(
			Signals.SELECT_CURSOR_PARENT,
			action,
			staticNoArgForBooleanGCallback
		)

	fun addOnSelectCursorRowCallback(action: TreeViewSelectCursorRowFunc) =
		addSignalCallback(
			Signals.SELECT_CURSOR_ROW,
			action,
			staticSelectCursorRowCallback
		)

	fun addOnStartInteractveSearchCallback(
		action: TypedNoArgForBooleanFunc<TreeView>,
	) = addSignalCallback(
		Signals.START_INTERACTIVE_SEARCH,
		action,
		staticNoArgForBooleanGCallback
	)

	fun addOnTestCollapseRowCallback(action: TreeViewTestSpreadRowFunc) =
		addSignalCallback(Signals.TEST_COLLAPSE_ROW, action, staticTestRowSpreadFunc)

	fun addOnTestExpandRowCallback(action: TreeViewTestSpreadRowFunc) =
		addSignalCallback(Signals.TEST_EXPAND_ROW, action, staticTestRowSpreadFunc)


	fun addOnToggleCursorRowCallback(
		action: TypedNoArgForBooleanFunc<TreeView>,
	) = addSignalCallback(
		Signals.TOGGLE_CURSOR_ROW,
		action,
		staticNoArgForBooleanGCallback
	)

	fun addOnUnselectAllCallback(action: TypedNoArgForBooleanFunc<TreeView>) =
		addSignalCallback(
			Signals.UNSELECT_ALL,
			action,
			staticNoArgForBooleanGCallback
		)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.TreeViewDropPosition.html">
	 *     GtkTreeViewDropPosition</a>
	 */
	enum class DropPosition(
		val gtk: GtkTreeViewDropPosition,
	) {
		BEFORE(GTK_TREE_VIEW_DROP_BEFORE),
		AFTER(GTK_TREE_VIEW_DROP_AFTER),
		INTO_OR_BEFORE(GTK_TREE_VIEW_DROP_INTO_OR_BEFORE),
		INTO_OR_AFTER(GTK_TREE_VIEW_DROP_INTO_OR_AFTER);

		companion object {
			fun valueOf(gtk: GtkTreeViewDropPosition) =
				when (gtk) {
					GTK_TREE_VIEW_DROP_BEFORE -> BEFORE
					GTK_TREE_VIEW_DROP_AFTER -> AFTER
					GTK_TREE_VIEW_DROP_INTO_OR_BEFORE -> INTO_OR_BEFORE
					GTK_TREE_VIEW_DROP_INTO_OR_AFTER -> INTO_OR_AFTER
				}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.TreeViewGridLines.html">
	 *     GtkTreeViewGridLines</a>
	 */
	enum class GridLines(
		val gtk: GtkTreeViewGridLines,
	) {
		NONE(GTK_TREE_VIEW_GRID_LINES_NONE),
		HORIZONTAL(GTK_TREE_VIEW_GRID_LINES_HORIZONTAL),
		VERTICAL(GTK_TREE_VIEW_GRID_LINES_VERTICAL),
		BOTH(GTK_TREE_VIEW_GRID_LINES_BOTH);

		companion object {
			fun valueOf(gtk: GtkTreeViewGridLines) =
				when (gtk) {
					GTK_TREE_VIEW_GRID_LINES_NONE -> NONE
					GTK_TREE_VIEW_GRID_LINES_HORIZONTAL -> HORIZONTAL
					GTK_TREE_VIEW_GRID_LINES_VERTICAL -> VERTICAL
					GTK_TREE_VIEW_GRID_LINES_BOTH -> BOTH
				}
		}
	}

	companion object {
		inline fun GtkTreeView_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkTreeView_autoptr.wrap() =
			TreeView(this)

		private val staticTreeCellDataFunc: GtkTreeCellDataFunc =
			staticCFunction {
					tree_column: GtkTreeViewColumn_autoptr?,
					cell: GtkCellRenderer_autoptr?,
					tree_model: GtkTreeModel_autoptr?,
					iter: GtkTreeIter_autoptr?,
					data: gpointer?,
				->
				data!!.asStableRef<TreeCellDataFunc>()
					.get()
					.invoke(
						tree_column!!.wrap(),
						cell!!.wrap(),
						tree_model!!.wrap(),
						iter!!.wrap()
					)
			}

		private val staticTreeMappingFunc: GtkTreeViewMappingFunc =
			staticCFunction {
					tree_view: GtkTreeView_autoptr?,
					path: GtkTreePath_autoptr?,
					data: gpointer?,
				->
				data!!.asStableRef<TreeViewMappingFunc>()
					.get()
					.invoke(tree_view!!.wrap(), path!!.wrap())
			}

		private val staticColumnDropFunc: GtkTreeViewColumnDropFunc =
			staticCFunction {
					tree_view: GtkTreeView_autoptr?,
					column: GtkTreeViewColumn_autoptr?,
					prev_column: GtkTreeViewColumn_autoptr?,
					next_column: GtkTreeViewColumn_autoptr?,
					data: gpointer?,
				->

				data!!.asStableRef<TreeViewColumnDropFunc>()
					.get()
					.invoke(
						tree_view!!.wrap(),
						column!!.wrap(),
						prev_column!!.wrap(),
						next_column!!.wrap()
					).gtk
			}

		private val staticSearchEqualFunc: GtkTreeViewSearchEqualFunc =
			staticCFunction {
					model: GtkTreeModel_autoptr?,
					column: Int,
					key: CStringPointer?,
					iter: GtkTreeIter_autoptr?,
					data: gpointer?,
				->
				data!!.asStableRef<TreeViewSearchEqualFunc>()
					.get()
					.invoke(
						model!!.wrap(),
						column,
						key!!.toKString(),
						iter!!.wrap(),
					).gtk
			}

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkTreeView_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<TreeView>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticNoArgForBooleanGCallback: GCallback =
			staticCFunction { self: GtkTreeView_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgForBooleanFunc<TreeView>>()
					.get()
					.invoke(self.wrap())
					.gtk
			}.reinterpret()

		private val staticExpandCollapseCursorRowCallback: GCallback =
			staticCFunction {
					self: GtkTreeView_autoptr,
					obj: gboolean,
					p0: gboolean,
					p1: gboolean,
					data: gpointer,
				->
				data.asStableRef<TreeViewExpandCollapseCursorRowFunc>()
					.get()
					.invoke(self.wrap(), obj.bool, p0.bool, p1.bool)
					.gtk
			}.reinterpret()

		private val staticMoveCursorFunction: GCallback =
			staticCFunction {
					self: GtkTreeView_autoptr,
					step: GtkMovementStep,
					direction: gint,
					extend: gboolean,
					modify: gboolean,
					data: gpointer,
				->
				data.asStableRef<TreeViewMoveCursorFunc>()
					.get()
					.invoke(
						self.wrap(),
						MovementStep.valueOf(step),
						direction,
						extend.bool,
						modify.bool
					)
					.gtk
			}.reinterpret()

		private val staticRowActivatedFunc: GCallback =
			staticCFunction {
					self: GtkTreeView_autoptr,
					path: GtkTreePath_autoptr,
					column: GtkTreeViewColumn_autoptr?,
					data: gpointer,
				->
				data.asStableRef<TreeViewRowActivatedFunc>()
					.get()
					.invoke(
						self.wrap(),
						path.wrap(),
						column.wrap()
					)
			}.reinterpret()

		private val staticRowSpreadFunc: GCallback =
			staticCFunction {
					self: GtkTreeView_autoptr,
					iter: GtkTreeIter_autoptr,
					path: GtkTreePath_autoptr,
					data: gpointer,
				->
				data.asStableRef<TreeViewRowSpreadFunc>()
					.get()
					.invoke(
						self.wrap(),
						iter.wrap(),
						path.wrap()
					)
			}.reinterpret()

		private val staticSelectCursorRowCallback: GCallback =
			staticCFunction {
					self: GtkTreeView_autoptr,
					obj: gboolean,
					data: gpointer,
				->
				data.asStableRef<TreeViewSelectCursorRowFunc>()
					.get()
					.invoke(self.wrap(), obj.bool)
					.gtk
			}.reinterpret()

		private val staticTestRowSpreadFunc: GCallback =
			staticCFunction {
					self: GtkTreeView_autoptr,
					iter: GtkTreeIter_autoptr,
					path: GtkTreePath_autoptr,
					data: gpointer,
				->
				data.asStableRef<TreeViewTestSpreadRowFunc>()
					.get()
					.invoke(
						self.wrap(),
						iter.wrap(),
						path.wrap()
					)
					.gtk
			}.reinterpret()

	}
}


typealias TreeCellDataFunc = (
	treeColumn: TreeViewColumn,
	cell: CellRenderer,
	treeModel: TreeModel,
	iter: TreeIter,
) -> Unit

typealias TreeViewMappingFunc =
		TreeView.(
			path: TreePath,
		) -> Unit

typealias TreeViewColumnDropFunc =
		TreeView.(
			column: TreeViewColumn,
			prevColumn: TreeViewColumn,
			nextColumn: TreeViewColumn,
		) -> Boolean

typealias TreeViewSearchEqualFunc = (
	model: TreeModel,
	column: Int,
	key: String,
	iter: TreeIter,
) -> Boolean

typealias TreeViewExpandCollapseCursorRowFunc =
		TreeView.(
			obj: Boolean,
			p0: Boolean,
			p1: Boolean,
		) -> Boolean

typealias TreeViewMoveCursorFunc =
		TreeView.(
			step: MovementStep,
			direction: Int,
			extend: Boolean,
			modify: Boolean,
		) -> Boolean

typealias TreeViewRowActivatedFunc =
		TreeView.(
			path: TreePath,
			column: TreeViewColumn?,
		) -> Unit

typealias TreeViewRowSpreadFunc =
		TreeView.(
			iter: TreeIter,
			path: TreePath,
		) -> Unit

typealias TreeViewSelectCursorRowFunc =
		TreeView.(
			obj: Boolean,
		) -> Boolean

typealias TreeViewTestSpreadRowFunc =
		TreeView.(
			iter: TreeIter,
			path: TreePath,
		) -> Boolean