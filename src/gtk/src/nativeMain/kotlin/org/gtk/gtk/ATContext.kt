package org.gtk.gtk

import glib.gpointer
import gobject.GCallback
import gtk.GtkATContext_autoptr
import gtk.gtk_at_context_create
import gtk.gtk_at_context_get_accessible
import gtk.gtk_at_context_get_accessible_role
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.Display
import org.gtk.gobject.KGObject
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback

/**
 * gtk-kt
 *
 * 25 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ATContext.html">GtkATContext</a>
 */
class ATContext(val atContextPointer: GtkATContext_autoptr) :
	KGObject(atContextPointer.reinterpret()) {

	@Throws(ATContextCreateException::class)
	constructor(role: Accessible.Role, accessible: Accessible, display: Display) :
			this(
				gtk_at_context_create(
					role.gtk,
					accessible.accessiblePointer,
					display.displayPointer
				) ?: throw ATContextCreateException()
			)

	val accessible: Accessible
		get() = ImplAccessible(gtk_at_context_get_accessible(atContextPointer)!!)

	val role: Accessible.Role
		get() = Accessible.Role.valueOf(gtk_at_context_get_accessible_role(atContextPointer))

	fun addOnStateChangeCallback(action: ATContext.() -> Unit) =
		addSignalCallback(Signals.STATE_CHANGE, action)

	/**
	 * Thrown when [gtk_at_context_create] returns null in the constructor of [ATContext]
	 */
	class ATContextCreateException : Exception("gtk_at_context_create returned null")

	companion object {
		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkATContext_autoptr, data: gpointer? ->
				data?.asStableRef<ATContext.() -> Unit>()
					?.get()
					?.invoke(self.wrap())
				Unit
			}.reinterpret()

		inline fun GtkATContext_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkATContext_autoptr.wrap() =
			ATContext(this)
	}
}