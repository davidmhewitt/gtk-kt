package org.gtk.gtk.widgets.popover

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.Rectangle
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals.ACTIVATE_DEFAULT
import org.gtk.gobject.Signals.CLOSED
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.ShortcutManager
import org.gtk.gtk.common.data.Coordinates
import org.gtk.gtk.common.enums.PositionType
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Popover.html">
 *     GtkPopover</a>
 */
open class Popover(
	val popoverPointer: CPointer<GtkPopover>,
) : Widget(popoverPointer.reinterpret()),
	ShortcutManager {

	override val shortcutManagerPointer: CPointer<GtkShortcutManager> by lazy {
		popoverPointer.reinterpret()
	}

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_POPOVER))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Popover.new.html">
	 *     gtk_popover_new</a>
	 */
	constructor() : this(gtk_popover_new()!!.reinterpret())

	var autoHide: Boolean
		get() = gtk_popover_get_autohide(popoverPointer).bool
		set(value) = gtk_popover_set_autohide(popoverPointer, value.gtk)


	var cascadePopdown: Boolean
		get() = gtk_popover_get_cascade_popdown(popoverPointer).bool
		set(value) = gtk_popover_set_cascade_popdown(popoverPointer, value.gtk)

	var child: Widget?
		get() = gtk_popover_get_child(popoverPointer).wrap()
		set(value) = gtk_popover_set_child(popoverPointer, value?.widgetPointer)

	var hasArrow: Boolean
		get() = gtk_popover_get_has_arrow(popoverPointer).bool
		set(value) = gtk_popover_set_has_arrow(popoverPointer, value.gtk)

	var mnemonicsVisible: Boolean
		get() = gtk_popover_get_mnemonics_visible(popoverPointer).bool
		set(value) = gtk_popover_set_mnemonics_visible(popoverPointer, value.gtk)

	var offset: Coordinates
		get() = memScoped {
			val x = cValue<IntVar>()
			val y = cValue<IntVar>()
			gtk_popover_get_offset(popoverPointer, x, y)
			Coordinates(x.ptr.pointed.value, y.ptr.pointed.value)
		}
		set(value) = gtk_popover_set_offset(popoverPointer, value.x, value.y)

	var pointingTo: Rectangle?
		get() = memScoped {
			val rect = cValue<GdkRectangle>()
			val result = gtk_popover_get_pointing_to(popoverPointer, rect).bool

			if (result) {
				Rectangle(rect.ptr)
			} else {
				null
			}
		}
		set(value) =
			gtk_popover_set_pointing_to(popoverPointer, value?.rectanglePointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Popover.get_position.html">
	 *     gtk_popover_get_position</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.Popover.set_position.html">
	 *     gtk_popover_set_position</a>
	 */
	var position: PositionType
		set(value) = gtk_popover_set_position(popoverPointer, value.gtk)
		get() = PositionType.valueOf(gtk_popover_get_position(popoverPointer))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Popover.popdown.html">
	 *     gtk_popover_popdown</a>
	 */
	fun popdown() {
		gtk_popover_popdown(popoverPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Popover.popup.html">gtk_popover_popup</a>
	 */
	fun popup() {
		gtk_popover_popup(popoverPointer)
	}

	fun present() {
		gtk_popover_present(popoverPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Popover.set_default_widget.html">
	 *     gtk_popover_get_default_widget</a>
	 */
	fun setDefaultWidget(value: Widget?) =
		gtk_popover_set_default_widget(popoverPointer, value?.widgetPointer)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Popover.activate-default.html">
	 *     activate-default</a>
	 */
	fun addOnActivateDefaultCallback(action: PopoverOnActivateDefaultFunction) =
		addSignalCallback(ACTIVATE_DEFAULT, action, staticOnActivateDefaultFunction)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/signal.Popover.closed.html">
	 *     closed</a>
	 */
	fun addOnClosedCallback(action: PopoverOnClosedFunction) =
		addSignalCallback(CLOSED, action, staticClosedFunction)

	companion object {
		inline fun CPointer<GtkPopover>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkPopover>.wrap() =
			Popover(this)

		private val staticOnActivateDefaultFunction: GCallback =
			staticCFunction { self: GtkPopover_autoptr, data: gpointer ->
				data.asStableRef<PopoverOnClosedFunction>().get().invoke(self.wrap())
			}.reinterpret()

		private val staticClosedFunction: GCallback =
			staticCFunction { self: GtkPopover_autoptr, data: gpointer ->
				data.asStableRef<PopoverOnClosedFunction>().get().invoke(self.wrap())
			}.reinterpret()
	}
}

typealias PopoverOnActivateDefaultFunction = Popover.() -> Unit
typealias PopoverOnClosedFunction = Popover.() -> Unit