package org.gtk.gtk.widgets

import gtk.GtkColumnViewColumn
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ColumnViewColumn.html">
 *     GtkColumnViewColumn</a>
 */
class ColumnViewColumn(
	val columnPointer: CPointer<GtkColumnViewColumn>,
) : KGObject(columnPointer.reinterpret()) {

}