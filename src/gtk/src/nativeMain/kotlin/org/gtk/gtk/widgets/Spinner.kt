package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * kotlinx-gtk
 *
 * 26 / 03 / 2021
 *
 * @see <a href="https://developer.gnome.org/gtk3/unstable/GtkSpinner.html"></a>
 */
class Spinner(
	 val spinnerPointer: CPointer<GtkSpinner>
) : Widget(spinnerPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_SPINNER))

	/**
	 * @see <a href="https://developer.gnome.org/gtk3/unstable/GtkSpinner.html#gtk-spinner-new"></a>
	 */
	constructor() : this(gtk_spinner_new()!!.reinterpret())

	/**
	 * Easy way to start / stop the Spinner
	 * @see <a href="https://developer.gnome.org/gtk3/unstable/GtkSpinner.html#gtk-spinner-start">gtk_spinner_start</a>
	 * @see <a href="https://developer.gnome.org/gtk3/unstable/GtkSpinner.html#gtk-spinner-stop">gtk_spinner_stop</a>
	 */
	var isRunning: Boolean = false
		set(value) {
			if (value)
				gtk_spinner_start(spinnerPointer)
			else gtk_spinner_stop(spinnerPointer)
			field = value
		}

	/**
	 * @see isRunning
	 * @see <a href="https://developer.gnome.org/gtk3/unstable/GtkSpinner.html#gtk-spinner-start">gtk_spinner_start</a>
	 */
	fun start() {
		isRunning = true
	}

	/**
	 * @see isRunning
	 * @see <a href="https://developer.gnome.org/gtk3/unstable/GtkSpinner.html#gtk-spinner-stop">gtk_spinner_stop</a>
	 */
	fun stop() {
		isRunning = false
	}
}