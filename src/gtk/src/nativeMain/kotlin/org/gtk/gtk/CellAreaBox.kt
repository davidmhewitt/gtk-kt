package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.reinterpret
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.cellrenderer.CellRenderer
import org.gtk.gtk.widgets.Widget

/**
 * 01 / 01 / 2022
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.CellAreaBox.html">
 *     GtkCellAreaBox</a>
 */
class CellAreaBox(
	val cellAreaBoxPointer: GtkCellAreaBox_autoptr,
) : CellArea(cellAreaBoxPointer.reinterpret()), Orientable {
	override val orientablePointer: GtkOrientable_autoptr by lazy {
		cellAreaBoxPointer.reinterpret()
	}

	constructor() : this(gtk_cell_area_box_new()!!.reinterpret())

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_CELL_AREA_BOX))

	var spacing: Int
		get() = gtk_cell_area_box_get_spacing(cellAreaBoxPointer)
		set(value) = gtk_cell_area_box_set_spacing(cellAreaBoxPointer, value)

	fun packEnd(
		renderer: CellRenderer,
		expand: Boolean,
		align: Boolean,
		fixed: Boolean,
	) {
		gtk_cell_area_box_pack_end(
			cellAreaBoxPointer,
			renderer.cellRendererPointer,
			expand.gtk,
			align.gtk,
			fixed.gtk
		)
	}

	fun packStart(
		renderer: CellRenderer,
		expand: Boolean,
		align: Boolean,
		fixed: Boolean,
	) {
		gtk_cell_area_box_pack_start(
			cellAreaBoxPointer,
			renderer.cellRendererPointer,
			expand.gtk,
			align.gtk,
			fixed.gtk
		)
	}
}