package org.gtk.gtk

import gtk.GtkSettings
import gtk.gtk_settings_get_default
import gtk.gtk_settings_get_for_display
import gtk.gtk_settings_reset_property
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gdk.Display
import org.gtk.gobject.KGObject

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.Settings.html">
 *     GtkSettings</a>
 */
class Settings(
	val settingsPointer: CPointer<GtkSettings>
) : KGObject(settingsPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Settings.reset_property.html">
	 *     gtk_settings_reset_property</a>
	 */
	fun resetProperty(name: String) {
		gtk_settings_reset_property(settingsPointer, name)
	}

	companion object {
		/**
		 * @see <a href="https://docs.gtk.org/gtk4/type_func.Settings.get_default.html">
		 *     gtk_settings_get_default</a>
		 */
		val default: Settings?
			get() = gtk_settings_get_default().wrap()

		/**
		 * @see <a href="https://docs.gtk.org/gtk4/type_func.Settings.get_for_display.html">
		 *     gtk_settings_get_for_display</a>
		 */
		fun getForDisplay(display: Display): Settings =
			gtk_settings_get_for_display(display.displayPointer)!!.wrap()

		inline fun CPointer<GtkSettings>?.wrap() =
			this?.let { Settings(it) }

		inline fun CPointer<GtkSettings>.wrap() =
			Settings(this)
	}
}