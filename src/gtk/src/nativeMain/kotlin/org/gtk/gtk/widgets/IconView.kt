package org.gtk.gtk.widgets

import glib.gboolean
import glib.gint
import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkIconViewDropPosition.*
import kotlinx.cinterop.*
import org.gtk.gdk.ContentFormats
import org.gtk.gdk.Paintable
import org.gtk.gdk.Paintable.Companion.wrap
import org.gtk.gdk.Rectangle
import org.gtk.glib.WrappedKList
import org.gtk.glib.WrappedKList.Companion.asWrappedKList
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.glib.usePointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gtk.*
import org.gtk.gtk.TreeIter.Companion.wrap
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gtk.TreePath.Companion.wrap
import org.gtk.gtk.cellrenderer.CellRenderer
import org.gtk.gtk.cellrenderer.CellRenderer.Companion.wrap
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.common.enums.MovementStep
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.common.enums.SelectionMode
import gtk.GtkIconViewDropPosition.Var as GtkIconViewDropPositionVar

/**
 * 26 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.IconView.html">
 *     GtkIconView</a>
 */
class IconView(
	val iconViewPointer: GtkIconView_autoptr,
) : Widget(iconViewPointer.reinterpret()),
	CellLayout,
	Scrollable {

	override val cellLayoutHolder: CPointer<GtkCellLayout> by lazy {
		iconViewPointer.reinterpret()
	}

	override val scrollablePointer: CPointer<GtkScrollable> by lazy {
		iconViewPointer.reinterpret()
	}

	constructor() :
			this(gtk_icon_view_new()!!.reinterpret())

	constructor(area: CellArea) :
			this(
				gtk_icon_view_new_with_area(
					area.cellAreaPointer
				)!!.reinterpret()
			)

	constructor(model: TreeModel) :
			this(
				gtk_icon_view_new_with_model(
					model.treeModelPointer
				)!!.reinterpret()
			)

	fun createDragIcon(path: TreePath): Paintable =
		gtk_icon_view_create_drag_icon(
			iconViewPointer,
			path.treePathPointer
		)!!.wrap()

	fun enableModelDragDest(
		formats: ContentFormats,
		actions: GdkDragAction,
	) {
		gtk_icon_view_enable_model_drag_dest(
			iconViewPointer,
			formats.contentFormatsPointer,
			actions
		)
	}

	fun enableModelDragSource(
		startButtonMask: GdkModifierType,
		formats: ContentFormats,
		actions: GdkDragAction,
	) {
		gtk_icon_view_enable_model_drag_source(
			iconViewPointer,
			startButtonMask,
			formats.contentFormatsPointer,
			actions
		)
	}

	var activateOnSingleClick: Boolean
		get() = gtk_icon_view_get_activate_on_single_click(iconViewPointer).bool
		set(value) = gtk_icon_view_set_activate_on_single_click(iconViewPointer, value.gtk)

	fun getCellRect(path: TreePath, cell: CellRenderer): Rectangle =
		memScoped {
			val rect = cValue<GdkRectangle>()
			gtk_icon_view_get_cell_rect(
				iconViewPointer,
				path.treePathPointer,
				cell.cellRendererPointer,
				rect
			)
			Rectangle(rect.ptr)
		}

	var columnSpacing: Int
		get() = gtk_icon_view_get_column_spacing(iconViewPointer)
		set(value) = gtk_icon_view_set_column_spacing(iconViewPointer, value)

	var columns: Int
		get() = gtk_icon_view_get_columns(iconViewPointer)
		set(value) = gtk_icon_view_set_columns(iconViewPointer, value)

	/**
	 * @param path current cursor path, must be freed
	 */
	data class IconViewCursor(
		val path: TreePath? = null,
		val cell: CellRenderer? = null,
	)

	val iconViewCursor: IconViewCursor
		get() = memScoped {
			val path = allocPointerTo<GtkTreePath>()
			val cell = allocPointerTo<GtkCellRenderer>()

			gtk_icon_view_get_cursor(
				iconViewPointer,
				path.ptr,
				cell.ptr
			)

			IconViewCursor(
				path.value.wrap(),
				cell.value.wrap()
			)
		}

	data class IconViewDestinationItem(
		val path: TreePath?,
		val pos: DropPosition,
	)

	fun getDestItemAtPos(
		dragX: Int,
		dragY: Int,
	): IconViewDestinationItem =
		memScoped {
			val path = allocPointerTo<GtkTreePath>()
			val pos: GtkIconViewDropPositionVar = alloc()

			gtk_icon_view_get_dest_item_at_pos(
				iconViewPointer,
				dragX,
				dragY,
				path.ptr,
				pos.ptr
			)
			IconViewDestinationItem(
				path.value.wrap(),
				DropPosition.valueOf(pos.value)
			)
		}

	val dragDestItem: IconViewDestinationItem
		get() = memScoped {
			val path = allocPointerTo<GtkTreePath>()
			val pos: GtkIconViewDropPositionVar = alloc()

			gtk_icon_view_get_drag_dest_item(
				iconViewPointer,
				path.ptr,
				pos.ptr
			)

			IconViewDestinationItem(
				path.value.wrap(),
				DropPosition.valueOf(pos.value)
			)
		}


	data class IconViewItem(
		val path: TreePath?,
		val cell: CellRenderer?,
	)

	fun getItemAtPos(
		x: Int,
		y: Int,
	): IconViewItem? =
		memScoped {
			val path = allocPointerTo<GtkTreePath>()
			val cell = allocPointerTo<GtkCellRenderer>()

			val r = gtk_icon_view_get_item_at_pos(
				iconViewPointer,
				x,
				y,
				path.ptr,
				cell.ptr
			)

			if (r.bool)
				IconViewItem(
					path.value.wrap(),
					cell.value.wrap()
				)
			else null
		}

	fun getItemColumn(path: TreePath): Int =
		gtk_icon_view_get_item_column(iconViewPointer, path.treePathPointer)

	var itemOrientation: Orientation
		get() = Orientation.valueOf(
			gtk_icon_view_get_item_orientation(iconViewPointer)
		)
		set(value) =
			gtk_icon_view_set_item_orientation(iconViewPointer, value.gtk)

	var itemPadding: Int
		get() = gtk_icon_view_get_item_padding(iconViewPointer)
		set(value) = gtk_icon_view_set_item_padding(iconViewPointer, value)

	fun getItemRow(path: TreePath): Int =
		gtk_icon_view_get_item_row(iconViewPointer, path.treePathPointer)

	var itemWidth: Int
		get() = gtk_icon_view_get_item_width(iconViewPointer)
		set(value) = gtk_icon_view_set_item_width(iconViewPointer, value)

	var margin: Int
		get() = gtk_icon_view_get_margin(iconViewPointer)
		set(value) = gtk_icon_view_set_margin(iconViewPointer, value)

	var markupColumn: Int
		get() = gtk_icon_view_get_markup_column(iconViewPointer)
		set(value) = gtk_icon_view_set_markup_column(iconViewPointer, value)

	var model: TreeModel?
		get() = gtk_icon_view_get_model(iconViewPointer).wrap()
		set(value) =
			gtk_icon_view_set_model(iconViewPointer, value?.treeModelPointer)

	fun getPathAtPos(x: Int, y: Int): TreePath? =
		gtk_icon_view_get_path_at_pos(iconViewPointer, x, y).wrap()

	var pixbufColumn: Int
		get() = gtk_icon_view_get_pixbuf_column(iconViewPointer)
		set(value) = gtk_icon_view_set_pixbuf_column(iconViewPointer, value)

	var reorderable: Boolean
		get() = gtk_icon_view_get_reorderable(iconViewPointer).bool
		set(value) = gtk_icon_view_set_reorderable(iconViewPointer, value.gtk)

	val selectedItems: WrappedKList<TreePath>
		get() = gtk_icon_view_get_selected_items(iconViewPointer)!!
			.asWrappedKList(
				{ reinterpret<GtkTreePath>().wrap() },
				{ treePathPointer }
			)

	var selectionMode: SelectionMode
		get() = SelectionMode.valueOf(
			gtk_icon_view_get_selection_mode(iconViewPointer)
		)
		set(value) = gtk_icon_view_set_selection_mode(iconViewPointer, value.gtk)

	var spacing: Int
		get() = gtk_icon_view_get_spacing(iconViewPointer)
		set(value) = gtk_icon_view_set_spacing(iconViewPointer, value)

	var textColumn: Int
		get() = gtk_icon_view_get_text_column(iconViewPointer)
		set(value) = gtk_icon_view_set_text_column(iconViewPointer, value)

	var tooltipColumn: Int
		get() = gtk_icon_view_get_tooltip_column(iconViewPointer)
		set(value) = gtk_icon_view_set_tooltip_column(iconViewPointer, value)

	data class TipContext(
		val model: TreeModel,
		val path: TreePath,
		val iter: TreeIter,
	)

	fun getTooltipContext(x: Int, y: Int, keyboardTip: Boolean): TipContext? =
		memScoped {
			val model = allocPointerTo<GtkTreeModel>()
			val path = allocPointerTo<GtkTreePath>()
			val iter = alloc<GtkTreeIter>()

			val r = gtk_icon_view_get_tooltip_context(
				iconViewPointer,
				x,
				y,
				keyboardTip.gtk,
				model.ptr,
				path.ptr,
				iter.ptr
			)

			if (r.bool)
				TipContext(
					model.value!!.wrap(),
					path.value!!.wrap(),
					iter.ptr.wrap()
				)
			else null
		}


	data class VisibleRange(
		val startPath: TreePath,
		val endPath: TreePath,
	)

	val visibleRange: VisibleRange?
		get() = memScoped {
			val startPath = allocPointerTo<GtkTreePath>()
			val endPath = allocPointerTo<GtkTreePath>()

			val r = gtk_icon_view_get_visible_range(
				iconViewPointer,
				startPath.ptr,
				endPath.ptr
			).bool

			if (r)
				VisibleRange(
					startPath.value!!.wrap(),
					endPath.value!!.wrap()
				)
			else null
		}

	fun itemActivated(path: TreePath) {
		gtk_icon_view_item_activated(iconViewPointer, path.treePathPointer)
	}

	fun isPathSelected(path: TreePath): Boolean =
		gtk_icon_view_path_is_selected(
			iconViewPointer,
			path.treePathPointer
		).bool

	fun scrollToPath(
		path: TreePath,
		useAlign: Boolean,
		rowAlign: Float,
		colAlign: Float,
	) {
		gtk_icon_view_scroll_to_path(
			iconViewPointer,
			path.treePathPointer,
			useAlign.gtk,
			rowAlign,
			colAlign
		)
	}

	fun selectAll() {
		gtk_icon_view_select_all(iconViewPointer)
	}

	fun selectPath(path: TreePath) {
		gtk_icon_view_select_path(iconViewPointer, path.treePathPointer)
	}

	fun selectedForeach(func: IconViewForEachFunc) {
		StableRef.create(func).usePointer {
			gtk_icon_view_selected_foreach(
				iconViewPointer,
				staticForeachFunction,
				it
			)
		}
	}

	fun setCursor(path: TreePath, cell: CellRenderer?, startEditing: Boolean) {
		gtk_icon_view_set_cursor(
			iconViewPointer,
			path.treePathPointer,
			cell?.cellRendererPointer,
			startEditing.gtk
		)
	}

	fun setDragDestItem(path: TreePath?, pos: DropPosition) {
		gtk_icon_view_set_drag_dest_item(
			iconViewPointer,
			path?.treePathPointer,
			pos.gtk
		)
	}

	fun setTooltipItem(tooltip: Tooltip, path: TreePath) {
		gtk_icon_view_set_tooltip_item(
			iconViewPointer,
			tooltip.tooltipPointer,
			path.treePathPointer
		)
	}

	fun unselectAll() {
		gtk_icon_view_unselect_all(iconViewPointer)
	}

	fun unselectPath(path: TreePath) {
		gtk_icon_view_unselect_path(iconViewPointer, path.treePathPointer)
	}

	fun unsetModelDragDest() {
		gtk_icon_view_unset_model_drag_dest(iconViewPointer)
	}

	fun unsetModelDragSource() {
		gtk_icon_view_unset_model_drag_source(iconViewPointer)
	}

	fun addOnActivateCursorItemCallback(
		action: IconViewActivateCursorItemFunc,
	) = addSignalCallback(
		Signals.ACTIVATE_CURSOR_ITEM,
		action,
		staticActivateCursorFunc
	)

	fun addOnItemActivatedCallback(action: IconViewItemActivatedFunc) =
		addSignalCallback(
			Signals.ITEM_ACTIVATED,
			action,
			staticItemActivatedFunc
		)

	fun addOnMoveCursorCallback(action: IconViewMoveCursorFunc) =
		addSignalCallback(Signals.MOVE_CURSOR, action, staticMoveCursorFunc)

	fun addOnSelectAllCallback(action: TypedNoArgFunc<IconView>) =
		addSignalCallback(Signals.SELECT_ALL, action, staticNoArgGCallback)

	fun addOnSelectCursorItemCallback(action: TypedNoArgFunc<IconView>) =
		addSignalCallback(
			Signals.SELECT_CURSOR_ITEM,
			action,
			staticNoArgGCallback
		)

	fun addOnSelectionChangedCallback(action: TypedNoArgFunc<IconView>) =
		addSignalCallback(
			Signals.SELECTION_CHANGED,
			action,
			staticNoArgGCallback
		)

	fun addOnToggleCursorItemCallback(action: TypedNoArgFunc<IconView>) =
		addSignalCallback(
			Signals.TOGGLE_CURSOR_ITEM,
			action,
			staticNoArgGCallback
		)

	fun addOnUnselectAllCallback(action: TypedNoArgFunc<IconView>) =
		addSignalCallback(Signals.UNSELECT_ALL, action, staticNoArgGCallback)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/enum.IconViewDropPosition.html">
	 *     GtkIconViewDropPosition</a>
	 */
	enum class DropPosition(
		val gtk: GtkIconViewDropPosition,
	) {
		NO_DROP(GTK_ICON_VIEW_NO_DROP),
		DROP_INTO(GTK_ICON_VIEW_DROP_INTO),
		DROP_LEFT(GTK_ICON_VIEW_DROP_LEFT),
		DROP_RIGHT(GTK_ICON_VIEW_DROP_RIGHT),
		DROP_ABOVE(GTK_ICON_VIEW_DROP_ABOVE),
		DROP_BELOW(GTK_ICON_VIEW_DROP_BELOW);

		companion object {
			fun valueOf(gtk: GtkIconViewDropPosition) =
				when (gtk) {
					GTK_ICON_VIEW_NO_DROP -> NO_DROP
					GTK_ICON_VIEW_DROP_INTO -> DROP_INTO
					GTK_ICON_VIEW_DROP_LEFT -> DROP_LEFT
					GTK_ICON_VIEW_DROP_RIGHT -> DROP_RIGHT
					GTK_ICON_VIEW_DROP_ABOVE -> DROP_ABOVE
					GTK_ICON_VIEW_DROP_BELOW -> DROP_BELOW
				}
		}
	}

	companion object {
		inline fun GtkIconView_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkIconView_autoptr.wrap() =
			IconView(this)

		private val staticForeachFunction: GtkIconViewForeachFunc =
			staticCFunction { icon_view, path, data ->
				data!!.asStableRef<IconViewForEachFunc>()
					.get()
					.invoke(icon_view!!.wrap(), path!!.wrap())
			}

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkIconView_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<IconView>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticActivateCursorFunc: GCallback =
			staticCFunction {
					self: GtkIconView_autoptr,
					data: gpointer,
				->
				data.asStableRef<IconViewActivateCursorItemFunc>()
					.get()
					.invoke(self.wrap()).gtk
			}.reinterpret()

		private val staticItemActivatedFunc: GCallback =
			staticCFunction {
					self: GtkIconView_autoptr,
					path: GtkTreePath_autoptr,
					data: gpointer,
				->
				data.asStableRef<IconViewItemActivatedFunc>()
					.get()
					.invoke(self.wrap(), path.wrap())
			}.reinterpret()

		private val staticMoveCursorFunc: GCallback =
			staticCFunction {
					self: GtkIconView_autoptr,
					step: GtkMovementStep,
					count: gint,
					extend: gboolean,
					modify: gboolean,
					data: gpointer,
				->
				data.asStableRef<IconViewMoveCursorFunc>()
					.get()
					.invoke(self.wrap(),
						MovementStep.valueOf(step),
						count,
						extend.bool,
						modify.bool
					).gtk
			}.reinterpret()
	}
}

typealias IconViewForEachFunc =
		IconView.(
			path: TreePath,
		) -> Unit

typealias IconViewActivateCursorItemFunc = IconView.() -> Boolean

typealias IconViewItemActivatedFunc = IconView.(path: TreePath) -> Unit

typealias IconViewMoveCursorFunc =
		IconView.(
			step: MovementStep,
			count: Int,
			extend: Boolean,
			modify: Boolean,
		) -> Boolean
