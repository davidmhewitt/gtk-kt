package org.gtk.gtk.common.enums

import gtk.GtkPositionType
import gtk.GtkPositionType.*

/**
 * kotlinx-gtk
 * 06 / 03 / 2021
 */
enum class PositionType(val gtk: GtkPositionType) {
	LEFT(GTK_POS_LEFT),
	RIGHT(GTK_POS_RIGHT),
	TOP(GTK_POS_TOP),
	BOTTOM(GTK_POS_BOTTOM);

	companion object {

		fun valueOf(gtk: GtkPositionType) =
			when (gtk) {
				GTK_POS_LEFT -> LEFT
				GTK_POS_RIGHT -> RIGHT
				GTK_POS_TOP ->TOP
				GTK_POS_BOTTOM ->BOTTOM
			}
	}
}