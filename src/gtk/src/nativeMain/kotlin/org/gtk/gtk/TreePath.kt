package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toCValues
import org.gtk.Closeable
import org.gtk.ClosedException
import org.gtk.glib.asSequence

class TreePath(
		val treePathPointer: CPointer<GtkTreePath>
	) : Closeable {
		private var isClosed = false
		private inline fun getCloseException() =
			ClosedException("TreePath has been freed")

		fun free() {
			if (isClosed) throw getCloseException()
			gtk_tree_path_free(treePathPointer)
			isClosed = true
		}


		val indices: Sequence<Int>
			get() {
				if (isClosed) throw getCloseException()
				return gtk_tree_path_get_indices(treePathPointer).asSequence(depth)
			}

		val depth: Int
			get() {
				if (isClosed) throw getCloseException()
				return gtk_tree_path_get_depth(treePathPointer)
			}

		constructor() : this(gtk_tree_path_new()!!)

		constructor(vararg indices: Int) : this(
			gtk_tree_path_new_from_indicesv(
				indices.toCValues(),
				indices.size.toULong()
			)!!
		)

		override fun close() {
			free()
		}

		companion object {
			inline fun CPointer<GtkTreePath>?.wrap() =
				this?.wrap()

			inline fun CPointer<GtkTreePath>.wrap() =
				TreePath(this)
		}
	}