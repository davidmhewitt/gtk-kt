package org.gtk.gtk.common.callback

import glib.gpointer
import gtk.GtkTreeIter
import gtk.GtkTreeModel
import gtk.GtkTreeViewRowSeparatorFunc
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.gtk
import org.gtk.gtk.TreeIter
import org.gtk.gtk.TreeModel
import org.gtk.gtk.TreeModel.Companion.wrap
import org.gtk.gtk.common.enums.MovementStep

/*
 * 19 / 12 / 2021
 */

typealias TypedNoArgFunc<T> = T.() -> Unit

typealias TypedNoArgForBooleanFunc<T> = T.() -> Boolean

typealias TypedStringFunc<T> = T.(String) -> Unit

typealias TypedExtendedMoveCursorFunction<T> = T.(
	step: MovementStep,
	count: Int,
	extendSelection: Boolean,
) -> Unit

/**
 * @see <a href="https://docs.gtk.org/gtk4/callback.TreeViewRowSeparatorFunc.html">
 *     GtkTreeViewRowSeparatorFunc</a>
 */
typealias TreeViewRowSeparatorFunc = (TreeModel, TreeIter) -> Boolean

/**
 * Static function for [TreeViewRowSeparatorFunc]
 *
 * @see TreeViewRowSeparatorFunc
 */
val staticTreeViewRowSeparatorFunc: GtkTreeViewRowSeparatorFunc =
	staticCFunction {
			model: CPointer<GtkTreeModel>?,
			iter: CPointer<GtkTreeIter>?,
			data: gpointer?,
		->
		data!!.asStableRef<TreeViewRowSeparatorFunc>()
			.get()
			.invoke(
				model!!.wrap(),
				TreeIter(iter!!)
			).gtk
	}