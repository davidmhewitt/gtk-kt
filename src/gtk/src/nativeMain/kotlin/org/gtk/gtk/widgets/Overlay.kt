package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals.GET_CHILD_POSITION
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * kotlinx-gtk
 *
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Overlay.html">
 *     GtkOverlay</a>
 */
class Overlay(
	val overlayPointer: GtkOverlay_autoptr,
) : Widget(overlayPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_OVERLAY))


	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Overlay.new.html">
	 *     gtk_overlay_new</a>
	 */
	constructor() : this(gtk_overlay_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Overlay.add_overlay.html">
	 *     gtk_overlay_add_overlay</a>
	 */
	fun addOverlay(widget: Widget) {
		gtk_overlay_add_overlay(overlayPointer, widget.widgetPointer)
	}

	var child: Widget?
		get() = gtk_overlay_get_child(overlayPointer).wrap()
		set(value) = gtk_overlay_set_child(overlayPointer, value?.widgetPointer)

	fun getClipOverlay(widget: Widget): Boolean =
		gtk_overlay_get_clip_overlay(overlayPointer, widget.widgetPointer).bool

	fun getMeasureOverlay(widget: Widget): Boolean =
		gtk_overlay_get_measure_overlay(overlayPointer, widget.widgetPointer).bool

	fun removeOverlay(widget: Widget) {
		gtk_overlay_remove_overlay(overlayPointer, widget.widgetPointer)
	}

	fun setClipOverlay(widget: Widget, clipOverlay: Boolean) {
		gtk_overlay_set_clip_overlay(
			overlayPointer,
			widget.widgetPointer,
			clipOverlay.gtk
		)
	}

	fun setMeasureOverlay(widget: Widget, measure: Boolean) {
		gtk_overlay_set_measure_overlay(
			overlayPointer,
			widget.widgetPointer,
			measure.gtk
		)
	}

	fun addOnGetChildPositionCallback(action: OverlayGetChildPositionFunc) =
		addSignalCallback(GET_CHILD_POSITION, action, staticGetChildPositionFunc)

	companion object {
		inline fun GtkOverlay_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkOverlay_autoptr.wrap() =
			Overlay(this)

		private val staticGetChildPositionFunc: GCallback =
			staticCFunction {
					self: GtkOverlay_autoptr,
					widget: GtkWidget_autoptr,
					allocation: CPointer<GtkAllocation>,
					data: gpointer,
				->
				data.asStableRef<OverlayGetChildPositionFunc>()
					.get()
					.invoke(
						self.wrap(),
						widget.wrap(),
						allocation.wrap()
					).gtk
			}.reinterpret()
	}

}

typealias OverlayGetChildPositionFunc =
		Overlay.(
			widget: Widget,
			allocation: Rectangle,
		) -> Boolean