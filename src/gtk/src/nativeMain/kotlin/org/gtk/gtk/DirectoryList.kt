package org.gtk.gtk

import gio.GListModel
import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import kotlinx.cinterop.toKStringFromUtf8
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.gio.ListModel
import org.gtk.glib.CStringPointer
import org.gtk.glib.toNullTermCStringArray
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 27 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DirectoryList.html">GtkDirectoryList</a>
 */
class DirectoryList(val directoryListPointer: CPointer<GtkDirectoryList>) :
	KGObject(directoryListPointer.reinterpret()), ListModel {
	override val listModelPointer: CPointer<GListModel> by lazy { directoryListPointer.reinterpret() }

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.DirectoryList.new.html">gtk_directory_list_new</a>
	 */
	constructor(attributes: String?, file: File?) : this(gtk_directory_list_new(attributes, file?.filePointer)!!)

	var file: File?
		get() = gtk_directory_list_get_file(directoryListPointer).wrap()
		set(value) = gtk_directory_list_set_file(directoryListPointer, value?.filePointer)

	var attributes: String?
		get() = gtk_directory_list_get_attributes(directoryListPointer)?.toKStringFromUtf8()
		set(value) = gtk_directory_list_set_attributes(directoryListPointer, value)
}