package org.gtk.gtk.filter

import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.glib.Variant
import org.gtk.glib.Variant.Companion.wrap
import org.gtk.glib.toArray

/**
 * gtk-kt
 *
 * 21 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FileFilter.html">
 *     GtkFileFilter</a>
 */
class FileFilter(val fileFilterPointer: GtkFileFilter_autoptr) :
	Filter(fileFilterPointer.reinterpret()) {
	constructor() : this(gtk_file_filter_new()!!)

	constructor(variant: Variant) : this(gtk_file_filter_new_from_gvariant(variant.variantPointer)!!)

	fun addMimeType(mimeType: String) {
		gtk_file_filter_add_mime_type(fileFilterPointer, mimeType)
	}

	fun addPattern(pattern: String) {
		gtk_file_filter_add_pattern(fileFilterPointer, pattern)
	}

	fun addPixbufFormats() {
		gtk_file_filter_add_pixbuf_formats(fileFilterPointer)
	}

	fun addSuffix(suffix: String) {
		gtk_file_filter_add_suffix(fileFilterPointer, suffix)
	}

	val attributes: Array<String>
		get() = gtk_file_filter_get_attributes(fileFilterPointer)!!.toArray()

	var name: String?
		get() = gtk_file_filter_get_name(fileFilterPointer)?.toKString()
		set(value) = gtk_file_filter_set_name(fileFilterPointer, value)

	fun toVariant(): Variant =
		gtk_file_filter_to_gvariant(fileFilterPointer)!!.wrap()
}