package org.gtk.gtk.widgets

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gtk.*
import gtk.GtkTextExtendSelection.GTK_TEXT_EXTEND_SELECTION_LINE
import gtk.GtkTextExtendSelection.GTK_TEXT_EXTEND_SELECTION_WORD
import gtk.GtkTextViewLayer.GTK_TEXT_VIEW_LAYER_ABOVE_TEXT
import gtk.GtkTextViewLayer.GTK_TEXT_VIEW_LAYER_BELOW_TEXT
import gtk.GtkWrapMode.*
import kotlinx.cinterop.*
import org.gtk.gdk.Event
import org.gtk.gdk.Rectangle
import org.gtk.gdk.Rectangle.Companion.wrap
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.CStringPointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.SignalManager
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.*
import org.gtk.gtk.TextBuffer.Companion.wrap
import org.gtk.gtk.TextIter.Companion.wrap
import org.gtk.gtk.common.callback.TypedExtendedMoveCursorFunction
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.common.callback.TypedStringFunc
import org.gtk.gtk.common.data.Coordinates
import org.gtk.gtk.common.enums.*
import org.gtk.pango.Context
import org.gtk.pango.Context.Companion.wrap
import org.gtk.pango.TabArray
import org.gtk.pango.TabArray.Companion.wrap

/**
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.TextView.html">
 *     GtkTextView</a>
 */
class TextView(
	val textViewPointer: GtkTextView_autoptr,
) : Widget(
	textViewPointer.reinterpret()
), Scrollable {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_TEXT_VIEW))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.TextView.new.html">
	 *     gtk_text_view_new</a>
	 */
	constructor() : this(gtk_text_view_new()!!.reinterpret())

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.TextView.new_with_buffer.html">
	 *     gtk_text_view_new_with_buffer</a>
	 */
	constructor(textBuffer: TextBuffer) : this(
		gtk_text_view_new_with_buffer(
			textBuffer.textBufferPointer
		)!!.reinterpret()
	)

	override val scrollablePointer: CPointer<GtkScrollable>
		get() = textViewPointer.reinterpret()

	fun addChildAtAnchor(child: Widget, anchor: TextChildAnchor) {
		gtk_text_view_add_child_at_anchor(textViewPointer,
			child.widgetPointer,
			anchor.textChildAnchorPointer)
	}

	fun addOverlay(child: Widget, xpos: Int, ypos: Int) {
		gtk_text_view_add_overlay(
			textViewPointer,
			child.widgetPointer,
			xpos,
			ypos
		)
	}

	fun backwardDisplayLine(iter: TextIter): Boolean =
		gtk_text_view_backward_display_line(textViewPointer, iter.pointer).bool

	fun backwardDisplayLineStart(iter: TextIter): Boolean =
		gtk_text_view_backward_display_line_start(
			textViewPointer,
			iter.pointer
		).bool

	fun bufferToWindowCoords(
		winType: TextWindowType,
		bufferX: Int,
		bufferY: Int,
	): Coordinates =
		memScoped {
			val y = cValue<IntVar>()
			val x = cValue<IntVar>()

			gtk_text_view_buffer_to_window_coords(textViewPointer,
				winType.gtk,
				bufferX,
				bufferY,
				x,
				y)

			Coordinates(
				x.ptr.pointed.value,
				y.ptr.pointed.value
			)
		}

	fun forwardDisplayLine(iter: TextIter): Boolean =
		gtk_text_view_forward_display_line(textViewPointer, iter.pointer).bool

	fun forwardDisplayLineEnd(iter: TextIter): Boolean =
		gtk_text_view_forward_display_line_end(
			textViewPointer,
			iter.pointer
		).bool

	var acceptsTab: Boolean
		get() = gtk_text_view_get_accepts_tab(textViewPointer).bool
		set(value) = gtk_text_view_set_accepts_tab(textViewPointer, value.gtk)

	var bottomMargin: Int
		get() = gtk_text_view_get_bottom_margin(textViewPointer)
		set(value) = gtk_text_view_set_bottom_margin(textViewPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.TextView.get_buffer.html">
	 *     gtk_text_view_get_buffer</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.TextView.set_buffer.html">
	 *     gtk_text_view_set_buffer</a>
	 */
	var buffer: TextBuffer?
		get() = gtk_text_view_get_buffer(textViewPointer).wrap()
		set(value) = gtk_text_view_set_buffer(
			textViewPointer,
			value?.textBufferPointer
		)

	data class CursorLocations(
		val strong: Rectangle?,
		val weak: Rectangle?,
	)

	fun getCursorLocations(iter: TextIter): CursorLocations =
		memScoped {
			val strong = cValue<GdkRectangle>()
			val weak = cValue<GdkRectangle>()

			gtk_text_view_get_cursor_locations(
				textViewPointer,
				iter.pointer,
				strong,
				weak
			)

			CursorLocations(
				strong.ptr.wrap(),
				weak.ptr.wrap()
			)
		}

	var cursorVisible: Boolean
		get() = gtk_text_view_get_cursor_visible(textViewPointer).bool
		set(value) = gtk_text_view_set_cursor_visible(
			textViewPointer,
			value.gtk
		)

	var editable: Boolean
		get() = gtk_text_view_get_editable(textViewPointer).bool
		set(value) = gtk_text_view_set_editable(textViewPointer, value.gtk)

	var extraMenu: MenuModel
		get() = gtk_text_view_get_extra_menu(textViewPointer)!!.wrap()
		set(value) = gtk_text_view_set_extra_menu(
			textViewPointer,
			value.menuModelPointer
		)

	fun getGutter(win: TextWindowType): Widget? =
		gtk_text_view_get_gutter(textViewPointer, win.gtk).wrap()

	var indent: Int
		get() = gtk_text_view_get_indent(textViewPointer)
		set(value) = gtk_text_view_set_indent(textViewPointer, value)

	var inputHints: GtkInputHints
		get() = gtk_text_view_get_input_hints(textViewPointer)
		set(value) = gtk_text_view_set_input_hints(textViewPointer, value)

	var inputPurpose: InputPurpose
		get() = InputPurpose.valueOf(
			gtk_text_view_get_input_purpose(textViewPointer)
		)
		set(value) = gtk_text_view_set_input_purpose(textViewPointer, value.gtk)

	fun getIterAtLocation(x: Int, y: Int): TextIter? = memScoped {
		val iter = cValue<GtkTextIter>()

		val r = gtk_text_view_get_iter_at_location(
			textViewPointer,
			iter.ptr,
			x,
			y
		).bool

		if (r)
			iter.ptr.wrap()
		else null
	}

	fun getIterAtPosition(x: Int, y: Int): Pair<TextIter, Int> =
		memScoped {
			val trailing = cValue<IntVar>()
			val iter = cValue<GtkTextIter>()

			gtk_text_view_get_iter_at_position(
				textViewPointer,
				iter,
				trailing,
				x,
				y
			)

			iter.ptr.wrap() to trailing.ptr.pointed.value
		}

	fun getIterLocation(iter: TextIter): Rectangle =
		memScoped {
			val rectangle = cValue<GdkRectangle>()

			gtk_text_view_get_iter_location(
				textViewPointer,
				iter.pointer,
				rectangle
			)

			rectangle.ptr.wrap()
		}

	var justification: Justification
		get() = Justification.valueOf(
			gtk_text_view_get_justification(textViewPointer)
		)
		set(value) = gtk_text_view_set_justification(textViewPointer, value.gtk)

	var leftMargin: Int
		get() = gtk_text_view_get_left_margin(textViewPointer)
		set(value) = gtk_text_view_set_left_margin(textViewPointer, value)

	data class LineAtY(
		val iter: TextIter,
		val topLine: Int,
	)

	fun getLineAtY(y: Int): LineAtY =
		memScoped {
			val targetIter = cValue<GtkTextIter>()
			val lineTop = cValue<IntVar>()

			gtk_text_view_get_line_at_y(textViewPointer, targetIter, y, lineTop)

			LineAtY(targetIter.ptr.wrap(), lineTop.ptr.pointed.value)
		}

	data class YRange(
		val y: Int,
		val height: Int,
	)

	fun getLineYRange(iter: TextIter): YRange =
		memScoped {
			val y = cValue<IntVar>()
			val height = cValue<IntVar>()
			gtk_text_view_get_line_yrange(
				textViewPointer,
				iter.pointer,
				y,
				height
			)
			YRange(y.ptr.pointed.value, height.ptr.pointed.value)
		}

	/**
	 * @since 4.4
	 */
	val ltrContext: Context
		get() = gtk_text_view_get_ltr_context(textViewPointer)!!.wrap()

	var monospace: Boolean
		get() = gtk_text_view_get_monospace(textViewPointer).bool
		set(value) = gtk_text_view_set_monospace(textViewPointer, value.gtk)

	var overwrite: Boolean
		get() = gtk_text_view_get_overwrite(textViewPointer).bool
		set(value) = gtk_text_view_set_overwrite(textViewPointer, value.gtk)

	var pixelsAboveLines: Int
		get() = gtk_text_view_get_pixels_above_lines(textViewPointer)
		set(value) = gtk_text_view_set_pixels_above_lines(
			textViewPointer,
			value
		)

	var pixelsBelowLines: Int
		get() = gtk_text_view_get_pixels_below_lines(textViewPointer)
		set(value) = gtk_text_view_set_pixels_below_lines(
			textViewPointer,
			value
		)

	var pixelsInsideWrap: Int
		get() = gtk_text_view_get_pixels_inside_wrap(textViewPointer)
		set(value) = gtk_text_view_set_pixels_inside_wrap(
			textViewPointer,
			value
		)

	var rightMargin: Int
		get() = gtk_text_view_get_right_margin(textViewPointer)
		set(value) = gtk_text_view_set_right_margin(textViewPointer, value)

	/**
	 * @since 4.4
	 */
	val rtlContext: Context
		get() = gtk_text_view_get_rtl_context(textViewPointer)!!.wrap()

	var tabs: TabArray?
		get() = gtk_text_view_get_tabs(textViewPointer).wrap()
		set(value) = gtk_text_view_set_tabs(
			textViewPointer,
			value?.tabArrayPointer
		)

	var topMargin: Int
		get() = gtk_text_view_get_top_margin(textViewPointer)
		set(value) = gtk_text_view_set_top_margin(textViewPointer, value)

	val visibleRectangle: Rectangle
		get() = memScoped {
			val rectangle = cValue<GdkRectangle>()
			gtk_text_view_get_visible_rect(textViewPointer, rectangle)
			rectangle.ptr.wrap()
		}

	var wrapMode: WrapMode
		get() = WrapMode.valueOf(gtk_text_view_get_wrap_mode(textViewPointer))
		set(value) = gtk_text_view_set_wrap_mode(textViewPointer, value.gtk)

	fun imContextFilterKeypress(event: Event): Boolean =
		gtk_text_view_im_context_filter_keypress(
			textViewPointer,
			event.eventPointer
		).bool

	fun moveMarkOnscreen(mark: TextMark): Boolean =
		gtk_text_view_move_mark_onscreen(textViewPointer, mark.markPointer).bool

	fun moveOverlay(child: Widget, xpos: Int, ypos: Int) {
		gtk_text_view_move_overlay(
			textViewPointer,
			child.widgetPointer,
			xpos,
			ypos
		)
	}

	fun moveVisually(iter: TextIter, count: Int): Boolean =
		gtk_text_view_move_visually(textViewPointer, iter.pointer, count).bool

	fun placeCursorOnscreen(): Boolean =
		gtk_text_view_place_cursor_onscreen(textViewPointer).bool

	fun remove(child: Widget) {
		gtk_text_view_remove(textViewPointer, child.widgetPointer)
	}

	fun resetCursorBlink() {
		gtk_text_view_reset_cursor_blink(textViewPointer)
	}

	fun resetImContext() {
		gtk_text_view_reset_im_context(textViewPointer)
	}

	fun scrollMarkOnscreen(mark: TextMark) {
		gtk_text_view_scroll_mark_onscreen(textViewPointer, mark.markPointer)
	}

	fun scrollToIter(
		iter: TextIter,
		withinMargin: Double,
		useAlign: Boolean,
		xAlign: Double,
		yAlign: Double,
	) {
		gtk_text_view_scroll_to_iter(textViewPointer,
			iter.pointer,
			withinMargin,
			useAlign.gtk,
			xAlign,
			yAlign)
	}

	fun scrollToMark(
		mark: TextMark,
		withinMargin: Double,
		useAlign: Boolean,
		xAlign: Double,
		yAlign: Double,
	) {
		gtk_text_view_scroll_to_mark(textViewPointer,
			mark.markPointer,
			withinMargin,
			useAlign.gtk,
			xAlign,
			yAlign)
	}

	fun setGutter(winType: TextWindowType, widget: Widget?) {
		gtk_text_view_set_gutter(
			textViewPointer,
			winType.gtk,
			widget?.widgetPointer
		)
	}

	fun startsDisplayLine(iter: TextIter): Boolean =
		gtk_text_view_starts_display_line(textViewPointer, iter.pointer).bool

	fun windowToBufferCords(
		winType: TextWindowType,
		windowX: Int,
		windowY: Int,
	): Coordinates =
		memScoped {
			val y = cValue<IntVar>()
			val x = cValue<IntVar>()

			gtk_text_view_window_to_buffer_coords(textViewPointer,
				winType.gtk,
				windowX,
				windowY,
				x,
				y)

			Coordinates(
				x.ptr.pointed.value,
				y.ptr.pointed.value
			)
		}


	/**
	 * <a href="https://docs.gtk.org/gtk4/vfunc.TextView.backspace.html">
	 *     backspace</a>
	 */
	fun addOnBackspaceCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(Signals.BACKSPACE, action, staticNoArgGCallback)

	/**
	 * <a href="https://docs.gtk.org/gtk4/vfunc.TextView.copy_clipboard.html">
	 *     copy-clipboard</a>
	 */
	fun addOnCopyClipboardCallback(
		action: TypedNoArgFunc<TextView>
	): SignalManager =
		addSignalCallback(Signals.COPY_CLIPBOARD, action, staticNoArgGCallback)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.cut-clipboard.html">
	 *     cut-clipboard</a>
	 */
	fun addOnCutClipboardCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(Signals.CUT_CLIPBOARD, action, staticNoArgGCallback)


	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.delete-from-cursor.html">
	 *     delete-from-cursor</a>
	 */
	fun addOnDeleteFromCursorCallback(action: TextViewDeleteFromCursorFunc) =
		addSignalCallback(
			Signals.DELETE_FROM_CURSOR,
			action,
			staticDeleteFromCursorCallback
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.extend-selection.html">
	 *     extend-selection</a>
	 */
	fun addOnExtendSelectionCallback(action: TextViewExtendSelectionFunc) =
		addSignalCallback(
			Signals.EXTEND_SELECTION,
			action,
			staticExtendSelectionCallback
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.insert-at-cursor.html">
	 *     insert-at-cursor</a>
	 */
	fun addOnInsertAtCursorCallback(action: TypedStringFunc<TextView>) =
		addSignalCallback(
			Signals.INSERT_AT_CURSOR,
			action,
			staticInsertAtCursorCallback
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.insert-emoji.html">
	 *     insert-emoji</a>
	 */
	fun addOnInsertEmojiCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(Signals.INSERT_EMOJI, action, staticNoArgGCallback)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.move-cursor.html">
	 *     move-cursor</a>
	 */
	fun addOnMoveCursorCallback(
		action: TypedExtendedMoveCursorFunction<TextView>
	) =
		addSignalCallback(
			Signals.MOVE_CURSOR,
			action,
			staticExtendedMoveCursorFunction
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.move-viewport.html">
	 *     move-viewport</a>
	 */
	fun addOnMoveViewPortCallback(action: TextViewMoveViewPortFunc) =
		addSignalCallback(
			Signals.MOVE_VIEWPORT,
			action,
			staticMoveViewportCallback
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.paste-clipboard.html">
	 *     paste-clipboard</a>
	 */
	fun addOnPasteClipboardCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(Signals.PASTE_CLIPBOARD, action, staticNoArgGCallback)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.preedit-changed.html">
	 *     preedit-changed</a>
	 */
	fun addOnPreeditChangedCallback(action: TextViewPreEditChangedFunc) =
		addSignalCallback(
			Signals.PREEDIT_CHANGED,
			action,
			staticPreeditChangedCallback
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.select-all.html">
	 *     select-all</a>
	 */
	fun addOnSelectAllCallback(action: TextViewSelectAllFunc) =
		addSignalCallback(Signals.SELECT_ALL, action, staticSelectAllCallback)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.set-anchor.html">
	 *     set-anchor</a>
	 */
	fun addOnSetAnchorCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(Signals.SET_ANCHOR, action, staticNoArgGCallback)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.toggle-cursor-visible.html">
	 *     toggle-cursor-visible</a>
	 */
	fun addOnToggleCursorVisibleCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(
			Signals.TOGGLE_CURSOR_VISIBLE,
			action,
			staticNoArgGCallback
		)

	/**
	 * <a href="https://docs.gtk.org/gtk4/signal.TextView.toggle-overwrite.html">
	 *     toggle-overwrite</a>
	 */
	fun addOnToggleOverwriteCallback(action: TypedNoArgFunc<TextView>) =
		addSignalCallback(Signals.TOGGLE_OVERWRITE, action, staticNoArgGCallback)

	/**
	 * <a href=""></a>
	 */
	enum class Layer(
		val gtk: GtkTextViewLayer,
	) {
		BELOW_TEXT(GTK_TEXT_VIEW_LAYER_BELOW_TEXT),
		ABOVE_TEXT(GTK_TEXT_VIEW_LAYER_ABOVE_TEXT);

		companion object {

			fun valueOf(gtk: GtkTextViewLayer) =
				values().find { it.gtk == gtk }
		}
	}

	/**
	 * <a href="https://docs.gtk.org/gtk4/enum.TextWindowType.html">
	 *     GtkTextWindowType</a>
	 */
	enum class TextWindowType(
		val gtk: GtkTextWindowType,
	) {
		WIDGET(GTK_TEXT_WINDOW_WIDGET),
		TEXT(GTK_TEXT_WINDOW_TEXT),
		LEFT(GTK_TEXT_WINDOW_LEFT),
		RIGHT(GTK_TEXT_WINDOW_RIGHT),
		TOP(GTK_TEXT_WINDOW_TOP),
		BOTTOM(GTK_TEXT_WINDOW_BOTTOM);

		companion object {

			fun valueOf(gtk: GtkTextWindowType) =
				values()
					.find { it.gtk == gtk }
		}
	}

	/**
	 * <a href="https://docs.gtk.org/gtk4/enum.TextExtendSelection.html">
	 *     GtkTextExtendSelection</a>
	 */
	enum class TextExtendSelection(
		val gtk: GtkTextExtendSelection,
	) {
		WORD(GTK_TEXT_EXTEND_SELECTION_WORD),
		LINE(GTK_TEXT_EXTEND_SELECTION_LINE);

		companion object {

			fun valueOf(gtk: GtkTextExtendSelection) =
				when (gtk) {
					GTK_TEXT_EXTEND_SELECTION_WORD -> WORD
					GTK_TEXT_EXTEND_SELECTION_LINE -> LINE
				}
		}
	}

	/**
	 * <a href="https://docs.gtk.org/gtk4/enum.WrapMode.html">
	 *     GtkWrapMode</a>
	 */
	enum class WrapMode(
		val gtk: GtkWrapMode,
	) {
		NONE(GTK_WRAP_NONE),
		CHAR(GTK_WRAP_CHAR),
		WORD(GTK_WRAP_WORD),
		WORD_CHAR(GTK_WRAP_WORD_CHAR);

		companion object {
			fun valueOf(gtk: GtkWrapMode) =
				when (gtk) {
					GTK_WRAP_NONE -> NONE
					GTK_WRAP_CHAR -> CHAR
					GTK_WRAP_WORD -> WORD
					GTK_WRAP_WORD_CHAR -> WORD_CHAR
				}
		}
	}

	companion object {
		inline fun GtkTextView_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkTextView_autoptr.wrap() =
			TextView(this)

		private val staticNoArgGCallback: GCallback =
			staticCFunction { self: GtkTextView_autoptr, data: gpointer ->
				data.asStableRef<TypedNoArgFunc<TextView>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticDeleteFromCursorCallback: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					type: GtkDeleteType,
					count: Int,
					data: gpointer,
				->
				data.asStableRef<TextViewDeleteFromCursorFunc>()
					.get()
					.invoke(
						self.wrap(),
						DeleteType.valueOf(type),
						count
					)
			}.reinterpret()

		private val staticExtendSelectionCallback: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					granularity: GtkTextExtendSelection,
					location: CPointer<GtkTextIter>,
					start: CPointer<GtkTextIter>,
					end: CPointer<GtkTextIter>,
					data: gpointer,
				->
				data.asStableRef<TextViewExtendSelectionFunc>()
					.get()
					.invoke(
						self.wrap(),
						TextExtendSelection.valueOf(granularity),
						location.wrap(),
						start.wrap(),
						end.wrap(),
					)
				Unit
			}.reinterpret()

		private val staticInsertAtCursorCallback: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					string: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<TypedStringFunc<TextView>>()
					.get()
					.invoke(
						self.wrap(),
						string.toKString()
					)
			}.reinterpret()


		private val staticPreeditChangedCallback: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					preedit: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<TextViewPreEditChangedFunc>()
					.get()
					.invoke(self.wrap(), preedit.toKString())
			}.reinterpret()


		private val staticExtendedMoveCursorFunction: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					step: GtkMovementStep,
					count: Int,
					extendSelection: gboolean,
					data: gpointer,
				->
				data.asStableRef<TypedExtendedMoveCursorFunction<TextView>>()
					.get()
					.invoke(
						self.wrap(),
						MovementStep.valueOf(step),
						count,
						extendSelection.bool
					)
			}.reinterpret()

		private val staticMoveViewportCallback: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					step: GtkScrollStep,
					count: Int,
					data: gpointer,
				->
				data.asStableRef<TextViewMoveViewPortFunc>()
					.get()
					.invoke(
						self.wrap(),
						ScrollStep.valueOf(step)!!,
						count
					)
			}.reinterpret()


		private val staticSelectAllCallback: GCallback =
			staticCFunction {
					self: GtkTextView_autoptr,
					select: gboolean,
					data: gpointer?,
				->
				data?.asStableRef<TextViewSelectAllFunc>()?.get()
					?.invoke(self.wrap(), select.bool)
				Unit
			}.reinterpret()
	}
}

typealias TextViewDeleteFromCursorFunc =
		TextView.(
			type: DeleteType,
			count: Int,
		) -> Unit

typealias TextViewExtendSelectionFunc =
		TextView.(
			granularity: TextView.TextExtendSelection,
			location: TextIter,
			start: TextIter,
			end: TextIter,
		) -> Boolean

typealias TextViewMoveViewPortFunc =
		TextView.(
			step: ScrollStep,
			count: Int,
		) -> Unit

typealias TextViewPreEditChangedFunc = TextView.(preEdit: String) -> Unit

typealias TextViewSelectAllFunc = TextView.(select: Boolean) -> Unit