package org.gtk.gtk.widgets.windows.dialog

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.*
import org.gtk.gtk.widgets.Widget
import org.gtk.gtk.widgets.windows.Window

/**
 * kotlinx-gtk
 *
 * 08 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.FileChooserDialog.html#description">
 *     GtkFileChooserDialog</a>
 */
class FileChooserDialog(
	@Suppress("MemberVisibilityCanBePrivate")
	val fileChooserDialogPointer: GtkFileChooserDialog_autoptr
) : Dialog(fileChooserDialogPointer.reinterpret()),
	Accessible,
	Buildable,
	ConstraintTarget,
	FileChooser,
	Native,
	Root,
	ShortcutManager {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_FILE_CHOOSER_DIALOG))

	override val fileChooserPointer: CPointer<GtkFileChooser> by lazy { fileChooserDialogPointer.reinterpret() }

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.FileChooserDialog.new.html">
	 *     gtk_file_chooser_dialog_new</a>
	 */
	constructor(
		action: FileChooser.Action,
		parent: Window? = null,
		title: String? = null,
		yesString: String,
		noString: String
	) : this(
		gtk_file_chooser_dialog_new(
			title,
			parent?.windowPointer,
			action.gtk,
			yesString,
			GTK_RESPONSE_ACCEPT,
			noString,
			GTK_RESPONSE_CANCEL,
			null
		)!!.reinterpret()
	)
}