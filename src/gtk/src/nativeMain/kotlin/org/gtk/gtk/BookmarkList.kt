package org.gtk.gtk

import gio.GListModel_autoptr
import gtk.*
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.gtk.gio.ListModel
import org.gtk.glib.bool
import org.gtk.gobject.KGObject

/**
 * gtk-kt
 *
 * 30 / 10 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.BookmarkList.html">
 *     GtkBookmarkList</a>
 */
class BookmarkList(val bookmarkPointer: GtkBookmarkList_autoptr) :
	KGObject(bookmarkPointer.reinterpret()), ListModel {
	override val listModelPointer: GListModel_autoptr by lazy {
		bookmarkPointer.reinterpret()
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.BookmarkList.new.html">
	 *     gtk_bookmark_list_new</a>
	 */
	constructor(filename: String, attributes: String) :
			this(gtk_bookmark_list_new(filename, attributes)!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.BookmarkList.get_attributes.html">
	 *     gtk_bookmark_list_get_attributes</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.BookmarkList.set_attributes.html">
	 *     gtk_bookmark_list_set_attributes</a>
	 */
	var attributes: String
		get() = gtk_bookmark_list_get_attributes(bookmarkPointer)!!.toKString()
		set(value) = gtk_bookmark_list_set_attributes(bookmarkPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.BookmarkList.get_filename.html">
	 *     gtk_bookmark_list_get_filename</a>
	 */
	val filename: String
		get() = gtk_bookmark_list_get_filename(bookmarkPointer)!!.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.BookmarkList.get_io_priority.html">
	 *     gtk_bookmark_list_get_io_priority</a>
	 * @see <a href="https://docs.gtk.org/gtk4/method.BookmarkList.set_io_priority.html">
	 *     gtk_bookmark_list_set_io_priority</a>
	 */
	var ioPriority: Int
		get() = gtk_bookmark_list_get_io_priority(bookmarkPointer)
		set(value) = gtk_bookmark_list_set_io_priority(bookmarkPointer, value)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.BookmarkList.is_loading.html">
	 *     gtk_bookmark_list_is_loading</a>
	 */
	val isLoading: Boolean
		get() = gtk_bookmark_list_is_loading(bookmarkPointer).bool
}