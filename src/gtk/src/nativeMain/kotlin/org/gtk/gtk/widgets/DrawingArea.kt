package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.cairo.Cairo
import org.gtk.glib.asStablePointer
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.staticDestroyStableRefFunction
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * kotlinx-gtk
 * 26 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.DrawingArea.html">
 *     GtkDrawingArea</a>
 */
class DrawingArea(
	val drawingAreaPointer: GtkDrawingArea_autoptr,
) : Widget(drawingAreaPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_DRAWING_AREA))

	constructor() : this(gtk_drawing_area_new()!!.reinterpret())

	var contentHeight: Int
		get() = gtk_drawing_area_get_content_height(drawingAreaPointer)
		set(value) = gtk_drawing_area_set_content_height(drawingAreaPointer, value)

	var contentWidth: Int
		get() = gtk_drawing_area_get_content_width(drawingAreaPointer)
		set(value) = gtk_drawing_area_set_content_width(drawingAreaPointer, value)

	fun setOnDrawFunction(function: DrawingAreaDrawFunction?) {
		if (function == null) {
			gtk_drawing_area_set_draw_func(
				drawingAreaPointer,
				null,
				null,
				null
			)
			return
		}

		gtk_drawing_area_set_draw_func(
			drawingAreaPointer,
			staticDrawingAreaDrawFunction,
			function.asStablePointer(),
			staticDestroyStableRefFunction
		)
	}

	fun addOnResizeCallback(flags: UInt = 0u, action: DrawingAreaResizeFunction) =
		addSignalCallback(Signals.RESIZE, action, staticDrawingAreaResizeFunction, flags)

	companion object {
		inline fun GtkDrawingArea_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkDrawingArea_autoptr.wrap() =
			DrawingArea(this)

		private val staticDrawingAreaDrawFunction: GtkDrawingAreaDrawFunc =
			staticCFunction { self: GtkDrawingArea_autoptr?, cr, width, height, data ->
				data?.asStableRef<DrawingAreaDrawFunction>()?.get()
					?.invoke(self!!.wrap(), Cairo(cr!!), width, height)
				Unit
			}

		private val staticDrawingAreaResizeFunction: GCallback =
			staticCFunction {
					self: GtkDrawingArea_autoptr,
					width: Int,
					height: Int,
					data: gpointer,
				->
				data.asStableRef<DrawingAreaResizeFunction>()
					.get()
					.invoke(self.wrap(), width, height)
				Unit
			}.reinterpret()
	}
}

typealias DrawingAreaDrawFunction =
		DrawingArea.(
			Cairo,
			width: Int,
			height: Int,
		) -> Unit

typealias DrawingAreaResizeFunction =
		DrawingArea.(
			width: Int,
			height: Int,
		) -> Unit