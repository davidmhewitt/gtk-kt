package org.gtk.gtk.cellrenderer

import gtk.GtkCellRenderer
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * @see <a href="https://docs.gtk.org/gtk4/class.CellRenderer.html">
 *     GtkCellRenderer</a>
 */
open class CellRenderer(
	val cellRendererPointer: CPointer<GtkCellRenderer>
) : KGObject(cellRendererPointer.reinterpret()) {


	companion object {
		inline fun CPointer<GtkCellRenderer>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkCellRenderer>.wrap() =
			CellRenderer(this)
	}


}