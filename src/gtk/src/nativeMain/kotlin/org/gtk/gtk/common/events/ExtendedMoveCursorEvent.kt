package org.gtk.gtk.common.events

import glib.gboolean
import glib.gpointer
import gobject.GCallback
import gtk.GtkMovementStep
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.gtk.common.enums.MovementStep


@Deprecated("Self reference instead")
typealias ExtendedMoveCursorFunction = (
	MovementStep,
	@ParameterName("count") Int,
	@ParameterName("extendSelection") Boolean,
) -> Unit



@Deprecated("Self reference instead")
internal val staticExtendedMoveCursorFunction: GCallback =
	staticCFunction {
			_: gpointer?,
			step: GtkMovementStep,
			count: Int,
			extendSelection: gboolean,
			data: gpointer?,
		->
		data?.asStableRef<ExtendedMoveCursorFunction>()?.get()
			?.invoke(
				MovementStep.valueOf(step)!!,
				count,
				extendSelection.bool
			)
		Unit
	}.reinterpret()