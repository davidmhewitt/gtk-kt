package org.gtk.gtk.itemfactory

import gtk.GtkBuilderListItemFactory
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.BuilderListItemFactory.html">
 *     GtkBuilderListItemFactory</a>
 */
class BuilderListItemFactory(
	val builderListItemFactoryPointer: CPointer<GtkBuilderListItemFactory>
) : ListItemFactory(builderListItemFactoryPointer.reinterpret()) {

}