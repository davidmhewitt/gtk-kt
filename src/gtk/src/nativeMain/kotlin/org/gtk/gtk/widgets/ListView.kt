package org.gtk.gtk.widgets

import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gtk.itemfactory.ListItemFactory
import org.gtk.gtk.itemfactory.ListItemFactory.Companion.wrap
import org.gtk.gtk.selection.SelectionModel
import org.gtk.gtk.selection.SelectionModel.Companion.wrap

/**
 * 27 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.ListView.html">GtkListView</a>
 */
class ListView(
	val listViewPointer: GtkListView_autoptr,
) : ListBase(listViewPointer.reinterpret()) {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_LIST_VIEW))

	constructor(model: SelectionModel?, factory: ListItemFactory?)
			: this(gtk_list_view_new(model?.selectionModelPointer,
		factory?.listItemFactoryPointer)!!.reinterpret())

	var isRubberbandEnabled: Boolean
		get() = gtk_list_view_get_enable_rubberband(listViewPointer).bool
		set(value) = gtk_list_view_set_enable_rubberband(listViewPointer, value.gtk)

	var factory: ListItemFactory?
		get() = gtk_list_view_get_factory(listViewPointer).wrap()
		set(value) = gtk_list_view_set_factory(listViewPointer, value?.listItemFactoryPointer)

	var model: SelectionModel?
		get() = gtk_list_view_get_model(listViewPointer).wrap()
		set(value) = gtk_list_view_set_model(listViewPointer, value?.selectionModelPointer)

	var showSeparators: Boolean
		get() = gtk_list_view_get_show_separators(listViewPointer).bool
		set(value) = gtk_list_view_set_show_separators(listViewPointer, value.gtk)

	var singleClickActivate: Boolean
		get() = gtk_list_view_get_single_click_activate(listViewPointer).bool
		set(value) = gtk_list_view_set_single_click_activate(listViewPointer, value.gtk)

	fun addOnActivateCallback(action: ListViewActivateFunction) =
		addSignalCallback(Signals.ACTIVATE, action, staticActivateFunction)

	companion object {
		inline fun GtkListView_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkListView_autoptr.wrap() =
			ListView(this)

		private val staticActivateFunction: GCallback =
			staticCFunction {
					self: GtkListView_autoptr,
					position: UInt,
					data: gpointer,
				->
				data.asStableRef<ListViewActivateFunction>()
					.get()
					.invoke(self.wrap(), position)
			}.reinterpret()
	}
}

typealias ListViewActivateFunction = ListView.(position: UInt) -> Unit