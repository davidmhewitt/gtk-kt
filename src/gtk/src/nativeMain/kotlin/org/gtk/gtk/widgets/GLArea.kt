package org.gtk.gtk.widgets

import glib.gint
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gdk.GLContext
import org.gtk.gdk.GLContext.Companion.wrap
import org.gtk.glib.KGError
import org.gtk.glib.KGError.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * 26 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.GLArea.html">
 *     GtkGLArea</a>
 */
class GLArea(
	val glAreaPointer: GtkGLArea_autoptr,
) : Widget(glAreaPointer.reinterpret()) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_GL_AREA))

	constructor() : this(gtk_gl_area_new()!!.reinterpret())

	fun attachBuffers() {
		gtk_gl_area_attach_buffers(glAreaPointer)
	}

	val getAutoRender: Boolean
		get() = gtk_gl_area_get_auto_render(glAreaPointer).bool

	val context: GLContext
		get() = gtk_gl_area_get_context(glAreaPointer)!!.wrap()

	val error: KGError?
		get() = gtk_gl_area_get_error(glAreaPointer).wrap()

	var hasDepthBuffer: Boolean
		get() = gtk_gl_area_get_has_depth_buffer(glAreaPointer).bool
		set(value) = gtk_gl_area_set_has_depth_buffer(glAreaPointer, value.gtk)

	var hasStencilBuffer: Boolean
		get() = gtk_gl_area_get_has_stencil_buffer(glAreaPointer).bool
		set(value) = gtk_gl_area_set_has_stencil_buffer(
			glAreaPointer,
			value.gtk
		)

	var requiredVersion: Version
		get() = memScoped {
			val major = cValue<IntVar>()
			val minor = cValue<IntVar>()

			gtk_gl_area_get_required_version(glAreaPointer, major, minor)

			Version(
				major.ptr.pointed.value,
				minor.ptr.pointed.value
			)
		}
		set(value) = gtk_gl_area_set_required_version(
			glAreaPointer,
			value.major,
			value.minor
		)

	var useEs: Boolean
		get() = gtk_gl_area_get_use_es(glAreaPointer).bool
		set(value) = gtk_gl_area_set_use_es(glAreaPointer, value.gtk)

	fun makeCurrent() {
		gtk_gl_area_make_current(glAreaPointer)
	}

	fun queueRender() {
		gtk_gl_area_queue_render(glAreaPointer)
	}

	fun addOnCreateContextCallback(action: GLAreaCreateContextFunc) =
		addSignalCallback(
			Signals.CREATE_CONTEXT,
			action,
			staticCreateContextFunc
		)

	fun addOnRenderCallback(action: GLAreaRenderFunc) =
		addSignalCallback(Signals.RENDER, action, staticRenderFunc)

	fun addOnResizeCallback(action: GLAreaResizeFunc) =
		addSignalCallback(Signals.RESIZE, action, staticResizeFunc)


	data class Version(val major: Int, val minor: Int)

	companion object {

		inline fun GtkGLArea_autoptr?.wrap() =
			this?.wrap()

		inline fun GtkGLArea_autoptr.wrap() =
			GLArea(this)

		private val staticCreateContextFunc: GCallback =
			staticCFunction {
					self: GtkGLArea_autoptr,
					data: gpointer,
				->
				data.asStableRef<GLAreaCreateContextFunc>()
					.get()
					.invoke(self.wrap()).glContextPointer
			}.reinterpret()

		private val staticRenderFunc: GCallback =
			staticCFunction {
					self: GtkGLArea_autoptr,
					context: GdkGLContext_autoptr,
					data: gpointer,
				->
				data.asStableRef<GLAreaRenderFunc>()
					.get()
					.invoke(self.wrap(), context.wrap()).gtk
			}.reinterpret()

		private val staticResizeFunc: GCallback =
			staticCFunction {
					self: GtkGLArea_autoptr,
					width: gint,
					height: gint,
					data: gpointer,
				->
				data.asStableRef<GLAreaResizeFunc>()
					.get()
					.invoke(self.wrap(), width, height)
			}.reinterpret()
	}

}

typealias GLAreaCreateContextFunc = GLArea.() -> GLContext

typealias GLAreaRenderFunc =
		GLArea.(
			context: GLContext,
		) -> Boolean

typealias GLAreaResizeFunc =
		GLArea.(
			width: Int,
			height: Int,
		) -> Unit