package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.*
import org.gtk.gobject.typeCheckInstanceCastOrThrow
import org.gtk.gsk.Transform
import org.gtk.gsk.Transform.Companion.wrap
import org.gtk.gtk.common.data.PreciseCoordinates

/**
 * kotlinx-gtk
 * 13 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Fixed.html">GtkFixed</a>
 */
class Fixed(
	val fixedPointer: CPointer<GtkFixed>,
) : Widget(
	fixedPointer.reinterpret()
) {

	constructor(widget: Widget) :
			this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_FIXED))

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Fixed.new.html">
	 *     gtk_fixed_new</a>
	 */
	constructor() :
			this(gtk_fixed_new()!!.reinterpret())

	fun getChildPosition(widget: Widget): PreciseCoordinates =
		memScoped {
			val x = cValue<DoubleVar>()
			val y = cValue<DoubleVar>()
			gtk_fixed_get_child_position(fixedPointer, widget.widgetPointer, x, y)

			PreciseCoordinates(
				x.ptr.pointed.value,
				y.ptr.pointed.value
			)
		}

	fun getChildTransform(widget: Widget): Transform? =
		gtk_fixed_get_child_transform(fixedPointer, widget.widgetPointer).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Fixed.move.html">
	 *     gtk_fixed_move</a>
	 */
	fun move(widget: Widget, x: Double, y: Double) {
		gtk_fixed_move(fixedPointer, widget.widgetPointer, x, y)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Fixed.put.html">
	 *     gtk_fixed_put</a>
	 *
	 * @return [FixedWidget] with direct management functions
	 */
	fun put(widget: Widget, x: Double, y: Double): FixedWidget {
		gtk_fixed_put(fixedPointer, widget.widgetPointer, x, y)
		return FixedWidget(widget)
	}

	fun remove(widget: Widget) {
		gtk_fixed_remove(fixedPointer, widget.widgetPointer)
	}

	fun setChildTransform(widget: Widget, transform: Transform?) {
		gtk_fixed_set_child_transform(
			fixedPointer,
			widget.widgetPointer,
			transform?.transformPointer
		)
	}

	/**
	 * Wraps a [Widget] to make it easy to manage a widget that is inside a [Fixed]
	 */
	inner class FixedWidget(val widget: Widget) {
		fun move(x: Double, y: Double) {
			move(widget, x, y)
		}

		fun remove() {
			remove(widget)
		}

		var transform: Transform?
			get() = getChildTransform(widget)
			set(value) = setChildTransform(widget, value)
	}
}