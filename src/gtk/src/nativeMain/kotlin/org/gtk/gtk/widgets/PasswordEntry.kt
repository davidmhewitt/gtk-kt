package org.gtk.gtk.widgets

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.typeCheckInstanceCastOrThrow

/**
 * gtk-kt
 *
 * 18 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.PasswordEntry.html">
 *     GtkPasswordEntry</a>
 */
class PasswordEntry(val passwordEntryPointer: CPointer<GtkPasswordEntry>) :
	Widget(passwordEntryPointer.reinterpret()), Editable {

	constructor(widget: Widget) : this(typeCheckInstanceCastOrThrow(widget, GTK_TYPE_PASSWORD_ENTRY))


	override val editablePointer: CPointer<GtkEditable> by lazy {
		passwordEntryPointer.reinterpret()
	}

	constructor() : this(gtk_password_entry_new()!!.reinterpret())

	var extraMenu: MenuModel?
		get() = gtk_password_entry_get_extra_menu(passwordEntryPointer).wrap()
		set(value) = gtk_password_entry_set_extra_menu(
			passwordEntryPointer,
			value?.menuModelPointer
		)

	var showPeekIcon: Boolean
		get() = gtk_password_entry_get_show_peek_icon(
			passwordEntryPointer
		).bool
		set(value) = gtk_password_entry_set_show_peek_icon(
			passwordEntryPointer,
			value.gtk
		)
}