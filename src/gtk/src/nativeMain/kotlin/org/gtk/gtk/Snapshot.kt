package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.*
import org.gtk.cairo.Cairo
import org.gtk.cairo.Cairo.Companion.wrap
import org.gtk.gdk.Paintable
import org.gtk.gdk.Paintable.Companion.wrap
import org.gtk.gdk.RGBA
import org.gtk.gdk.Texture
import org.gtk.glib.KGBytes
import org.gtk.graphene.*
import org.gtk.gsk.*
import org.gtk.gsk.RenderNode.Companion.wrap
import org.gtk.pango.Direction
import org.gtk.pango.Layout
import pango.PangoDirection

/**
 * kotlinx-gtk
 *
 * 06 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Snapshot.html">GtkSnapshot</a>
 */
class Snapshot(val snapshotPointer: CPointer<GtkSnapshot>) :
	org.gtk.gdk.Snapshot(snapshotPointer.reinterpret()) {

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/ctor.Snapshot.new.html">
	 *     gtk_snapshot_new</a>
	 */
	constructor() : this(gtk_snapshot_new()!!)

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_border.html">
	 *     gtk_snapshot_append_border</a>
	 */
	fun appendBorder(
		outline: RoundedRect,
		topWidth: Float,
		rightWidth: Float,
		bottomWidth: Float,
		leftWidth: Float,
		topColor: RGBA,
		rightColor: RGBA,
		bottomColor: RGBA,
		leftColor: RGBA
	) {
		memScoped {
			gtk_snapshot_append_border(
				snapshotPointer,
				outline.pointer,
				allocArrayOf(topWidth, rightWidth, bottomWidth, leftWidth),
				allocArrayOf(
					topColor.rgbaPointer,
					rightColor.rgbaPointer,
					bottomColor.rgbaPointer,
					leftColor.rgbaPointer
				).pointed.value
			)
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_cairo.html">
	 *     gtk_snapshot_append_cairo</a>
	 */
	fun appendCairo(rect: Rect): Cairo =
		gtk_snapshot_append_cairo(snapshotPointer, rect.rectPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_color.html">
	 *     gtk_snapshot_append_color</a>
	 */
	fun appendColor(color: RGBA, bounds: Rect) {
		gtk_snapshot_append_color(
			snapshotPointer,
			color.rgbaPointer,
			bounds.rectPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_conic_gradient.html">
	 *     gtk_snapshot_append_conic_gradient</a>
	 */
	fun appendConicGradient(
		bounds: Rect,
		center: Point,
		rotation: Float,
		stops: Array<ColorStop>
	) {
		memScoped {
			gtk_snapshot_append_conic_gradient(
				snapshotPointer,
				bounds.rectPointer,
				center.pointPointer,
				rotation,
				allocArrayOf(stops),
				stops.size.toULong()
			)
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_inset_shadow.html">
	 *     gtk_snapshot_append_inset_shadow</a>
	 */
	fun appendInsetShadow(
		outline: RoundedRect,
		color: RGBA,
		dx: Float,
		dy: Float,
		spread: Float,
		blurRadius: Float
	) {
		gtk_snapshot_append_inset_shadow(
			snapshotPointer,
			outline.pointer,
			color.rgbaPointer,
			dx,
			dy,
			spread,
			blurRadius
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_layout.html">
	 *     gtk_snapshot_append_layout</a>
	 */
	fun appendLayout(layout: Layout, color: RGBA) {
		gtk_snapshot_append_layout(
			snapshotPointer,
			layout.pointer,
			color.rgbaPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_linear_gradient.html">
	 *     gtk_snapshot_append_linear_gradient</a>
	 */
	fun appendLinearGradient(
		bounds: Rect,
		startPoint: Point,
		endPoint: Point,
		stops: Array<ColorStop>
	) {
		memScoped {
			gtk_snapshot_append_linear_gradient(
				snapshotPointer,
				bounds.rectPointer,
				startPoint.pointPointer,
				endPoint.pointPointer,
				allocArrayOf(stops),
				stops.size.toULong()
			)
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_node.html">
	 *     gtk_snapshot_append_node</a>
	 */
	fun appendNode(node: RenderNode) {
		gtk_snapshot_append_node(snapshotPointer, node.renderNodePointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_outset_shadow.html">
	 *     gtk_snapshot_append_outset_shadow</a>
	 */
	fun appendOutsetShadow(
		outline: RoundedRect,
		color: RGBA,
		dx: Float,
		dy: Float,
		spread: Float,
		blurRadius: Float
	) {
		gtk_snapshot_append_outset_shadow(
			snapshotPointer,
			outline.pointer,
			color.rgbaPointer,
			dx,
			dy,
			spread,
			blurRadius
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_radial_gradient.html">
	 *     gtk_snapshot_append_radial_gradient</a>
	 */
	fun appendRadialGradient(
		bounds: Rect,
		center: Point,
		horizontalRadius: Float,
		verticalRadius: Float,
		start: Float,
		end: Float,
		stops: Array<ColorStop>
	) {
		memScoped {
			gtk_snapshot_append_radial_gradient(
				snapshotPointer,
				bounds.rectPointer,
				center.pointPointer,
				horizontalRadius,
				verticalRadius,
				start,
				end,
				allocArrayOf(stops),
				stops.size.toULong()
			)
		}
	}

	inline fun NativePlacement.allocArrayOf(stops: Array<ColorStop>) =
		allocArrayOf(stops.map { it.colorStopPointer }).pointed.value

	/**
	 * @see
	 * <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_repeating_linear_gradient.html">
	 *     gtk_snapshot_append_repeating_linear_gradient</a>
	 */
	fun appendRepeatingLinearGradient(
		bounds: Rect,
		startPoint: Point,
		endPoint: Point,
		stops: Array<ColorStop>
	) {
		memScoped {
			gtk_snapshot_append_repeating_linear_gradient(
				snapshotPointer,
				bounds.rectPointer,
				startPoint.pointPointer,
				endPoint.pointPointer,
				allocArrayOf(stops),
				stops.size.toULong()
			)
		}
	}

	/**
	 * @see
	 * <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_repeating_radial_gradient.html">
	 *     gtk_snapshot_append_repeating_radial_gradient</a>
	 */
	fun appendRepeatingRadialGradient(
		bounds: Rect,
		center: Point,
		horizontalRadius: Float,
		verticalRadius: Float,
		start: Float,
		end: Float,
		stops: Array<ColorStop>
	) {
		memScoped {
			gtk_snapshot_append_repeating_radial_gradient(
				snapshotPointer,
				bounds.rectPointer,
				center.pointPointer,
				horizontalRadius,
				verticalRadius,
				start,
				end,
				allocArrayOf(stops),
				stops.size.toULong()
			)
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.append_texture.html">
	 *     gtk_snapshot_append_texture</a>
	 */
	fun appendTexture(
		texture: Texture,
		bounds: Rect
	) {
		gtk_snapshot_append_texture(
			snapshotPointer,
			texture.texturePointer,
			bounds.rectPointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.free_to_node.html">
	 *     gtk_snapshot_free_to_node</a>
	 */
	fun freeToNode(): RenderNode =
		gtk_snapshot_free_to_node(snapshotPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.free_to_paintable.html">
	 *     gtk_snapshot_free_to_paintable</a>
	 */
	fun freeToPaintable(size: Size?): Paintable =
		gtk_snapshot_free_to_paintable(
			snapshotPointer,
			size?.sizePointer
		)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.gl_shader_pop_texture.html">
	 *     gtk_snapshot_gl_shader_pop_texture</a>
	 */
	fun glShaderPopTexture() {
		gtk_snapshot_gl_shader_pop_texture(snapshotPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.perspective.html">
	 *     gtk_snapshot_perspective</a>
	 */
	fun perspective(depth: Float) {
		gtk_snapshot_perspective(snapshotPointer, depth)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.pop.html">
	 *     gtk_snapshot_pop</a>
	 */
	fun pop() {
		gtk_snapshot_pop(snapshotPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_blend.html">
	 *     gtk_snapshot_push_blend</a>
	 */
	fun pushBlend(
		blendMode: BlendMode
	) {
		gtk_snapshot_push_blend(snapshotPointer, blendMode.gsk)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_blur.html">
	 *     gtk_snapshot_push_blur</a>
	 */
	fun pushBlur(radius: Double) {
		gtk_snapshot_push_blur(snapshotPointer, radius)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_clip.html">
	 *     gtk_snapshot_push_clip</a>
	 */
	fun pushClip(bounds: Rect) {
		gtk_snapshot_push_clip(snapshotPointer, bounds.rectPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_color_matrix.html">
	 *     gtk_snapshot_push_color_matrix</a>
	 */
	fun pushColorMatrix(
		colorMatrix: Matrix,
		colorOffset: Vec4
	) {
		gtk_snapshot_push_color_matrix(
			snapshotPointer,
			colorMatrix.matrixPointer,
			colorOffset.vec4Pointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_cross_fade.html">
	 *     gtk_snapshot_push_cross_fade</a>
	 */
	fun pushCrossFade(progress: Double) {
		gtk_snapshot_push_cross_fade(
			snapshotPointer,
			progress
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_debug.html">
	 *     gtk_snapshot_push_debug</a>
	 */
	fun pushDebug(message: String) {
		gtk_snapshot_push_debug(snapshotPointer, message)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_gl_shader.html">
	 *     gtk_snapshot_push_gl_shader</a>
	 */
	fun pushGlShader(
		shader: GLShader,
		bounds: Rect,
		takeArgs: KGBytes
	) {
		gtk_snapshot_push_gl_shader(
			snapshotPointer,
			shader.glShaderPointer,
			bounds.rectPointer,
			takeArgs.pointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_opacity.html">
	 *     gtk_snapshot_push_opacity</a>
	 */
	fun pushOpacity(opacity: Double) {
		gtk_snapshot_push_opacity(snapshotPointer, opacity)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_repeat.html">
	 *     gtk_snapshot_push_repeat</a>
	 */
	fun pushRepeat(
		bounds: Rect,
		childBounds: Rect
	) {
		gtk_snapshot_push_repeat(
			snapshotPointer,
			bounds.rectPointer,
			childBounds.rectPointer
		)

	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_rounded_clip.html">
	 *     gtk_snapshot_push_rounded_clip</a>
	 */
	fun pushRoundedClip(
		bounds: RoundedRect
	) {
		gtk_snapshot_push_rounded_clip(
			snapshotPointer,
			bounds.pointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.push_shadow.html">
	 *     gtk_snapshot_push_shadow</a>
	 */
	fun pushShadow(
		shadows: Array<Shadow>
	) {
		memScoped {
			gtk_snapshot_push_shadow(
				snapshotPointer,
				allocArrayOf(shadows.map { it.shadowPointer }).pointed.value,
				shadows.size.toULong()
			)
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.render_background.html">
	 *     gtk_snapshot_render_background</a>
	 */
	fun renderBackground(
		context: StyleContext,
		x: Double,
		y: Double,
		width: Double,
		height: Double
	) {
		gtk_snapshot_render_background(
			snapshotPointer,
			context.styleContextPointer,
			x,
			y,
			width,
			height
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.render_focus.html">
	 *     gtk_snapshot_render_focus</a>
	 */
	fun renderFocus(
		context: StyleContext,
		x: Double,
		y: Double,
		width: Double,
		height: Double
	) {
		gtk_snapshot_render_focus(
			snapshotPointer,
			context.styleContextPointer,
			x,
			y,
			width,
			height
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.render_frame.html">
	 *     gtk_snapshot_render_frame</a>
	 */
	fun renderFrame(
		context: StyleContext,
		x: Double,
		y: Double,
		width: Double,
		height: Double
	) {
		gtk_snapshot_render_frame(
			snapshotPointer,
			context.styleContextPointer,
			x,
			y,
			width,
			height
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.render_insertion_cursor.html">
	 *     gtk_snapshot_render_insertion_cursor</a>
	 */
	fun renderInsertionCursor(
		context: StyleContext,
		x: Double,
		y: Double,
		layout: Layout,
		index: Int,
		direction: Direction
	) {
		gtk_snapshot_render_insertion_cursor(
			snapshotPointer,
			context.styleContextPointer,
			x,
			y,
			layout.pointer,
			index,
			direction.pango
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.render_layout.html">
	 *     gtk_snapshot_render_layout</a>
	 */
	fun renderLayout(
		context: StyleContext,
		x: Double,
		y: Double,
		layout: Layout,
	) {
		gtk_snapshot_render_layout(
			snapshotPointer,
			context.styleContextPointer,
			x,
			y,
			layout.pointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.restore.html">
	 *     gtk_snapshot_restore</a>
	 */
	fun restore() {
		gtk_snapshot_restore(snapshotPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.rotate.html">
	 *     gtk_snapshot_rotate</a>
	 */
	fun rotate(angle: Float) {
		gtk_snapshot_rotate(snapshotPointer, angle)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.rotate_3d.html">
	 *     gtk_snapshot_rotate_3d</a>
	 */
	fun rotate3D(
		angle: Float,
		axis: Vec3
	) {
		gtk_snapshot_rotate_3d(
			snapshotPointer,
			angle,
			axis.vec3Pointer
		)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.save.html">
	 *     gtk_snapshot_save</a>
	 */
	fun save() {
		gtk_snapshot_save(snapshotPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.scale.html">
	 *     gtk_snapshot_scale</a>
	 */
	fun scale(factorX: Float, factorY: Float) {
		gtk_snapshot_scale(snapshotPointer, factorX, factorY)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.scale_3d.html">
	 *     gtk_snapshot_scale_3d</a>
	 */
	fun scale3D(factorX: Float, factorY: Float, factorZ: Float) {
		gtk_snapshot_scale_3d(snapshotPointer, factorX, factorY, factorZ)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.to_node.html">
	 *     gtk_snapshot_to_node</a>
	 */
	fun toNode(): RenderNode =
		gtk_snapshot_to_node(snapshotPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.to_paintable.html">
	 *     gtk_snapshot_to_paintable</a>
	 */
	fun toPaintable(size: Size?): Paintable =
		gtk_snapshot_to_paintable(snapshotPointer, size?.sizePointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.transform.html">
	 *     gtk_snapshot_transform</a>
	 */
	fun transform(transform: Transform) {
		gtk_snapshot_transform(snapshotPointer, transform.transformPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.transform_matrix.html">
	 *     gtk_snapshot_transform_matrix</a>
	 */
	fun transformMatrix(
		matrix: Matrix
	) {
		gtk_snapshot_transform_matrix(snapshotPointer, matrix.matrixPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.translate.html">
	 *     gtk_snapshot_translate</a>
	 */
	fun translate(point: Point) {
		gtk_snapshot_translate(snapshotPointer, point.pointPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/gtk4/method.Snapshot.translate_3d.html">
	 *     gtk_snapshot_translate_3d</a>
	 */
	fun translate3D(point: Point3D) {
		gtk_snapshot_translate_3d(snapshotPointer, point.point3D)
	}
}