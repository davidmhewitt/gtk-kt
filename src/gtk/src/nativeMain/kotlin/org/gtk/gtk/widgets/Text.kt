package org.gtk.gtk.widgets

import glib.gboolean
import glib.gint
import glib.gpointer
import gobject.GCallback
import gtk.*
import kotlinx.cinterop.*
import org.gtk.gio.MenuModel
import org.gtk.gio.MenuModel.Companion.wrap
import org.gtk.glib.CStringPointer
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gobject.Signals
import org.gtk.gobject.addSignalCallback
import org.gtk.graphene.Rect
import org.gtk.graphene.Rect.Companion.wrap
import org.gtk.gtk.common.callback.TypedNoArgFunc
import org.gtk.gtk.common.enums.DeleteType
import org.gtk.gtk.common.enums.InputPurpose
import org.gtk.gtk.common.enums.MovementStep
import org.gtk.gtk.entrybuffer.EntryBuffer
import org.gtk.gtk.entrybuffer.EntryBuffer.Companion.wrap
import org.gtk.pango.AttrList
import org.gtk.pango.AttrList.Companion.wrap
import org.gtk.pango.TabArray
import org.gtk.pango.TabArray.Companion.wrap

/**
 * 27 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Text.html">
 *     GtkText</a>
 */
class Text(
	val textPointer: CPointer<GtkText>,
) : Widget(textPointer.reinterpret()), Editable {
	override val editablePointer: CPointer<GtkEditable> by lazy {
		textPointer.reinterpret()
	}

	constructor() : this(gtk_text_new()!!.reinterpret())

	constructor(buffer: EntryBuffer) :
			this(
				gtk_text_new_with_buffer(
					buffer.entryBufferPointer
				)!!.reinterpret()
			)

	data class CursorExtents(
		val strong: Rect,
		val weak: Rect,
	)

	fun computeCursorExtents(position: ULong): CursorExtents = memScoped {
		val strong = cValue<graphene_rect_t>()
		val weak = cValue<graphene_rect_t>()

		gtk_text_compute_cursor_extents(
			textPointer,
			position,
			strong,
			weak
		)

		CursorExtents(
			strong.ptr.wrap(),
			weak.ptr.wrap()
		)
	}

	var activatesDefault: Boolean
		get() = gtk_text_get_activates_default(textPointer).bool
		set(value) = gtk_text_set_activates_default(textPointer, value.gtk)

	var attributes: AttrList?
		get() = gtk_text_get_attributes(textPointer).wrap()
		set(value) = gtk_text_set_attributes(
			textPointer,
			value?.attrListPointer
		)

	var buffer: EntryBuffer
		get() = gtk_text_get_buffer(textPointer)!!.wrap()
		set(value) = gtk_text_set_buffer(textPointer, value.entryBufferPointer)

	var enableEmojiCompletion: Boolean
		get() = gtk_text_get_enable_emoji_completion(textPointer).bool
		set(value) = gtk_text_set_enable_emoji_completion(
			textPointer,
			value.gtk
		)

	var extraMenu: MenuModel?
		get() = gtk_text_get_extra_menu(textPointer).wrap()
		set(value) = gtk_text_set_extra_menu(
			textPointer,
			value?.menuModelPointer
		)

	var inputHints: GtkInputHints
		get() = gtk_text_get_input_hints(textPointer)
		set(value) = gtk_text_set_input_hints(textPointer, value)

	var inputPurpose: InputPurpose
		get() = InputPurpose.valueOf(gtk_text_get_input_purpose(textPointer))
		set(value) = gtk_text_set_input_purpose(textPointer, value.gtk)

	var invisibleChar: UInt
		get() = gtk_text_get_invisible_char(textPointer)
		set(value) = gtk_text_set_invisible_char(textPointer, value)

	var maxLength: Int
		get() = gtk_text_get_max_length(textPointer)
		set(value) = gtk_text_set_max_length(textPointer, value)

	var overwriteMode: Boolean
		get() = gtk_text_get_overwrite_mode(textPointer).bool
		set(value) = gtk_text_set_overwrite_mode(textPointer, value.gtk)

	var placeholderText: String?
		get() = gtk_text_get_placeholder_text(textPointer)?.toKString()
		set(value) = gtk_text_set_placeholder_text(textPointer, value)

	var propagateWidth: Boolean
		get() = gtk_text_get_propagate_text_width(textPointer).bool
		set(value) = gtk_text_set_propagate_text_width(textPointer, value.gtk)

	var tabs: TabArray?
		get() = gtk_text_get_tabs(textPointer).wrap()
		set(value) = gtk_text_set_tabs(textPointer, value?.tabArrayPointer)

	val textLength: UShort
		get() = gtk_text_get_text_length(textPointer)

	var truncateMultiline: Boolean
		get() = gtk_text_get_truncate_multiline(textPointer).bool
		set(value) = gtk_text_set_truncate_multiline(textPointer, value.gtk)

	var isTextVisible: Boolean
		get() = gtk_text_get_visibility(textPointer).bool
		set(value) = gtk_text_set_visibility(textPointer, value.gtk)

	fun grabFocusWithoutSelecting(): Boolean =
		gtk_text_grab_focus_without_selecting(textPointer).bool

	fun unsetInvisibleChar() {
		gtk_text_unset_invisible_char(textPointer)
	}

	fun addOnActivateCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.ACTIVATE, action, staticNoArgFunc)

	fun addOnBackspaceCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.BACKSPACE, action, staticNoArgFunc)

	fun addOnCopyClipboardCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.COPY_CLIPBOARD, action, staticNoArgFunc)

	fun addOnCutClipboardCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.CUT_CLIPBOARD, action, staticNoArgFunc)

	fun addOnDeleteFromCursorCallback(action: TextDeleteFromCursorFunc) =
		addSignalCallback(
			Signals.DELETE_FROM_CURSOR,
			action,
			staticDeleteFromCursorFunc
		)

	fun addOnInsertAtCursorCallback(action: TextInsertAtCursorFunc) =
		addSignalCallback(Signals.INSERT_AT_CURSOR, action, staticStringFunc)

	fun addOnInsertEmojiCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.INSERT_EMOJI, action, staticNoArgFunc)

	fun addOnMoveCursorCallback(action: TextMoveCursorFunc) =
		addSignalCallback(Signals.MOVE_CURSOR, action, staticMoveCursorFunc)

	fun addOnPasteClipboardCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.PASTE_CLIPBOARD, action, staticNoArgFunc)

	fun addOnPreEditChangedCallback(action: TextPreEditChangedCallback) =
		addSignalCallback(Signals.PREEDIT_CHANGED, action, staticStringFunc)


	fun addOnToggleOverwriteCallback(action: TypedNoArgFunc<Text>) =
		addSignalCallback(Signals.TOGGLE_OVERWRITE, action, staticNoArgFunc)

	companion object {
		inline fun CPointer<GtkText>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkText>.wrap() =
			Text(this)

		private val staticNoArgFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkText>,
					data: gpointer,
				->
				data.asStableRef<TypedNoArgFunc<Text>>()
					.get()
					.invoke(self.wrap())
			}.reinterpret()

		private val staticDeleteFromCursorFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkText>,
					type: GtkDeleteType,
					count: Int,
					data: gpointer,
				->
				data.asStableRef<TextDeleteFromCursorFunc>()
					.get()
					.invoke(
						self.wrap(),
						DeleteType.valueOf(type),
						count
					)
			}.reinterpret()

		private val staticStringFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkText>,
					string: CStringPointer,
					data: gpointer,
				->
				data.asStableRef<Text.(String) -> Unit>()
					.get()
					.invoke(self.wrap(), string.toKString())
			}.reinterpret()

		private val staticMoveCursorFunc: GCallback =
			staticCFunction {
					self: CPointer<GtkText>,
					step: GtkMovementStep,
					count: gint,
					extend: gboolean,
					data: gpointer,
				->
				data.asStableRef<TextMoveCursorFunc>()
					.get()
					.invoke(
						self.wrap(),
						MovementStep.valueOf(step),
						count,
						extend.bool
					)
			}.reinterpret()
	}
}

typealias TextDeleteFromCursorFunc =
		Text.(
			type: DeleteType,
			count: Int,
		) -> Unit

typealias TextInsertAtCursorFunc =
		Text.(
			string: String,
		) -> Unit

typealias TextMoveCursorFunc =
		Text.(
			step: MovementStep,
			count: Int,
			extend: Boolean,
		) -> Unit

typealias TextPreEditChangedCallback =
		Text.(
			preEdit: String,
		) -> Unit