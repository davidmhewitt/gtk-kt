package org.gtk.gtk

import gtk.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import org.gtk.gio.File
import org.gtk.gio.File.Companion.wrap
import org.gtk.glib.bool
import org.gtk.glib.gtk
import org.gtk.gtk.MediaStream.Companion.wrap
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 15 / 11 / 2021
 *
 * @see <a href="https://docs.gtk.org/gtk4/class.Video.html">GtkVideo</a>
 */
class Video(val videoPointer: CPointer<GtkVideo>) :
	Widget(videoPointer.reinterpret()) {

	constructor() :
			this(gtk_video_new()!!.reinterpret())

	constructor(file: File) :
			this(gtk_video_new_for_file(file.filePointer)!!.reinterpret())

	constructor(fileName: String, isResource: Boolean = false) :
			this(
				if (isResource) {
					gtk_video_new_for_resource(fileName)
				} else {
					gtk_video_new_for_filename(fileName)
				}!!.reinterpret()
			)

	constructor(stream: MediaStream) :
			this(
				gtk_video_new_for_media_stream(stream.mediaStreamPointer)
				!!.reinterpret()
			)

	var autoPlay: Boolean
		get() = gtk_video_get_autoplay(videoPointer).bool
		set(value) = gtk_video_set_autoplay(videoPointer, value.gtk)

	var file: File?
		get() = gtk_video_get_file(videoPointer).wrap()
		set(value) = gtk_video_set_file(videoPointer, value?.filePointer)

	var loop: Boolean
		get() = gtk_video_get_loop(videoPointer).bool
		set(value) = gtk_video_set_loop(videoPointer, value.gtk)

	var mediaStream: MediaStream?
		get() = gtk_video_get_media_stream(videoPointer).wrap()
		set(value) = gtk_video_set_media_stream(
			videoPointer,
			value?.mediaStreamPointer
		)

	fun setResource(resourcePath: String) {
		gtk_video_set_resource(videoPointer, resourcePath)
	}

	companion object {
		inline fun CPointer<GtkVideo>?.wrap() =
			this?.wrap()

		inline fun CPointer<GtkVideo>.wrap() =
			Video(this)
	}
}