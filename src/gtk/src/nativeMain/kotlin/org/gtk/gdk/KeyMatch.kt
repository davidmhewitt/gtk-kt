package org.gtk.gdk

import gtk.GdkKeyMatch
import gtk.GdkKeyMatch.*

/**
 * 26 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/enum.KeyMatch.html">
 *     GdkKeyMatch</a>
 */
enum class KeyMatch(
	val gdk: GdkKeyMatch,
) {
	NONE(GDK_KEY_MATCH_NONE),
	PARTIAL(GDK_KEY_MATCH_PARTIAL),
	EXACT(GDK_KEY_MATCH_EXACT);

	companion object {
		fun valueOf(gdk: GdkKeyMatch): KeyMatch =
			when (gdk) {
				GDK_KEY_MATCH_NONE -> NONE
				GDK_KEY_MATCH_PARTIAL -> PARTIAL
				GDK_KEY_MATCH_EXACT -> EXACT
			}
	}
}