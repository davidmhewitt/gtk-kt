package org.gtk.gdk

import gtk.GdkPaintable
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 06 / 08 / 2021
 *
 * @see <a href=""></a>
 */
interface Paintable {
	val paintablePointer: CPointer<GdkPaintable>

	companion object {
		inline fun CPointer<GdkPaintable>?.wrap() =
			this?.wrap()

		fun CPointer<GdkPaintable>.wrap(): Paintable =
			ImplPaintable(this)

		internal class ImplPaintable(override val paintablePointer: CPointer<GdkPaintable>) : Paintable
	}
}