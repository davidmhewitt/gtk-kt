package org.gtk.gdk

import gtk.GdkDrag_autoptr
import kotlinx.cinterop.reinterpret
import org.gtk.gobject.KGObject

/**
 * 20 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.Drag.html">
 *     GdkDrag</a>
 */
class Drag(
	val dragPointer: GdkDrag_autoptr,
) : KGObject(dragPointer.reinterpret()) {
}