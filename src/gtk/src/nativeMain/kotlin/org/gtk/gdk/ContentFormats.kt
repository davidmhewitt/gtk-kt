package org.gtk.gdk

import gtk.GdkContentFormats_autoptr

/**
 * 26 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/struct.ContentFormats.html">
 *     GdkContentFormats</a>
 */
class ContentFormats(
	val contentFormatsPointer:GdkContentFormats_autoptr
) {
}