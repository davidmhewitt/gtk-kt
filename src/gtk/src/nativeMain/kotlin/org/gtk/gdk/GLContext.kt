package org.gtk.gdk

import gtk.GdkGLContext_autoptr
import kotlinx.cinterop.reinterpret

/**
 * 24 / 12 / 2021
 *
 * @see <a href="https://docs.gtk.org/gdk4/class.GLContext.html">
 *     GdkGLContext</a>
 */
class GLContext(
	val glContextPointer: GdkGLContext_autoptr,
) :DrawContext(glContextPointer.reinterpret()){

	companion object{
		inline fun GdkGLContext_autoptr?.wrap() =
			this?.wrap()

		inline fun GdkGLContext_autoptr.wrap() =
			GLContext(this)
	}
}