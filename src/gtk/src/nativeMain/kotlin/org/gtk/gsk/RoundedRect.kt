package org.gtk.gsk

import gtk.*
import kotlinx.cinterop.*
import org.gtk.glib.bool
import org.gtk.graphene.Point
import org.gtk.graphene.Rect
import org.gtk.graphene.Size
import org.gtk.gtk.widgets.Widget

/**
 * kotlinx-gtk
 *
 * 09 / 08 / 2021
 *
 * @see <a href="https://docs.gtk.org/gsk4/struct.RoundedRect.html">GskRoundedRect</a>
 */
class RoundedRect(val pointer: CPointer<GskRoundedRect>) {

	//var corner: None

	fun containsPoint(point: Point): Boolean =
		gsk_rounded_rect_contains_point(pointer, point.pointPointer).bool

	fun containsRect(rect: Rect): Boolean =
		gsk_rounded_rect_contains_rect(pointer, rect.rectPointer).bool

	constructor(
		bounds: Rect,
		topLeft: Size,
		topRight: Size,
		bottomLeft: Size,
		bottomRight: Size
	) : this(memScoped<CPointer<GskRoundedRect>> {
		gsk_rounded_rect_init(
			self = alloc<GskRoundedRect>().ptr,
			bounds = bounds.rectPointer,
			top_left = topLeft.sizePointer,
			top_right = topRight.sizePointer,
			bottom_left = bottomLeft.sizePointer,
			bottom_right = bottomRight.sizePointer,
		)!!
	})

	constructor(src: RoundedRect) : this(memScoped {
		gsk_rounded_rect_init_copy(alloc<GskRoundedRect>().ptr, src.pointer)!!
	})

	constructor(bounds: Rect, radius: Float) : this(memScoped {
		gsk_rounded_rect_init_from_rect(alloc<GskRoundedRect>().ptr, bounds.rectPointer, radius)!!
	})

	fun intersectsRect(rect: Rect): Boolean =
		gsk_rounded_rect_intersects_rect(pointer, rect.rectPointer).bool

	val isRectilinear: Boolean
		get() = gsk_rounded_rect_is_rectilinear(pointer).bool

	fun normalize(): RoundedRect =
		gsk_rounded_rect_normalize(pointer)!!.wrap()

	fun offset(dx: Float, dy: Float): RoundedRect =
		gsk_rounded_rect_offset(pointer, dx, dy)!!.wrap()

	fun shrink(
		top: Float,
		bottom: Float,
		right: Float,
		left: Float
	): RoundedRect =
		gsk_rounded_rect_shrink(pointer, top, right, bottom, left)!!.wrap()

	companion object {
		inline fun CPointer<GskRoundedRect>?.wrap() =
			this?.wrap()

		inline fun CPointer<GskRoundedRect>.wrap() =
			RoundedRect(this)

	}
}