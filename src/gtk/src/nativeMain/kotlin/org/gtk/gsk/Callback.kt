package org.gtk.gsk

import gtk.GskParseErrorFunc
import kotlinx.cinterop.asStableRef
import kotlinx.cinterop.staticCFunction
import org.gtk.glib.KGError
import org.gtk.glib.KGError.Companion.wrap
import org.gtk.gsk.ParseLocation.Companion.wrap

/*
 * kotlinx-gtk
 *
 * 13 / 10 / 2021
 *
 */


typealias ParseErrorFunction = (
	start: ParseLocation,
	end: ParseLocation,
	error: KGError
) -> Unit

internal val staticParseErrorFunction: GskParseErrorFunc =
	staticCFunction { start, end, error, data ->
		data?.asStableRef<ParseErrorFunction>()?.get()?.invoke(
			start!!.wrap(),
			end!!.wrap(),
			error!!.wrap()
		)
		Unit
	}