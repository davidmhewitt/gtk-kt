package org.gtk.gsk

import gtk.GskShadow
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 27 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class Shadow(val shadowPointer: CPointer<GskShadow>) {
}