package org.gtk.graphene

import gtk.graphene_vec3_t
import kotlinx.cinterop.CPointer

/**
 * kotlinx-gtk
 *
 * 27 / 10 / 2021
 *
 * @see <a href=""></a>
 */
class Vec3(val vec3Pointer: CPointer<graphene_vec3_t>) {
}