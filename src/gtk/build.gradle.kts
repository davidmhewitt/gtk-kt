import java.io.FileInputStream
import java.util.*

plugins {
	kotlin("multiplatform")
	`maven-publish`
	`dokka-script`
}

version = "4.5.0-alpha0"

kotlin {
	linuxX64("native") {
		val main by compilations.getting
		val gtk by main.cinterops.creating
		//val gtkunixprint by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:gio"))
				implementation(project(":src:cairo"))
				implementation(project(":src:pango"))
				implementation(project(":src:gobject"))
				implementation(project(":src:glib"))
				implementation(project(":src:gdk-pixbuf"))
			}
		}
	}



	publishing {

		publications {
			repositories {
				maven {

					val releasesRepoUrl =
						uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2")
					val snapshotsRepoUrl =
						uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")

					url =
						if (version.toString().endsWith("SNAPSHOT")) {
							snapshotsRepoUrl
						} else {
							releasesRepoUrl
						}

					val mavenPropertiesFile = rootProject.file("./maven.properties")
					val mavenProperties = Properties()

					if (mavenPropertiesFile.exists()) {
						mavenProperties.load(FileInputStream(mavenPropertiesFile))
					}else{
						println("No properties file")
					}

					val SONATYPE_USERNAME: String =
						mavenProperties["SONATYPE_USERNAME"]?.toString() ?: ""

					val SONATYPE_PASSWORD: String =
						mavenProperties["SONATYPE_PASSWORD"]?.toString() ?: ""

					credentials {
						username = SONATYPE_USERNAME
						password = SONATYPE_PASSWORD
					}
				}
			}
		}
	}
}
