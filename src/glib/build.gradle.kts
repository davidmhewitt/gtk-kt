plugins {
	kotlin("multiplatform")
	`maven-publish`
	`dokka-script`
}

version = "2.68.0-alpha0"

kotlin {
	linuxX64("native") {
		val main by compilations.getting
		val glib by main.cinterops.creating

		binaries {
			sharedLib()
		}
	}
}
