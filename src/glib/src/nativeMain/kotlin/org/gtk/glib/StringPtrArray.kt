package org.gtk.glib

import glib.GPtrArray_autoptr
import glib.g_ptr_array_new
import kotlinx.cinterop.cstr
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.pointed

/**
 * 18 / 12 / 2021
 */
class StringPtrArray(
	ptrArrayPointer: GPtrArray_autoptr = g_ptr_array_new()!!,
) : PtrArray(ptrArrayPointer) {

	val length: UInt
		get() = ptrArrayPointer.pointed.len

	fun add(string: String) {
		memScoped {
			super.add(string.cstr.ptr)
		}
	}
}