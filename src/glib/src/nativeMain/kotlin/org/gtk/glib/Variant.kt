package org.gtk.glib

import glib.*
import kotlinx.cinterop.*
import org.gtk.glib.KGBytes.Companion.wrap
import org.gtk.glib.VariantIter.Companion.wrap

/**
 * kotlinx-gtk
 *
 * 23 / 03 / 2021
 *
 * @see <a href="https://docs.gtk.org/glib/struct.Variant.html">
 *     GVariant</a>
 */
open class Variant(
	val variantPointer: CPointer<GVariant>
) : Comparable<Variant>, UnrefMe {

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.byteswap.html">
	 *     g_variant_byteswap</a>
	 */
	fun byteswap(): Variant =
		g_variant_byteswap(variantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.check_format_string.html">
	 *     g_variant_check_format_string</a>
	 */
	fun checkFormatString(formatString: String, copyOnly: Boolean): Boolean =
		g_variant_check_format_string(variantPointer, formatString, copyOnly.gtk).bool

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.classify.html">
	 *     g_variant_classify</a>
	 */
	val classified: VariantClass by lazy {
		VariantClass.valueOf(g_variant_classify(variantPointer))!!
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.compare.html">
	 *     g_variant_compare</a>
	 */
	override fun compareTo(other: Variant): Int =
		g_variant_compare(variantPointer, other.variantPointer)

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.dup_bytestring.html">
	 *     g_variant_dup_bytestring</a>
	 */
	fun dupBytestring(): String =
		g_variant_dup_bytestring(variantPointer, null)!!.use {
			it.toKString()
		}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.dup_bytestring_array.html">
	 *     g_variant_dup_bytestring_array</a>
	 */
	fun dupBytestringArray(): Array<String> = memScoped {
		val length = cValue<gsizeVar>()
		val cArray = g_variant_dup_bytestring_array(variantPointer, length)!!

		Array(length.ptr.pointed.value.toInt()) { index ->
			cArray[index]!!.toKString()
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.dup_objv.html">
	 *     g_variant_dup_objv</a>
	 */
	fun dupObjv(): Array<String> = memScoped {
		val length = cValue<gsizeVar>()
		val cArray = g_variant_dup_objv(variantPointer, length) ?: return@memScoped arrayOf()

		Array(length.ptr.pointed.value.toInt()) { index ->
			cArray[index]!!.toKString()
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.dup_string.html">
	 *     g_variant_dup_string</a>
	 */
	fun dupString(): String =
		g_variant_dup_string(variantPointer, null)!!.toKString()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.dup_strv.html">
	 *     g_variant_dup_strv</a>
	 */
	fun dupStrv(): Array<String> = memScoped {
		val length = cValue<gsizeVar>()
		val cArray = g_variant_dup_strv(variantPointer, length)!!

		Array(length.ptr.pointed.value.toInt()) { index ->
			cArray[index]!!.toKString()
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.equal.html">
	 *     g_variant_is_of_type</a>
	 */
	override fun equals(other: Any?): Boolean {
		if (other is VariantType) {
			if (other == this) {
				// return true if kotlin object is the same
				return true
			} else {
				g_variant_is_of_type(
					variantPointer,
					other.variantTypePointer
				).bool
			}
		}
		return false
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_boolean.html">
	 *     g_variant_get_boolean</a>
	 */
	val boolean: Boolean by lazy {
		checkClass(VariantClass.BOOLEAN)
		g_variant_get_boolean(variantPointer).bool
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_byte.html">
	 *     g_variant_get_byte</a>
	 */
	val byte: UByte by lazy {
		checkClass(VariantClass.BYTE)
		g_variant_get_byte(variantPointer)
	}

	/*
	TODO Figure out confusing documentation
	https://docs.gtk.org/glib/method.Variant.get_bytestring.html
	val byteString: Array<UByte> by lazy {
		g_variant_get_bytestring(variantPointer)!!.use {}
	}
	 */

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_bytestring_array.html">
	 *     g_variant_get_bytestring_array</a>
	 */
	val byteStringArray: Array<String> by lazy {
		memScoped {
			val length = cValue<gsizeVar>()
			g_variant_get_bytestring_array(variantPointer, length)!!.use { cArray ->
				Array(length.ptr.pointed.value.toInt()) { index ->
					cArray[index]!!.toKString()
				}
			}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_child_value.html">
	 *     g_variant_get_child_value</a>
	 */
	fun getChildValue(index: ULong): Variant {
		if (index >= nChildren)
			throw IndexOutOfBoundsException("It is an error if index is greater than the number of child items in the container.")

		return g_variant_get_child_value(variantPointer, index)!!.wrap()
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_data_as_bytes.html">
	 *     g_variant_get_data_as_bytes</a>
	 */
	val dataAsBytes: KGBytes
		get() = g_variant_get_data_as_bytes(variantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_double.html">
	 *     g_variant_get_double</a>
	 */
	val double: Double by lazy {
		checkClass(VariantClass.DOUBLE)
		g_variant_get_double(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_handle.html">
	 *     g_variant_get_handle</a>
	 */
	val handle: Int by lazy {
		checkClass(VariantClass.HANDLE)
		g_variant_get_handle(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_int16.html">
	 *     g_variant_get_int16</a>
	 */
	val short: Short by lazy {
		checkClass(VariantClass.SHORT)
		g_variant_get_int16(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_int32.html">
	 *     g_variant_get_int32</a>
	 */
	val int: Int by lazy {
		checkClass(VariantClass.INT)
		g_variant_get_int32(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_int64.html">
	 *     g_variant_get_int64</a>
	 */
	val long: Long by lazy {
		checkClass(VariantClass.LONG)
		g_variant_get_int64(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_maybe.html">
	 *     g_variant_get_maybe</a>
	 */
	val maybe: Variant? by lazy {
		checkClass(VariantClass.MAYBE)
		g_variant_get_maybe(variantPointer).wrap()
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_normal_form.html">
	 *     g_variant_get_normal_form</a>
	 */
	val normalForm: Variant by lazy {
		g_variant_get_normal_form(variantPointer)!!.wrap()
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_objv.html">
	 *     g_variant_get_objv</a>
	 */
	val objv: Array<String> by lazy {
		memScoped {
			val length = cValue<gsizeVar>()
			g_variant_get_objv(variantPointer, length)!!.use { cArray ->
				Array(length.ptr.pointed.value.toInt()) { index ->
					cArray[index]!!.toKString()
				}
			}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_size.html">
	 *     g_variant_get_size</a>
	 */
	val size: ULong by lazy {
		g_variant_get_size(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_string.html">
	 *     g_variant_get_string</a>
	 */
	val string: String by lazy {
		g_variant_get_string(variantPointer, null)!!.toKString()
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_strv.html">
	 *     g_variant_get_strv</a>
	 */
	val strv: Array<String> by lazy {
		memScoped {
			val length = cValue<gsizeVar>()
			g_variant_get_strv(variantPointer, length)!!.use { cArray ->
				Array(length.ptr.pointed.value.toInt()) { index ->
					cArray[index]!!.toKString()
				}
			}
		}
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_type.html">
	 *     g_variant_get_type</a>
	 */
	val type: VariantType by lazy {
		VariantType.OpenVariant(g_variant_get_type(variantPointer)!!)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_type_string.html">
	 *     g_variant_get_type_string</a>
	 */
	val typeString: String by lazy {
		g_variant_get_type_string(variantPointer)!!.toKString()
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_uint16.html">
	 *     g_variant_get_uint16</a>
	 */
	val uShort: UShort by lazy {
		checkClass(VariantClass.USHORT)
		g_variant_get_uint16(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_uint32.html">
	 *     g_variant_get_uint32</a>
	 */
	val uInt: UInt by lazy {
		checkClass(VariantClass.UINT)
		g_variant_get_uint32(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_uint64.html">
	 *     g_variant_get_uint64</a>
	 */
	val uLong: ULong by lazy {
		checkClass(VariantClass.ULONG)
		g_variant_get_uint64(variantPointer)
	}

	/* TODO get_va https://docs.gtk.org/glib/method.Variant.get_va.html
	/**
	 * @see <a href="https://developer.gnome.org/glib/stable/glib-GVariant.html#g-variant-get-va">
	 *     g_variant_get_va</a>
	 */
	fun get(format: String, vararg args: String) {
		val v: va_list = args.toList().toNullTermCStringArray()
			.reinterpret()
		g_variant_get_va(
			variantPointer,
			format,
			null,
			null, //TODO Solve va_list issue
		)
	}
	*/

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.get_variant.html">
	 *     g_variant_get_variant</a>
	 */
	val variant: Variant
		get() = g_variant_get_variant(variantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.hash.html">
	 *     g_variant_hash</a>
	 */
	val hash: UInt by lazy {
		g_variant_hash(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.is_container.html">
	 *     g_variant_is_container</a>
	 */
	val isContainer: Boolean by lazy {
		g_variant_is_container(variantPointer).bool
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.is_floating.html">
	 *     g_variant_is_floating</a>
	 */
	val isFloating: Boolean by lazy {
		g_variant_is_floating(variantPointer).bool
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.is_normal_form.html">
	 *     g_variant_is_normal_form</a>
	 */
	val isNormalForm: Boolean by lazy {
		g_variant_is_normal_form(variantPointer).bool
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.is_of_type.html">
	 *     g_variant_is_of_type</a>
	 */
	fun isOfType(type: VariantType): Boolean =
		g_variant_is_of_type(variantPointer, type.variantTypePointer).bool

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.iter_new.html">
	 *     g_variant_iter_new</a>
	 */
	fun iterNew(): VariantIter =
		g_variant_iter_new(variantPointer)!!.wrap()

	// TODO lookup https://docs.gtk.org/glib/method.Variant.lookup.html

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.lookup_value.html">
	 *     g_variant_lookup_value</a>
	 */
	fun lookupValue(key: String, expectedType: VariantType?) =
		g_variant_lookup_value(
			variantPointer,
			key,
			expectedType?.variantTypePointer
		).wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.n_children.html">
	 *     g_variant_n_children</a>
	 */
	val nChildren: ULong by lazy {
		g_variant_n_children(variantPointer)
	}

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.print.html">
	 *     g_variant_print</a>
	 */
	fun print(typeAnnotate: Boolean): String =
		g_variant_print(variantPointer, typeAnnotate.gtk)!!.use {
			it.toKString()
		}

	// TODO print_string https://docs.gtk.org/glib/method.Variant.print_string.html

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.ref.html">
	 *     g_variant_ref</a>
	 */
	fun ref(): Variant =
		g_variant_ref(variantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.ref_sink.html">
	 *     g_variant_ref_sink</a>
	 */
	fun refSink(): Variant =
		g_variant_ref_sink(variantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.take_ref.html">
	 *     g_variant_take_ref</a>
	 */
	fun takeRef(): Variant =
		g_variant_take_ref(variantPointer)!!.wrap()

	/**
	 * @see <a href="https://docs.gtk.org/glib/method.Variant.unref.html">
	 *     g_variant_unref</a>
	 */
	override fun unref() {
		g_variant_unref(variantPointer)
	}

	/**
	 * Enforces that one doesn't try to get a value that doesn't exist
	 */
	@Throws(VariantGetException::class)
	private inline fun checkClass(expected: VariantClass) {
		if (classified != expected) throw VariantGetException(expected, classified)
	}

	class VariantGetException(variantClass: VariantClass, thisIs: VariantClass) :
		NoSuchElementException("Attempted to retrieve $variantClass from a $thisIs container")

	/*
	TODO g_variant_new_va
	/**
	 * @see <a href="https://developer.gnome.org/glib/stable/glib-GVariant.html#g-variant-new-va">
	 *     g_variant_new_va</a>
	 */
	constructor(format: String, vararg args: Any) : this(
		g_variant_new_va(
			format,
			null,
			null
		)!!
	) {
	}
	 */

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_boolean.html">
	 *     g_variant_new_boolean</a>
	 */
	constructor(value: Boolean) : this(g_variant_new_boolean(value.gtk)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_byte.html">
	 *     g_variant_new_byte</a>
	 */
	constructor(value: UByte) : this(g_variant_new_byte(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_int16.html">
	 *     g_variant_new_int16</a>
	 */
	constructor(value: Short) : this(g_variant_new_int16(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_uint16.html">
	 *     g_variant_new_uint16</a>
	 */
	constructor(value: UShort) : this(g_variant_new_uint16(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_int32.html">
	 *     g_variant_new_int32</a>
	 */
	constructor(value: Int) : this(g_variant_new_int32(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_uint32.html">
	 *     g_variant_new_uint32</a>
	 */
	constructor(value: UInt) : this(g_variant_new_uint32(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_int64.html">
	 *     g_variant_new_int64</a>
	 */
	constructor(value: Long) : this(g_variant_new_int64(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_uint64.html">
	 *     g_variant_new_uint64</a>
	 */
	constructor(value: ULong) : this(g_variant_new_uint64(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_string.html">
	 *     g_variant_new_string</a>
	 */
	constructor(value: String) : this(g_variant_new_string(value)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_variant.html">
	 *     g_variant_new_variant</a>
	 */
	constructor(variant: Variant) : this(g_variant_new_variant(variant.variantPointer)!!)

	/**
	 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_strv.html">
	 *     g_variant_new_strv</a>
	 */
	constructor(strv: Array<String>) : this(g_variant_new_strv(memScoped {
		strv.toCStringArray(this)
	}, strv.size.toLong())!!)

	override fun hashCode(): Int = hash.toInt()

	/**
	 * @see <a href=""></a>
	 */
	companion object {
		/**
		 * @see <a href="https://docs.gtk.org/glib/type_func.Variant.is_object_path.html">
		 *     g_variant_is_object_path</a>
		 */
		fun isObjectPath(string: String): Boolean =
			g_variant_is_object_path(string).bool

		/**
		 * @see <a href="https://docs.gtk.org/glib/type_func.Variant.is_signature.html">
		 *     g_variant_is_signature</a>
		 */
		fun isSignature(string: String): Boolean =
			g_variant_is_signature(string).bool

		/*
		https://docs.gtk.org/glib/type_func.Variant.parse.html
		fun parse TODO g_variant_parse
		*/

		/**
		 * @see <a href="https://docs.gtk.org/glib/type_func.Variant.parse_error_print_context.html">
		 *     g_variant_parse_error_print_context</a>
		 */
		fun parseErrorPrintContext(error: KGError, sourceStr: String): String =
			g_variant_parse_error_print_context(error.pointer, sourceStr)!!.toKString()

		/**
		 * @see <a href="https://docs.gtk.org/glib/type_func.Variant.parse_error_quark.html">
		 *     g_variant_parse_error_quark</a>
		 */
		fun parseErrorQuark(): UInt =
			g_variant_parse_error_quark()

		/**
		 * @see <a href="https://docs.gtk.org/glib/type_func.Variant.parser_get_error_quark.html">
		 *     g_variant_parser_get_error_quark</a>
		 */
		val errorQuark: UInt
			get() = g_variant_parser_get_error_quark()

		/**
		 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_handle.html">
		 *     g_variant_new_handle</a>
		 */
		fun newHandle(value: Int): Variant =
			g_variant_new_handle(value)!!.wrap()

		/**
		 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_signature.html">
		 *     g_variant_new_signature</a>
		 */
		fun newSignature(value: String) =
			g_variant_new_signature(value)!!.wrap()

		/**
		 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_objv.html">
		 *     g_variant_new_object_path</a>
		 */
		fun newObjectPath(value: String) =
			g_variant_new_object_path(value)!!.wrap()

		/**
		 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_objv.html">
		 *     g_variant_new_objv</a>
		 */
		fun newObjectPaths(strv: Array<String>) =
			g_variant_new_objv(memScoped {
				strv.toCStringArray(this)
			}, strv.size.toLong())!!

		/**
		 * @see <a href="https://docs.gtk.org/glib/ctor.Variant.new_bytestring_array.html">
		 *     g_variant_new_bytestring_array</a>
		 */
		fun newByteStringArray(strv: Array<String>) =
			g_variant_new_bytestring_array(memScoped {
				strv.toCStringArray(this)
			}, strv.size.toLong())!!.wrap()


		inline fun CPointer<GVariant>?.wrap() =
			this?.let { Variant(it) }

		inline fun CPointer<GVariant>.wrap() =
			Variant(this)

		inline fun Array<Variant>.toCArray() = memScoped {
			allocArrayOf(this@toCArray.map { it.variantPointer } + null)
		}

		inline fun Array<out Variant>.toCArray() = memScoped {
			allocArrayOf(this@toCArray.map { it.variantPointer } + null)
		}
	}

}