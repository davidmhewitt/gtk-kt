package org.gtk.glib

import glib.*
import kotlinx.cinterop.toKString

/**
 * gtk-kt
 *
 * 24 / 09 / 2021
 *
 * @see <a href=""></a>
 */

fun getOSInfo(key: String) = g_get_os_info(key)?.toKString()

fun getOSInfo(key: OSInfoKey) = getOSInfo(key.keyName)

enum class OSInfoKey(val keyName: String) {
	ID(G_OS_INFO_KEY_ID),
	BUG_REPORT_URL(G_OS_INFO_KEY_BUG_REPORT_URL),
	NAME(G_OS_INFO_KEY_NAME),
	DOCUMENTATION_URL(G_OS_INFO_KEY_DOCUMENTATION_URL),
	HOME_URL(G_OS_INFO_KEY_HOME_URL),
	PRETTY_NAME(G_OS_INFO_KEY_PRETTY_NAME),
	PRIVACY_POLICY_URL(G_OS_INFO_KEY_PRIVACY_POLICY_URL),
	SUPPORT_URL(G_OS_INFO_KEY_SUPPORT_URL),
	VERSION(G_OS_INFO_KEY_VERSION),
	VERSION_CODENAME(G_OS_INFO_KEY_VERSION_CODENAME),
	VERSION_ID(G_OS_INFO_KEY_VERSION_ID);
}