package org.gtk.glib

import glib.GSList
import kotlinx.cinterop.CPointer
import org.gtk.glib.SList.Companion.wrap

/**
 * gtk-kt
 *
 * 19 / 09 / 2021
 */
class WrappedSList<T>(
	val sList: SList,
	private val wrapPointer: VoidPointer.() -> T,
	private val getPointer: T.() -> VoidPointer
) : FreeMe {
	/**
	 * @see [sList.append]
	 */
	fun append(data: T): WrappedSList<T> =
		sList.append(data.getPointer()).wrap()

	/**
	 * @see [sList.prepend]
	 */
	fun prepend(data: T): WrappedSList<T> =
		sList.prepend(data.getPointer()).wrap()

	/**
	 * @see [sList.insert]
	 */
	fun insert(position: Int, data: T): WrappedSList<T> =
		sList.insert(position, data.getPointer()).wrap()

	/**
	 * @see [sList.insertBefore]
	 */
	fun insertBefore(sibling: WrappedSList<T>, data: T): WrappedSList<T> =
		sList.insertBefore(sibling.sList, data.getPointer()).wrap()

	/**
	 * @see [sList.remove]
	 */
	fun remove(data: T) =
		sList.remove(data.getPointer()).wrap()

	/**
	 * @see [sList.removeLink]
	 */
	fun removeLink(link: WrappedSList<T>) =
		sList.removeLink(link.sList).wrap()

	/**
	 * @see [sList.deleteLink]
	 */
	fun deleteLink(link: WrappedSList<T>) =
		sList.deleteLink(link.sList).wrap()

	/**
	 * @see [sList.removeAll]
	 */
	fun removeAll(data: T) =
		sList.removeAll(data.getPointer()).wrap()

	/**
	 * @see [sList.free]
	 */
	override fun free() {
		sList.free()
	}

	/**
	 * @see [sList.free1]
	 */
	fun free1() {
		sList.free1()
	}

	/**
	 * @see [sList.length]
	 */
	val length: UInt
		get() = sList.length

	/**
	 * @see [sList.copy]
	 */
	fun copy() =
		sList.copy().wrap()

	/**
	 * @see [sList.copyDeep]
	 */
	fun copyDeep(func: (T) -> T) =
		sList.copyDeep {
			func(it.wrapPointer()).getPointer()
		}.wrap()

	/**
	 * @see [sList.reverse]
	 */
	fun reverse() =
		sList.reverse().wrap()

	/**
	 * @see [sList.insertSortedWithData]
	 */
	fun insertSorted(data: T, compare: (T?, T?) -> Int) =
		sList.insertSortedWithData(data.getPointer()) { a, b ->
			compare(a?.let(wrapPointer), b?.let(wrapPointer))
		}.wrap()

	/**
	 * @see [sList.sortWithData]
	 */
	fun sort(compare: (T?, T?) -> Int) =
		sList.sortWithData { a, b ->
			compare(a?.let(wrapPointer), b?.let(wrapPointer))
		}

	/**
	 * @see [sList.concat]
	 */
	fun concat(list: WrappedSList<T>) =
		sList.concat(list.sList).wrap()

	/**
	 * @see [sList.forEach]
	 */
	fun forEach(action: (T) -> Unit) {
		sList.forEach {
			action(it.wrapPointer())
		}
	}

	/**
	 * @see [sList.last]
	 */
	fun last() =
		sList.last().wrap()

	/**
	 * @see [sList.nth]
	 */
	fun get(index: UInt) =
		sList.nth(index).wrap()

	/**
	 * @see [sList.nthData]
	 */
	fun getData(index: UInt) =
		sList.nthData(index)?.let(wrapPointer)

	/**
	 * @see [sList.find]
	 */
	fun find(data: T) =
		sList.find(data.getPointer()).wrap()

	/**
	 * @see [sList.position]
	 */
	fun getPosition(link: WrappedSList<T>) =
		sList.position(link.sList)

	/**
	 * @see [sList.index]
	 */
	fun getIndex(data: T) =
		sList.index(data.getPointer())

	/**
	 * Simple pass through wrap
	 */
	private fun SList.wrap(): WrappedSList<T> =
		wrap(wrapPointer, getPointer)

	/**
	 * Simple pass through wrap
	 */
	private fun SList?.wrap(): WrappedSList<T>? =
		this?.wrap()

	companion object {
		inline fun <T> SList.wrap(
			noinline wrapPointer: VoidPointer.() -> T,
			noinline getPointer: T.() -> VoidPointer
		): WrappedSList<T> = WrappedSList(this, wrapPointer, getPointer)

		inline fun <T> CPointer<GSList>.asWrappedSList(
			noinline wrapPointer: VoidPointer.() -> T,
			noinline getPointer: T.() -> VoidPointer
		): WrappedSList<T> = WrappedSList(this.wrap(), wrapPointer, getPointer)
	}
}