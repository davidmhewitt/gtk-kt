import org.gtk.dsl.gio.*
import org.gtk.dsl.gtk.application
import org.gtk.dsl.gtk.applicationWindow
import org.gtk.dsl.gtk.x
import org.gtk.gio.MenuItem

/**
 * @see <a href="https://stackoverflow.com/a/69153849">Source</a>
 */
fun main() {
	application("com.ircclient") {
		onCreateUI {
			addSimpleAction("connect") {
				addOnActivateCallback {
					println("The action $name was clicked.")
				}
			}

			addSimpleAction("disconnect") {
				addOnActivateCallback {
					println("The action $name was clicked.")
				}
			}

			menuBar {
				appendSubmenu("Network", menu {
				})

				// *** Server_Menu
				appendSubmenu("Server", menu {
					appendItem(MenuItem("Connect", "app.connect"))
					appendItem(MenuItem("Disconnect", "app.disconnect"))
				})
			}

			applicationWindow {
				title = "IRC Client"
				defaultSize = 400 x 400
				showMenuBar = true
			}.present()
		}
	}
}