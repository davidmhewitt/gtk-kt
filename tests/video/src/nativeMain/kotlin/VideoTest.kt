import org.gtk.dsl.gio.onCreateUI
import org.gtk.dsl.gtk.*
import org.gtk.gtk.FileChooser
import org.gtk.gtk.Video
import org.gtk.gtk.common.enums.Orientation
import org.gtk.gtk.widgets.windows.dialog.Dialog.ResponseType.ACCEPT
import org.gtk.gtk.widgets.windows.dialog.FileChooserDialog

fun main() {
	application("org.gtk.example") {
		onCreateUI {
			applicationWindow {
				title = "Window"

				fun openFileChooser(video: Video) {
					FileChooserDialog(
						FileChooser.Action.ACTION_OPEN,
						this@applicationWindow,
						"Select Video",
						"Open",
						"Cancel"
					).apply {
						addOnResponseCallback { response ->
							when (response) {
								ACCEPT -> {
									if (file != null) {
										video.mediaStream?.streamEnded()
										video.file = file
										this@applicationWindow.title =
											file!!.baseName
									}
									close()
								}
								else -> close()
							}
						}
						show()
					}
				}

				box(Orientation.VERTICAL, 10) {
					val video = Video().apply {
						sizeRequest = 720 x 480
					}
					append(video)

					append(
						box(Orientation.HORIZONTAL, 5) {
							this.button("Play") {
								onClicked { video.mediaStream?.play() }
							}
							button("Pause") {
								onClicked { video.mediaStream?.pause() }
							}
							button("Select File") {
								onClicked {
									openFileChooser(video)
								}
							}
							button("Quit") {
								onClicked {
									// Be careful of the context when you invoke destroy
									this@applicationWindow.close()
								}
							}
						}
					)
				}
			}.show()
		}
	}
}