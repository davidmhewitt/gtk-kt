import org.jetbrains.dokka.base.DokkaBase
import org.jetbrains.dokka.base.DokkaBaseConfiguration


plugins {
	kotlin("multiplatform")
	id("org.jetbrains.dokka")
}

allprojects {
	group = "org.gtk-kt"

	repositories {
		mavenCentral()
	}
}

kotlin {
	val hostOs = System.getProperty("os.name")
	//val isMingwX64 = hostOs.startsWith("Windows")
	val nativeTarget = when {
		//hostOs == "Mac OS X" -> macosX64("native")
		hostOs == "Linux" -> linuxX64("native")
		//isMingwX64 -> mingwX64("native")
		else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
	}
}

tasks {
	dokkaHtmlMultiModule {
		outputDirectory.set(buildDir.resolve("dokka"))

		includes.from("dokka/includes/home.md")

		pluginConfiguration<DokkaBase, DokkaBaseConfiguration> {
			customStyleSheets =
				rootDir
					.resolve("dokka/stylesheets")
					.listFiles()
					.toList()

			customAssets =
				rootDir
					.resolve("dokka/assets")
					.walkTopDown()
					.filter { it.isFile }
					.toList()
		}
	}
}
