import org.jetbrains.kotlin.gradle.plugin.KotlinPlatformType

plugins {
	kotlin("multiplatform")
}

val applicationId = "org.gtk-kt.example"

kotlin {
	linuxX64("native") {
		val main by compilations.getting
		binaries {
			executable()
		}
	}

	sourceSets {

		val nativeMain by getting {
			dependencies {
				implementation(project(":src:gtk"))
				implementation(project(":dsl"))
				implementation(project(":coroutines"))
				implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.5.0-native-mt")
			}
		}
	}
}

tasks {
	kotlin.targets.filter { it.platformType == KotlinPlatformType.native }.forEach {
		println(it)
		println(it.name)
		println(it.targetName)
		println(it.artifactsTaskName)
		println(it.platformType)
	}

	arrayOf("release", "debug").forEach { tarType ->
		val tarTypeCap = tarType.capitalize()
		register("generate${tarTypeCap}FlatpakManifest") {
			group = "distribution"

			dependsOn(
				getTasksByName("build", false)
			)
			val file = File("${buildDir.absolutePath}/distributions/flatpak/${applicationId}.yml")

			doFirst {
				file.parentFile?.mkdirs()
				file.createNewFile()
			}

			doLast {
				file.writeText(
					"""
						app-id: $applicationId
						runtime: org.gnome.Platform
						runtime-version: 41
						sdk: org.gnome.Sdk
						command: ${project.name}.kexe
						modules:
						  - name: $tarType
						    buildsystem: simple
						    build-commands:
						      - install -D ${buildDir.absolutePath}/bin/native/${tarType}Executable/* /app/bin/
					    finish-args:
						  - socket=fallback-x11
						  - socket=wayland
					""".trimIndent()
				)
			}
		}

		register("generate${tarTypeCap}DesktopFile") {
			group = "distribution"
			val file = File("${buildDir.absolutePath}/files/$tarType/$applicationId.desktop")
			doFirst {
				file.parentFile?.mkdirs()
				file.createNewFile()
			}
			doLast {
				file.writeText(
					"""
					[Desktop Entry]
					Encoding=UTF-8
					Version=$version
					Type=Application
					Terminal=false
					Exec=${project.name}
					Name=${project.name}
				""".trimIndent()
				)
			}
		}

		register<Tar>("generate${tarTypeCap}Tar") {
			dependsOn(
				getTasksByName("build", false)
			)
			dependsOn(
				getTasksByName("create${tarTypeCap}DesktopFile", true)
			)
			group = "distribution"
			archiveAppendix.set(tarType)
			description = "Generates a $tarType tar of a native linux application"
			from(
				File("${buildDir.absolutePath}/bin/native/${tarType}Executable/"),
				File("${buildDir.absolutePath}/files/${tarType}/")
			)
			rename {
				if (it.endsWith(".kexe")) {
					it.replace(".kexe", "")
				} else {
					it
				}
			}
		}
	}
}