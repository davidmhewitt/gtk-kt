package org.gtk.gobject.ktx

import org.gtk.gobject.KGObject

/*
 * gtk-kt
 *
 * 10 / 09 / 2021
 *
 * @see <a href=""></a>
 */

/**
 * Use a [KGObject], then unref, finally return the result
 */
fun <T : KGObject, O> T.use(block: (T) -> O): O {
	val result = block(this)
	unref()
	return result
}