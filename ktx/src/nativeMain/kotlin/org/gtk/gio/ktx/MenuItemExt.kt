package org.gtk.gio.ktx

import org.gtk.gio.Menu
import org.gtk.gio.MenuItem
import org.gtk.gio.MenuModel

inline operator fun MenuItem.plus(menu: Menu) =
	menu.prependItem(this)

inline operator fun MenuItem.get(link: String) =
	getLink(link)

inline operator fun MenuItem.set(link: String, menuModel: MenuModel) =
	setLink(link, menuModel)