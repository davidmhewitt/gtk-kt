package org.gtk.gio.ktx

import org.gtk.gio.Menu
import org.gtk.gio.MenuItem


inline operator fun Menu.plus(item: MenuItem) = appendItem(item)