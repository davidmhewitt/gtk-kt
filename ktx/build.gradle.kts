plugins {
	kotlin("multiplatform")
	`maven-publish`
	`dokka-script`
}

version = "0.1.0-alpha"

kotlin {
	linuxX64("native") {
		val main by compilations.getting
		binaries {
			sharedLib()
		}
	}

	sourceSets {
		val nativeMain by getting {
			dependencies {
				implementation(project(":src:gtk"))
			}
		}
	}
}
