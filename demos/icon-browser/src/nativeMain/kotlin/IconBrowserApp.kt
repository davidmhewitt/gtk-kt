import glib.glib_major_version
import glib.glib_micro_version
import glib.glib_minor_version
import org.gtk.gio.ActionEntry
import org.gtk.glib.OSInfoKey
import org.gtk.glib.getOSInfo
import org.gtk.gtk.*
import org.gtk.gtk.widgets.windows.Window
import org.gtk.gtk.widgets.windows.dialog.AboutDialog
import org.gtk.pango.pangoVersionString

/**
 * gtk-kt
 *
 * 24 / 09 / 2021
 *
 * @see <a href=""></a>
 */
class IconBrowserApp {
	var parent: Application? = null
	val appEntries = arrayOf(
		ActionEntry("quit", { _, _, _ -> quitActivated() }),
		ActionEntry("inspector", { _, _, _ -> inspectorActivated() }),
		ActionEntry("about", { _, _, _ -> aboutActivated() })
	)


	fun quitActivated() {
		parent?.quit()
	}

	fun inspectorActivated() {
		Window.setInteractiveDebugging(true)
	}

	fun aboutActivated() {
		val authors = arrayOf("The GTK Team", "Doomsdayrs")
		val iconTheme: String
		val version: String

		iconTheme = Settings.default?.getString("gtk-icon-theme-name")!!

		val s = StringBuilder()

		val osName: String? = getOSInfo(OSInfoKey.NAME)
		val osVersion: String? = getOSInfo(OSInfoKey.VERSION)

		if (osName != null && osVersion != null) {
			s.appendLine("OS\t$osName $osVersion \n")
		}

		s.appendLine("System libraries")
			.appendLine("\tGlib\t$glib_major_version.$glib_minor_version.$glib_micro_version")
			.appendLine("\tPango\t$pangoVersionString")
			.appendLine("\tGTK\t%$gtkMajorVersion$gtkMinorVersion$gtkMicroVersion")
			.append("\nIcon theme\n\t$iconTheme")

		version = "???\nRunning against GTK $gtkMajorVersion.$gtkMinorVersion.$gtkMicroVersion"
		AboutDialog().apply {
			programName = "GTK Icon Browser" // TODO demo_conf.h
			this.version = version
			copyright = "© 1997—2021 The GTK Team"
			licenseType = AboutDialog.License.LGPL_2_1
			comments = "Program to browse themed icons"
			this.authors = authors
			logoIconName = "org.gtk.IconBrowser4"
			title = "About GTK Icon Browser"
			// TODO system-information
		}.show()
	}

	fun startup(application: Application) {
		val quitAccels = arrayOf("<Ctrl>Q")
		//TODO G_APPLICATION_CLASS (icon_browser_app_parent_class)->startup (app);
		//application.addActionEntries()
	}
}

